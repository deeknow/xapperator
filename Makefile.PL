use ExtUtils::MakeMaker;
# See lib/ExtUtils/MakeMaker.pm for details of how to influence
# the contents of the Makefile that is written.
WriteMakefile(
    'NAME'	=> 'XApperator',
    'VERSION_FROM' => 'lib/XApperator.pm', # finds $VERSION
	($] >= 5.005 ?
      ( AUTHOR         => 'Dean Stringer <deeknow@pobox.com>') : ())
);
