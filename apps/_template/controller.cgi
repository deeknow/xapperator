#!c:/Perl/bin/perl -w
use strict;
use CGI;
use XApperator;
my $cgi = new CGI;
print $cgi->header('text/html; charset=UTF-8');
print "<html>
    <head><link rel='stylesheet' href='/css/XApp.css'></head>
    <body>";
my $XApp = XApperator->new(
        cgi => $cgi,
        errorsToUser => 1,
        dieOnError => 1,    # default is '0'
        rootDir => "w:/wwwroot/XApp/",
        XSLTDir => "w:/wwwroot/XApp/xslt/",
        resourcesPath => "/XApp/resources/",
);

if ($XApp->error()) {
    print "Failed to create application, see error log";
} else {
    print "<h1>" . $XApp->title() . "</h1>";
    print $XApp->menuHTML() . "\n";
    $XApp->processActions();
    if ($XApp->error()) {
        print "Something went wrong: " . $XApp->error();
    } else {
        print $XApp->content();
    }
}
print "</body></html>";
exit;