<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
	xmlns:rss="http://purl.org/rss/1.0/" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
	xmlns:dc="http://purl.org/dc/elements/1.1/" 
	xmlns:taxo="http://purl.org/rss/1.0/modules/taxonomy/" 
	xmlns:uow="http://waikato.ac.nz/syndication/rss/" 
	xmlns:admin="http://webns.net/mvcb/" 
	xmlns:syn="http://purl.org/rss/1.0/modules/syndication/"
	exclude-result-prefixes="rss syn admin taxo uow dc rdf"
	>
<xsl:output method="xml"></xsl:output>

<xsl:template match="/">
<rdf:RDF>
<channel rdf:about="http://xapperator.sourceforge.net/blog/">
<title>XApperator Blog</title>
<link>http://xapperator.sourceforge.net/blog/</link>
<description>An example use of XApperator to manage the RSS contents of a blog</description>
<dc:language>en</dc:language>
<syn:updatePeriod>daily</syn:updatePeriod>
<syn:updateFrequency>1</syn:updateFrequency>
<syn:updateBase>1901-01-01T00:00+00:00</syn:updateBase>
<items>
<rdf:Seq>
<rdf:li rdf:resource="http://xapperator.sourceforge.net/blog/"/>
</rdf:Seq>
</items>
<xsl:apply-templates select="items/item"></xsl:apply-templates>
</channel>
</rdf:RDF>

</xsl:template>

<xsl:template match="item">
<item rdf:about="http://xapperator.sourceforge.net/blog/">
<title><xsl:value-of select="title"></xsl:value-of></title>
<link><xsl:value-of select="link"></xsl:value-of></link>
<description><xsl:value-of select="description"></xsl:value-of></description>
<dc:date><xsl:value-of select="date"></xsl:value-of></dc:date>
</item>
</xsl:template>

</xsl:stylesheet>
