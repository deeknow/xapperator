<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dc="http://purl.org/dc/elements/1.1/" 
	exclude-result-prefixes="dc">

<xsl:output method="html" omit-xml-declaration="yes"></xsl:output>
<xsl:param name="key"></xsl:param>
<xsl:param name="fkey"></xsl:param>

<xsl:template match="application">
<xsl:apply-templates select="data/items"></xsl:apply-templates>
</xsl:template>


<xsl:template match="items">
<a href="http://xapperator.sourceforge.net/"><img src="http://xapperator.sourceforge.net/XApp-logo.jpg" align="right" alt="Logo"  border="0"></img></a>
<h1>XApperator Blog</h1>
<p><em>An example use of XApperator to manage the content, presenation and RSS rendering of a sample blog.</em></p>
<h3>entries for month: <xsl:value-of select="/application/foreignkeys/key[value = $fkey]/label"></xsl:value-of></h3>
<hr></hr>
<xsl:apply-templates select="item">
<xsl:sort select="date" data-type="text" order="descending"></xsl:sort>
</xsl:apply-templates>
<a href="http://xapperator.sourceforge.net/rss.xml"><img src="http://xapperator.sourceforge.net/images/rss.png" alt="RSS" border="0"></img></a>
</xsl:template>

<xsl:template match="item">
<h2><xsl:value-of select="title"></xsl:value-of></h2>
<p><xsl:value-of select="description"></xsl:value-of>
<br></br><xsl:text disable-output-escaping="yes">&amp;nbsp;&amp;nbsp;</xsl:text><small><xsl:value-of select="substring(date,1,4)"></xsl:value-of>/<xsl:value-of select="substring(date,5,2)"></xsl:value-of>/<xsl:value-of select="substring(date,7,2)"></xsl:value-of> - <xsl:value-of select="substring(time,1,2)"></xsl:value-of>:<xsl:value-of select="substring(time,3,2)"></xsl:value-of>  - <a href="{link}">link</a></small>
</p>
<hr></hr>
</xsl:template>

</xsl:stylesheet>
