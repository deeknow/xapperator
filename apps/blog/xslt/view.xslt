<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:param name="fkey"></xsl:param>
	<xsl:param name="key"></xsl:param>

<xsl:template match="/application">
<h1>XApperator Blog</h1>
<xsl:apply-templates select="foreignkeys"></xsl:apply-templates>
<xsl:apply-templates select="data"></xsl:apply-templates>
</xsl:template>

<xsl:template match="foreignkeys">
<div style="width:250px; position: absolute; left: 15px; top: 65px; height: 75px;">
<h2>Previous Posts:</h2>
<ul>
<xsl:for-each select="key">
<li><a href="controller.cgi?x-app=blog&amp;x-fkey={value}&amp;x-mode=View"><xsl:value-of select="label"></xsl:value-of></a></li>
</xsl:for-each>
</ul>
</div>
</xsl:template>


<xsl:template match="data">
<div style="width:450px; position: absolute; left: 215px; top: 65px; height: 75px;">
<xsl:for-each select="items/item">
<h3><xsl:value-of select="title"></xsl:value-of></h3><xsl:text>
</xsl:text>
<xsl:if test="link != ''">
<a href="{link}">link</a>
</xsl:if><xsl:text>
</xsl:text>
<p><xsl:value-of select="description"></xsl:value-of></p>
<p style="font-size: 0.7em; text-decoration: italic; color: #666666;">
<xsl:value-of select="date"></xsl:value-of> - 
<xsl:value-of select="time"></xsl:value-of>
</p>
</xsl:for-each>
</div>
</xsl:template>

</xsl:stylesheet>
