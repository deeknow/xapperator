<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="application">
<h2>Search:</h2>

<form action="">
	<input type="hidden" name="x-app" value="demo" />
	<input type="hidden" name="x-fkey" value="{$fkey}" />
	<input type="hidden" name="x-mode" value="search" />
	<p>Search <input type='text' name='x-search' value='{$search}'  /><input type='submit' value='Search' /></p>
</form>

<xsl:for-each select="data/testroot/testrecord">
<h2><xsl:value-of select="name"></xsl:value-of></h2>
<p><xsl:value-of select="desc" disable-output-escaping="yes"></xsl:value-of></p>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
