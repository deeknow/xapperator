<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="application">
<h1>Transportation Favourites</h1>
<xsl:for-each select="data/testroot/testrecord">
<h2><xsl:value-of select="name"></xsl:value-of></h2>
<p><xsl:value-of select="desc" disable-output-escaping="yes"></xsl:value-of></p>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
