<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xhtml">
<xsl:output method="html" media-type="html" encoding="utf-8" 
doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd" indent="yes"/>
<xsl:param name="fkey"></xsl:param>
<xsl:param name="fkeyLabel"></xsl:param>

<xsl:template match="/" >
<html>
<head lang="en">
	<title><xsl:value-of select="/department/name" /> Staff</title>
	<meta name="description" content="" />
	<meta name="author" content="" />
</head>
<body style="font-family: verdana, arial; font-size: 0.9em;">
<xsl:apply-templates select="staff"></xsl:apply-templates>
</body></html>
</xsl:template>

<xsl:template match="staff">
<h1>Phonebook: <xsl:value-of select="$fkeyLabel"></xsl:value-of> Department</h1>
<table border="0" width="500" cellspacing="3" style="margin-left: 1.5em;">
<xsl:apply-templates select="member">
<xsl:sort select="familyname" order="ascending"></xsl:sort>
</xsl:apply-templates>
</table>
</xsl:template>

<xsl:template match="member">
<tr>
<td valign="top" nowrap="nowrap" colspan="2">
<strong><xsl:value-of select="name"></xsl:value-of></strong></td>
</tr>
<tr>
<td>
<xsl:value-of select="contact"></xsl:value-of>
</td>
<td>
<xsl:if test="showemail = '1'">
email: <a><xsl:attribute name="href">mailto:<xsl:value-of select="email" /></xsl:attribute><xsl:value-of select="email" /></a><br />
</xsl:if>
</td>
</tr>
<tr>
<td colspan="2"><xsl:value-of select="expertise"></xsl:value-of></td>
</tr>
</xsl:template>

<xsl:template match="foreignkeys"></xsl:template>

</xsl:stylesheet>
