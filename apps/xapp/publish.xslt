<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xapp="http://xapperator.sourceforge.net/"
	exclude-result-prefixes="xhtml xapp">
<xsl:output method="html" media-type="html" encoding="utf-8" 
doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd" indent="yes" omit-xml-declaration="yes" />
<xsl:param name="fkey"></xsl:param>
<xsl:variable name="showEdits"></xsl:variable>
<xsl:variable name="cgiPath">http://localhost/cgi-bin/XApp/controller.cgi</xsl:variable>
<xsl:template match="/" >

<html>
<head>
<title>XApperator: <xsl:value-of select="page/element[id='title']/content" /></title>
<meta name="Generator" content="XApperator" />
<meta name="Author" content="deeknow@pobox.com" />
<meta name="Keywords" content="" />
<link rel="shortcut icon" href="favicon.ico" />
<meta name="Description" content="XApperator: Perl based open source MVC-like lightweight XML editing application controller framework" />
<link rel='stylesheet' type='text/css' href='XApp.css' />
</head>

<body>
<div class='header'>
<a href='index.html'><img src="XApp-logo.jpg" alt="logo" align="left" width="200" border="0"></img></a>
<h1 class="header">XApperator</h1>
<p class="header">lightweight, manouverable and quickly deployed</p>
</div>

<div class='menu'>
<ul>
<li><a href='index.html'>Description</a></li>
<li><a href='changes.html'>Changes</a></li>
<li><a href='http://sourceforge.net/projects/xapperator/'>Download</a></li>
<li><a href='documentation.html'>Documentation</a></li>
<li><a href='examples.html'>Examples</a></li>
<li><a href='FAQ.html'>FAQ</a></li>
<li><a href='features.html'>Features</a></li>
<li><a href='requirements.html'>Requirements</a></li>
<li><a href='screenshots.html'>Screenshots</a></li>
<li><a href='GPL'>License</a></li>
</ul>

<ul>
<li><a href='http://www.deeknow.com/notes/about_dk.html'>Author</a></li>
<li><a href='http://xapperator.sourceforge.net/rss.xml'><img src='images/rss.png' border='0' alt='RSS version' /></a></li>
</ul>
<center>
<br/><A href="http://sourceforge.net/projects/xapperator/"> <IMG src="http://sourceforge.net/sflogo.php?group_id=133744" width="88" border="0" alt="SourceForge.net Logo" /></A>
<br clear='all' />
<br/><A href="http://www.perl.com/"><IMG src="images/pb_perl.gif" width="88" height="31" border="0" alt="Powered by Perl" /></A>
<br clear='all' />
<br/><A href="http://xmlsoft.org/"><IMG src="images/Libxml2.gif" width="88" height="31" border="0" alt="Libxml2" /></A>
</center>
</div>

<div class='body'>
<h2><xsl:value-of select="page/element[id='title']/content" /></h2>
<xsl:if test="page/element[type = 'intro']">
<p class="intro"><xsl:value-of select="page/element[type = 'intro']/content"></xsl:value-of></p>
</xsl:if>
<xsl:for-each select="page/element[(id != 'title') and (type != 'intro')]">
<xsl:sort select="order"></xsl:sort>
<xsl:apply-templates select="."></xsl:apply-templates>
</xsl:for-each>
<xsl:if test="$showEdits = '1'">
<p><a href="{$cgiPath}?app=xapp&amp;fkey={$fkey}&amp;mode=Add&amp;nomenu"><img border="0" src="add_button.gif" alt="[+]"></img> add an element</a></p>
</xsl:if>

<p><br></br><small><em>Authored using</em>: <a href='http://xapperator.sourceforge.net/'>XApperator</a></small></p>

</div>

</body>
</html>
</xsl:template>

<xsl:template match="element">
<xsl:choose>
	<xsl:when test="type = 'h1'"><h1><xsl:value-of select="content"></xsl:value-of><xsl:call-template name="editbutton"></xsl:call-template></h1></xsl:when>
	<xsl:when test="type = 'h2'"><h2><xsl:value-of select="content"></xsl:value-of><xsl:call-template name="editbutton"></xsl:call-template></h2></xsl:when>
	<xsl:when test="type = 'h3'"><h3><xsl:value-of select="content"></xsl:value-of><xsl:call-template name="editbutton"></xsl:call-template></h3></xsl:when>
	<xsl:when test="type = 'li'"><p><img src="bullet.gif" alt="*"></img><xsl:text> </xsl:text><xsl:value-of select="xapp:displayPrep(content)" disable-output-escaping="yes"></xsl:value-of><xsl:call-template name="editbutton"></xsl:call-template></p></xsl:when>
	<xsl:when test="type = 'blob'"><xsl:value-of select="xapp:displayPrep(content)" disable-output-escaping="yes"></xsl:value-of><xsl:call-template name="editbutton"></xsl:call-template></xsl:when>
	<xsl:otherwise><p><xsl:value-of select="xapp:displayPrep(content)" disable-output-escaping="yes"></xsl:value-of><xsl:call-template name="editbutton"></xsl:call-template></p></xsl:otherwise>
</xsl:choose>
</xsl:template>



<xsl:template name="editbutton">
<xsl:if test="$showEdits = '1'">
<xsl:variable name="href"><xsl:value-of select="$cgiPath"></xsl:value-of>?app=xapp&amp;fkey=<xsl:value-of select="$fkey"></xsl:value-of>&amp;&amp;mode=Edit&amp;key=<xsl:value-of select="id"></xsl:value-of>&amp;nomenu</xsl:variable>
<xsl:text> </xsl:text><a href="{$href}" alt='edit' title="edit this"><img border="0" src="edit_button.gif" alt="edit"></img></a>
</xsl:if>
</xsl:template>

</xsl:stylesheet>
