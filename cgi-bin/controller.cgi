#!/usr/bin/perl -w
# or !c:/Perl/bin/perl -w
# --------------------------------------------------------------------------------- 
# XApperator - sample controller.cgi script for XApperator framework
# Copyright (C) 2005-2007  Dean Stringer (deeknow @ pobox . com)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# --------------------------------------------------------------------------------- 
# Author: Dean Stringer (deeknow @ pobox . com)
# Original Release Date: 11/Feb/2005
# This Release: 19/Sep/2007
# Version: 1.0
#
# Description/Purpose:
#    This script demonstrates a minimal use of the generic XApperator.pm
#    module. See man XApperator.pm for details
# --------------------------------------------------------------------------------- 

use strict;
use CGI;
use XApperator;

my $cgi = new CGI;    # need to pass all CGI params into the XApp object as we instantiate it

my $XApp = XApperator->new(
        cgi => $cgi,
        detailsToUser => 1,
        debugMode => 1,		# default is '0'
        dieOnError => 1,	# default is '0'

	# example *nix paths
        rootDir => '/home/www/XApp/instances/its/',
        XSLTDir => '/home/www/XApp/xslt/',
        resourcesPath => "/XApp/resources/"

	# example windows paths
        #rootDir => "w:/wwwroot/XApp/",
        #XSLTDir => "w:/wwwroot/XApp/xslt/",
        #resourcesPath => "/XApp/resources/",
);

$XApp->processActions();
print $XApp->contentHeader();
if ($XApp->mode() !~ /xml|view/) {	# dont wan't title in XML or VIEW mode
	print "<h1>" . $XApp->title() . "</h1>";
}
print $XApp->contentSelectmenu() .	 # might be supressed by XApp depending on mode we're in
	  $XApp->content() .
	  $XApp->contentFooter();

exit;
