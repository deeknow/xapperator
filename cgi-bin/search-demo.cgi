#!/usr/bin/perl -w
# !c:/Perl/bin/perl -w
# --------------------------------------------------------------------------------- 
# XApperator - sample controller.cgi script for XApperator framework
# Copyright (C) 2005  Dean Stringer (deeknow @ pobox . com)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# --------------------------------------------------------------------------------- 
# Author: Dean Stringer (deeknow @ pobox . com)
# This Release: 24/Sep/2007
# Original Release Date: 24/Sep/2007
# Version: 1.0
#
# Description/Purpose:
#    This script demonstrates use of the XApperator framework to present
#    a searchable interface to an application's data-set. See the demo
#    apps folder for a sample config file which is used with this CGI
# --------------------------------------------------------------------------------- 

use strict;
use CGI;
use XApperator;

my $cgi = new CGI;    # need to pass all CGI params into the XApp object as we instantiate it

my $XApp = XApperator->new(
        cgi => $cgi,
	app => 'demo',
	configFile => 'config-search.xml',
	debugMode => 0,
        XSLTDir => "/home/www/XApperator/xslt/",
        rootDir => "/home/www/XApperator/",
        resourcesPath => "/utils/XApp/resources/",
);
$XApp->processActions();
print $XApp->contentHeader();
if ($XApp->mode() !~ /xml|view/) {	# dont want title or select menu in XML or VIEW mode
	print "<h1>" . $XApp->title() . "</h1>" . $XApp->contentSelectmenu();
}
print $XApp->content() .
	$XApp->contentFooter();

exit;
