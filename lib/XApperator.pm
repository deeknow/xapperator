package XApperator;

=pod

=head1 NAME

XApperator - simple XML editing framework

=head1 LICENSE

XApperator: simple XML editing/publishing framework
Copyright (C) 2005-2007  Dean Stringer (deeknow @ pobox . com)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

=head1 VERSION

B<1.0.1>

This Release: 24/Sep/2007
Original Release: 11/Feb/2005

=head1 AUTHOR

Dean Stringer (deeknow @ pobox . com)

=head1 DESCRIPTION

XApperator is a Perl based open source MVC-like lightweight 
application controller framework which uses XML to store individual 
application's configuration, functionality availability, behaviour 
and UI messages, uses XSLT to transform simple application XML 
data structures into vanilla web interfaces for editing and 
administration, and optionally renders data/content out to 
a static publishing point using XSLT for eventual web serving 
and end-user consumption

XApp is 'MVC-like' in that we have a 'M'odel in the form of the
<config> element and its children in the condig file, we've
got a 'V' in the form of the generic XSLT script which gets
behavioural queues from the data and config, then we've got
our 'C'ontroller in the form of this generic Perl module
which takes input from user and directs the view output.

=cut

# --------------------------------------------------------------------------------- 

BEGIN {
use vars qw($VERSION);
use XApperator::Utils;
use XApperator::Model;
use XApperator::View;
use XApperator::Controller;
use XML::LibXSLT;
use XML::LibXML;

our @months = qw/Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec/;
our @days = qw/Mon Tue Wed Thu Fri Sat Sun/;

$VERSION = '1.0.1';
}
# --------------------------------------------------------------------------------------
=pod

=head1 SYNOPSIS

    use XApperator;
    use CGI;
    my $cgi = new CGI; 
    my $XApp = XApperator->new(
        cgi => $cgi,
        debugMode => 1,		# default is '0'
        detailsToUser => 1, # default is '0'
        dieOnError => 1,    # default is '0'
        rootDir => "/home/XApp/",                 # main XApp root directory
        XSLTDir => "/home/XApp/xslt/",            # common XSLT transforms
        resourcesPath => '/tools/XApp/resources/',	# relative path to JS, CSS etc
        );

    $XApp->processActions();         # check CGI params and manipulate data

    # output the seperate pieces of the UI
    print $XApp->contentHeader();
    if ($XApp->mode() !~ /xml|view/) {	# dont wan't title in XML or VIEW mode
        print "<h1>" . $XApp->title() . "</h1>";
    }
    print $XApp->contentSelectmenu() .	 # might be supressed by XApp depending on mode we're in
        $XApp->content() .
        $XApp->contentFooter();

=cut

sub new {
    my $invocant = shift;
    my $class = ref($invocant) || $invocant;
    my $self = {
    	# any of these can theoretically be overridden by passing into the new() call in your controller CGI
        app => '',    # name of sub-folder containing the app data and config
        appDir => 'apps/',        # relative to 'rootDir'
        configFile => 'config.xml',    # default = 'config.xml', no need to change
        content => '',        # container for XSLT render output
		dateEntryFormat => 'ISO8601',		# 'ISO8601' or 'compound', date field submission format
        dataDir => 'data/',       # relative to 'appDir'
        debugMode => 0,    # if '1' show some details to user re keys index fields etc
        detailsToUser => 0,              # used in dieNice
        dieOnError => 0,    # dont die by default, return an error message to app
        display => '',    # used to direct the XSLT display mode
        displayAlert => '',    # used to direct the XSLT alert mode
        editXSLTFile => 'edit.xslt',    # standard UI builder
        menuXSLTFile => 'menu.xslt',    # standard recordset selector builder
        publishFolder => '',
        relativeURL => CGI::url(-relative=>1),
        resourcesDir => 'resources/',    # language specific messages and titles
        resourcesPath => '/XApp/resources/',    # location of Javascript and UI files relative to web root
        rootDir => '/home/www/XApp/',    # default, normally overridden in controller.cgi
        useCompressedJS => 0,
        warnLevel => 1,
        XSLTDir => 'xslt/',        # relative to 'appRoot'
        # --------------------------------------
        @_    # remaining passed arguments
        # -------------------------------------- 
        };
    bless ($self, $class);
    $self->_parseCGIParams();
	unless ($self->mode() =~ /^(add|cancel|cut|copy|delete|edit|new|paste|publish|publishall|save|search|select|xml|view|)$/) {
		$self->_warn('Invalid mode', "mode '" . $self->mode() . "' not recognised, defaulting to 'select'");
		$self->mode('message');
       	$self->{displayAlert} = 'invalidmode';
	}
	unless ($self->{app} ne '') {
		$self->dieNice('No app specified','');
	}
	unless (-e $self->{rootDir} . $self->{appDir} . $self->{app} . '/') {
	    $self->dieNice("No such app found", $self->{rootDir} . $self->{appDir} . $self->{app});
	}
    $self->{parser} = XML::LibXML->new();
    $self->{xslt} = XML::LibXSLT->new();
    # create the container for the compound data/config we will pass into the XSLT
    # note: we dont actually get record data until processActions gets called
    $self->{applicationDoc} = XML::LibXML::Document->new();
    $self->{applicationRootNode} = $self->{applicationDoc}->createElement( 'application' );
    $self->{applicationDoc}->setDocumentElement($self->{applicationRootNode});
    $self->_checkLang();
   	$self->_getDateTime();    # set self->{dateToday} and ->{timeNow)
  
    unless (-e $self->_configFilePath()) { $self->dieNice('No config file found', $self->_configFilePath()); }
    unless ($self->_parseConfig()) { return $self; }
    unless ($self->_loadUIresource('titles')) { $self->dieNice('No title resources found'); }
    unless ($self->_loadUIresource('messages')) { $self->dieNice('No messages resources found'); }
    unless (-e $self->{fkeyPath}) { $self->dieNice('No menu file found', $self->{fkeyPath}); }
    unless ($self->_parseFKeyData()) { return $self; }
    $self->_getIndexFieldValue();
    $self->_registerFunctions();
	if ((defined $self->{selectEnabled}) && ($self->{selectEnabled} eq '1')) {	# usually selecting is enabled, but may not be if you dont want users browsing records
		$self->buildMenu() || $self->dieNice('Failed to parse menu stylesheet', $self->{menuXSLTFile});
	}
    return $self;
};


# =====================================================================================
=pod

=head1 DEPRECATED METHODS - THESE WILL BE DELETED IN v1.0 RELEASE 

 o menuHTML()

=cut

# =====================================================================================

=pod

=head1 SEE ALSO

L<XML::LibXSLT>, L<XML::LibXML>, XApperator::Model, XApperator::View, XApperator::Controller, XApperator::Utils  
L<http://xapperator.sourceforge.net/>

=cut

1
