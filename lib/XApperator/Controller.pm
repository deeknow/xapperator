package XApperator::Controller;

=pod

=head1 NAME

XApperator::Controller - Controller related functions for the XApperator MVC framework

=head1 VERSION

B<1.0.1>

This Release: 24/Sep/2007
Original Release: 20/Feb/2007

=head1 AUTHOR

Dean Stringer (deeknow @ pobox . com)

=head1 DESCRIPTION

This module contains subs that are invoked by the XApperator::new() constructor
to check submitted CGI paramaters, load and determine the paths of various resource
files, read an applications configuration, and ultimately to processActions()
based on application state and requests.

=cut

# --------------------------------------------------------------------------------- 


# --------------------------------------------------------------------------------- 
BEGIN {
use vars qw($VERSION @EXPORT @ISA);
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(mode processActions title _checkCompulsoryFieldsOK _checkLang 
	_configFilePath _getCompoundCGIValue _parseCGIParams _parseConfig _dataPayloadReqd
	_fieldTypesOK _getIndexFieldValue _loadUIresource );
$VERSION = '1.0.1';
}
# --------------------------------------------------------------------------------------


# --------------------------------------------------------------------------------------
=pod

=head1 PUBLIC SUBS

The following public subs typicaly called by the controller.cgi, all other's
are really for internal use only

=cut
# --------------------------------------------------------------------------------------



sub mode {
# --------------------------------------------------------------------------------------
=pod

=head2 mode()

returns or sets the current operating mode, e.g. view, edit, select, delete, xml etc

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    if (@_) { $self->{mode} = shift }
	return $self->{mode};
}


sub title {
# --------------------------------------------------------------------------------------
=pod

=head2 title()

returns the text of the application title. used as method so developer
of controller.cgi doesnt need to reference any internal property value

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    return $self->{title};
}



sub processActions {
# --------------------------------------------------------------------------------------
=pod

=head2 processActions()

Check what mode user has selected and process the application XML data
accordingly. If there are any additional UI messages that needs to be displayed
to the user we set some special vars $self->{display} (for general info)
or {displayAlert} (for warnings or errors) that the XSLT files will check 
for to display appropriate messages. The actual messages bodies are stored 
in the resource files titles.xml and messages.xml

whether we're editing, canceling or listing we have to jump thru all the
same hoops, ie...

 o parse CGI paramaters
 o parse the config.xml file for the app requested
 o parse the data.xml file for the records requested
 o get the appropriate u/i messages for requested action
 o assemble a composite config/data XML document
 o parse the XSLT file that will handle the transform
 o do the final transform to create the UI

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    $self->{displayAlert} = '';
    $self->{display} = '';
    
    # chances are we're gonna need the data file if we're in one of
    # the modes that needs it, so parse it now. We dont need it if
    # we're editing keys
    if ( $self->_dataPayloadReqd()) {
        # load the data...
        unless ($self->_loadXML()) {
			# but if there was none, add a null payload so we can still build the UI
	        $self->_addNullPayload();
			$self->{display} = 'nofkeyexists';
			$self->{displayAlert} = 'nofkeyexists';
			$self->mode('message');  # coz there wont be any records to show
		}
    } else {
        # add a null root data payload so the XSLT has something to look at
        $self->_addNullPayload();
    }

	# -------------------------------------------------------------------------
	#               !!!!!!  please note  !!!!!!!
	# under some conditions following the 'mode' value is reset to a display/message 
	# mode so you shouldnt depend on it being the same value you passed into it
	# when calling processActions from a custom script of your own
	# -------------------------------------------------------------------------

	# ----------------------------------------------
	# log some key cgi values if we're in debug mode
	# ----------------------------------------------
	$self->_debug( "MODE: " . $self->{mode} . " FKEY: " . $self->{fkey} . " KEY: " . $self->{key} );

    # ----------------------------------------------
    # have we been asked for data only in xml mode
    # ----------------------------------------------
	if ($self->mode() eq 'xml') {
        unless ($self->{xmlEnabled} eq '1') {
            $self->{displayAlert} = 'noxmlview';    # warn the user XML view disabled
            $self->{mode} = 'message'; 
		} else {
			my $dataContainerNode = $self->{applicationDoc}->createElement( 'data' );
    	    $dataContainerNode->appendChild($self->{dataRootNode});
			$self->{applicationRootNode}->appendChild($dataContainerNode);
			$self->{content} = $self->{applicationRootNode}->toString;
			return;
		}

    # ----------------------------------------------
    # if were in Add mode
    # ----------------------------------------------
    } elsif ($self->mode() eq 'add') {
    	if ($self->{addEnabled} eq '0') {
           	$self->{displayAlert} = 'adddisabled';
           	$self->mode('message');
 		}

    # ----------------------------------------------
    # have we been asked to render a dataset for view-only
    # ----------------------------------------------
    } elsif (($self->mode() eq 'view')) {
		unless ($self->{viewEnabled} eq '1') {
            $self->{displayAlert} = 'viewdisabled';    # warn the user XML view disabled
		} else {
			if ($self->{fkey} eq '') {
				# no key value specified
        	    $self->{displayAlert} = 'nofkeyvalue';
			} elsif (! $self->_fkeyExists($self->{fkey})) {
				# no such foreign key
        	    $self->{displayAlert} = 'fkeyinvalid';
			} else {
				$self->{display} = 'viewed';
			}
		}

    # ----------------------------------------------
    # are we Adding a new foreign-key record file?
    # ----------------------------------------------
    } elsif (($self->mode() eq 'new')) {
        unless ($self->{addFKeyEnabled} eq '1') {
            $self->{displayAlert} = 'addingkeydisabled'; 
            $self->{mode} = 'message';
        }
        
    # ----------------------------------------------
    # are we Saving a new foreign-key record file?
    # ----------------------------------------------
    } elsif (($self->mode() eq 'save') && ($self->{type} eq 'keys')) {
        if ($self->{label} eq '') {
            # no foreign key label so generate error
            $self->{displayAlert} = 'nofkeylabel';
        } elsif ($self->{value} eq '') {
            # no value provided, must be one coz we use as filename
            $self->{displayAlert} = 'nofkeyvalue';
        } elsif (! $self->_fkeyValOK($self->{value})) {
            # value needs to be a file-system friendly sequence if chars
            $self->{displayAlert} = 'fkeyinvalid';
        } elsif ($self->_fkeyExists($self->{value})) {
            # value needs to be a file-system friendly sequence if chars
            $self->{displayAlert} = 'fkeyduplicate';
        } else {
            $self->_addForeignKey();
            $self->{display} = 'addedfkey';
        }
 
    # ----------------------------------------------
    # are we INSERTING a new record?
    #    insertkey will only be present if they previously select insert
    # ----------------------------------------------
    } elsif (($self->mode() eq 'save') && ( ($self->{insertkey} ne '') || ($self->{rootInsert} ne '')) ) {
        # cant add if its disabled so check that 1st
        unless ($self->{insertEnabled} eq '1') {
            $self->{displayAlert} = 'adddisabled';    # warn the user additions disabled
            $self->mode('message');
        } else {

            # make sure we've not exceeded the max record count
            if ($self->_recordCount() >= $self->{maxOccurs}) {
                $self->{displayAlert} = 'addtoomany';

            # check user has supplied an index field, cant save without one
            } elsif ($self->{indexFieldValue} eq '') {
                $self->{displayAlert} = 'noindex';   

            # check if there's an existing record with the same key
            } elsif ($self->_keyExists( $self->{indexFieldValue} )) {
                $self->{displayAlert} = 'duplicateindex';    # warn the user about the duplicate

            # make sure all the other 'required' fields have a value
            } elsif (not $self->_checkCompulsoryFieldsOK() ) {
                $self->{displayAlert} = 'missingfield';
            } else {
                $self->_insertRecord();
                $self->{display} = 'saved';
            }
        }

    # ----------------------------------------------
    # are we ADDING a new record? ie user hit 'Save'
    # ----------------------------------------------
    } elsif (($self->mode() eq 'save') && ($self->{oldkey} eq '')) {
        # this is a new record coz $oldkey is blank

        # cant add if its disabled so check that 1st
        if ($self->{addEnabled} eq '1') {

        # make sure we've not exceeded the max record count
        if ($self->_recordCount() >= $self->{maxOccurs}) {
                $self->{displayAlert} = 'addtoomany';
            
        # check if there's an existing record with the same key
        } elsif ($self->_keyExists( $self->{indexFieldValue} )) {
            $self->{displayAlert} = 'duplicateindex';    # warn the user about the duplicate

        # check is not an empty index field (dont care if an xapp:id coz we will auto generate one later
        } elsif (($self->{indexFieldValue} eq '') && ($self->{indexFieldName} ne 'xapp:id')) {
            $self->{displayAlert} = 'noindex';

        # make sure all the other 'required' fields have a value
        } elsif (not $self->_checkCompulsoryFieldsOK() ) {
            $self->{displayAlert} = 'missingfield';

        } else {
            $self->_addRecord();
            $self->{display} = 'addedrecord';    # confirm to user that record has been added
        }

        } else {
            $self->{displayAlert} = 'adddisabled';    # warn the user additions disabled
            $self->{mode} = 'message';
        }


    # ----------------------------------------------
    # ... or ... UPDATING an existing record
    # ----------------------------------------------
    } elsif ($self->mode() eq 'save') {

        # cant save if no index field value submitted so check it...
        if ($self->{indexFieldValue} eq '') {
            $self->{displayAlert} = 'noindex';    # warn the user about the empty index field

        # make sure all the other 'required' fields have a value
        } elsif (not $self->_checkCompulsoryFieldsOK() ) {
            $self->{displayAlert} = 'missingfield';

        # make sure all the other 'required' fields have a value
        } elsif (not $self->_fieldTypesOK() ) {
            $self->{displayAlert} = 'fieldvalerror';

        } else {
            # should be OK, but need to check for key collision
            # check to see if the key value has changed, ie new doesnt = old
            my $indexCount = 0;
            if ( $self->{oldkey} ne $self->{indexFieldValue} ) {
                # it has changed, so see if there's an existing record with the new
                # key value, if there is we dont want to replace it...
                my $indexCheckXPath = "count(//" . $self->{recordName} . "[" . $self->{indexFieldName} .
                    " = '" . $self->{indexFieldValue} . "'])";
                $indexCount = $self->{dataRootNode}->findvalue($indexCheckXPath);
            }
            if ($indexCount > 0) {
                # there's a key collision
                $self->{displayAlert} = 'duplicateindex';    # warn the user about the duplicate
            } else {
                # we should be good to go, no key collision
                my $changes = $self->_updateRecord();    # [3] always save, even if no changes
                if ($changes) {
                    $self->{display} = 'saved';
                    $self->{key} = $self->{indexFieldValue};
                } else {
                    $self->{displayAlert} = 'nochanges';
                }
            }
        }

    # ----------------------------------------------
    # ... or ... COPYING a record
    # ----------------------------------------------
    } elsif ($self->mode() eq 'copy') {
        unless ($self->{copyPasteEnabled} eq '1') {
            $self->{displayAlert} = 'copypastedisabled';    # warn the user XML view disabled
            	$self->mode('message');
        } else {
	        if ($self->{key} eq '') {
    	        # no key value either, looks like a delete without specifiying what
        	    $self->{displayAlert} = 'nothingselected';
            	$self->mode('select');
        	} else {
            	$self->{display} = '';
        	}
        }

    # ----------------------------------------------
    # ... or ... CUTTING a record
    # ----------------------------------------------
    } elsif ($self->mode() eq 'cut') {
        unless ($self->{copyPasteEnabled} eq '1') {
            $self->{displayAlert} = 'copypastedisabled';    # warn the user XML view disabled
            	$self->mode('message');
        } else {
	        if ($self->{key} eq '') {
    	        # no key value either, looks like a delete without specifiying what
        	    $self->{displayAlert} = 'nothingselected';
            	$self->mode('select');
	        } else {
    	        $self->{display} = '';
        	}
        }

    # ----------------------------------------------
    # ... or ... PASTING a record
    # ----------------------------------------------
    } elsif ($self->mode() eq 'paste') {

        unless ($self->{copyPasteEnabled} eq '1') {
            $self->{displayAlert} = 'copypastedisabled';    # warn the user XML view disabled
            $self->{mode} = 'message'; 
        } else {
	    	if ($self->{key} eq '') {
    			$self->{key} = $self->{tkey};
    		}
	        # make sure we've not exceeded the max record count
    	    if ($self->_recordCount() >= $self->{maxOccurs}) {
        	        $self->{displayAlert} = 'addtoomany';
    	    } elsif (! $self->_keyExists( $self->{tkey} )) {
	            $self->{displayAlert} = 'indexnotfound';    # warn the user if index not found
	        } else {
    	        $self->{insertkey} = $self->{tkey};
        	    $self->_pasteRecord();
            	$self->{display} = 'pasted';
        	}
        }

    # ----------------------------------------------
    # ... or ... DELETING a FKEY record
    # ----------------------------------------------
    } elsif (($self->mode() eq 'delete') && ($self->{type} eq 'keys')) {

        if ($self->{deleteFKeyEnabled} eq '1') {
        # dont do anything (really) unless deletion is allowed


            if ($self->{fkey} eq '') {
                # no key value either, looks like a delete without specifiying what
                $self->{displayAlert} = 'nothingselected';
            } elsif (! $self->_fkeyExists($self->{fkey})) {
                # no such foreign key, show an error
                $self->{displayAlert} = 'nofkeyexists';
            } else {
                # check they have confirmed delete
                if ($self->{confirm} ne 'yes') {
                    $self->{display} = 'confirmdelete';
                } else {
                    my $changes = $self->_deleteFKey();
                    if ($changes) {
                        $self->{display} = 'deletedfkey';
                        $self->mode('message');  # coz there wont be any records to show
                    } else {
                        $self->{displayAlert} = 'deletefkeyfail';
                    }
                }
            }

        } else {
            # deletion isnt allowed
            $self->{displayAlert} = 'deletedisabled';
			$self->mode('message');
        }

    # ----------------------------------------------
    # ... or ... DELETING a DATA record
    # ----------------------------------------------
    } elsif ($self->mode() eq 'delete') {
        if ($self->{deleteEnabled} eq '1') {
        # dont do anything (really) unless deletion is allowed

        if ($self->{oldkey} eq '') {
            # could be deleting from the record list rather than record edit i/f
            # in which case we should have a key value
            if ($self->{key} eq '') {
                # no key value either, looks like a delete without specifiying what
                $self->{displayAlert} = 'nothingselected';
            } else {
                # we've got a key value, so assign oldkey to that coz deleteRecord
                # will look for oldkey later
                $self->{oldkey} = $self->{key};
            }
        }
        
        if (($self->{oldkey} ne '') and ($self->{confirm} ne 'yes')) {
        # prompt for delete if confirm not set to Yes
            $self->{key} = $self->{oldkey};
            $self->{display} = 'confirmdelete';
        
        # cant do a delete without oldkey
        } elsif (($self->{oldkey} ne '') and ($self->{confirm} eq 'yes')) {
            # make sure we wont be deleting to less than the minimum no of records
            if ($self->_recordCount() <= $self->{minOccurs}) {
                $self->{displayAlert} = 'deletetoofew';
            } else {
                my $changes = $self->_deleteRecord();
                if ($changes) {
                    $self->{display} = 'deletedrecord';
                    $self->{mode} = 'select';
                } else {
                    $self->{displayAlert} = 'nochanges';
                }
            }
        }

        } else {
            # deletion isnt allowed
            $self->{displayAlert} = 'deletedisabled';
            $self->{mode} = 'message';
    	}


    # ----------------------------------------------
    # Publish a recordset using XSLT specified in the config file
    # ----------------------------------------------
    } elsif (($self->mode() eq 'publish') && ($self->{type} eq 'keys')) {
        if ($self->{publishEnabled} eq '1') {
            my $changes = $self->publish();
            if ($changes) {
                $self->{display} = 'published';
            } else {
                $self->{displayAlert} = 'publishfail';
            }
        } else {
            $self->{displayAlert} = 'publishdisabled';
            $self->{mode} = 'message';
        }


    # ----------------------------------------------
    # Publish ALL datasets
    # ----------------------------------------------

    } elsif (($self->mode() eq 'publishall') && ($self->{type} eq 'keys')) {
        if ($self->{publishAllEnabled} eq '1') {
            my $changes = $self->publishAll();
            if ($changes) {
                $self->{display} = 'published';
            } else {
                $self->{content} .= 'publishing failed';
                $self->{displayAlert} = 'publishfail';
            }
        } else {
            $self->{displayAlert} = 'publishalldisabled';
            $self->{mode} = 'message';
        }

    # ----------------------------------------------
    # Search all recordsets
    # ----------------------------------------------
    } elsif ($self->mode() eq 'search') {
        if ($self->{searchEnabled} ne '1') {
            $self->{displayAlert} = 'searchdisabled';
			$self->mode('message');
        } elsif ($self->{search} eq '') {
            $self->{displayAlert} = 'searchempty';
        } else {
            $self->{display} = 'searched';
            $self->{displayAlert} = '';
        }
   
    # ----------------------------------------------
    # if were in edit mode check user has actually selected a record
    # ----------------------------------------------
    } elsif ($self->mode() eq 'edit') {
    	if ($self->{editEnabled} eq '1') {
			if (! $self->_keyExists($self->{key})) {
    	        $self->{displayAlert} = 'indexnotfound';    # warn the user if index not found
				$self->mode('select');
			} elsif ($self->{key} eq '') {
            	$self->{displayAlert} = 'nothingselected';
			}
 		} else {
           	$self->{displayAlert} = 'editdisabled';
           	$self->mode('message');
 		}

    # ----------------------------------------------
    # if were in edit mode check user has actually selected a record
    # ----------------------------------------------
    } elsif ($self->mode() eq 'select') {
    	unless ($self->{selectEnabled} eq '1') {
           	$self->{displayAlert} = 'selectdisabled';
           	$self->mode('message');
    	}

    # ----------------------------------------------
	# otherwise may be in cancel mode
	# ----------------------------------------------
	} elsif ($self->mode() eq 'cancel') {
        $self->{display} = 'cancel';
	}
 	
	# log user actions that generated warnings, only if warnlevel > 0, which is not default
	if ($self->{displayAlert} ne '') {
		$self->error($self->{displayAlert});
		if ($self->{warnLevel} > 0) {
            $self->_warn($self->{displayAlert}, "x-app was: '" . $self->{app} . "'");
		}
	}

    # --------------------------------------------------------------------------
    # now we're done checking CGI input and loading or saving XML files, we can
    # move on to transforming that XML into some HTML for the user interface
    # --------------------------------------------------------------------------
		
	if ($self->mode() eq '') {
		# no mode so we dont need to do any content transformation as there will
		# only need to be the menuselect dropdown in the UI
		if ($self->{selectEnabled} eq '1') {
			$self->{content} = '<p>Use the menu to Select a record-set to edit</p>';
		} else {
			$self->{content} = '';	# this wouldnt be expected, ie select not enabled and no mode specified
		}

	} elsif ($self->mode() eq 'view') {
		# in view mode, transform the payload and keys
		$self->buildViewContent();

	} elsif ($self->mode() eq 'search') {
		# we're in search mode, check for the search string across all data files
		my @resultRecords; 
		if ($self->{search} ne '') {	# only search if theres a phrase from user
			my @fkeysToSearch = $self->_fkeyValues();
			foreach my $fKey (@fkeysToSearch) {
				$self->{fkey} = $fKey;
				$self->_loadXML();
				my @thisFKeyResultRecords = $self->_searchRecords($self->{search});
	    		foreach my $resultNode (@thisFKeyResultRecords) {
	    			$resultNode->setAttribute('key',$fKey);
					push @resultRecords, $resultNode;
    			}
			}
		}
		$self->_addNullPayload();	# set the root payload to a new empty one
    	foreach my $resultNode (@resultRecords) {
			$self->{dataRootNode}->addChild( $resultNode );
    	}
		$self->buildSearchContent();

	} elsif ($self->mode() eq 'new') {
		# in new fkey mode, transform the payload and keys
		$self->buildEditContent($self->{XSLTDir} . 'edit-fkey.xslt');

	} elsif ($self->mode() =~ /^(add|insert)$/) {
		# adding or inserting a new record
		$self->buildEditContent($self->{XSLTDir} . 'add.xslt');

	} elsif ($self->mode() =~ /^(select|publish|publishall|save|cancel|cut|copy|paste)$/) {
		# in new fkey mode, transform the payload and keys
		$self->buildEditContent($self->{XSLTDir} . 'select.xslt');
		
	} else { 
		# must be in one of the edit modes, add data and config to root node and transform
		$self->buildEditContent($self->{XSLTDir} . 'edit.xslt');
	};

}



# ======================================================================================
=pod

=head1 PRIVATE(ish) SUBS

The following should all only be used internally by the XApperator object itself. They are
called directly by each other or by the three public subs new(), buildMenu() or processActions()

=cut
# ======================================================================================



sub _checkCompulsoryFieldsOK {
# --------------------------------------------------------------------------------------
=pod

=head2 _checkCompulsoryFieldsOK()

check that each of the cgi fields marked as 'required' in the schema in config.xml
has actually been submitted with a value. if any of them is absent will return '0' 
otherwise returns '1'. Is called by processActions()

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    my @fieldNames = @{$self->{fieldNames}};
    foreach my $fieldName (@fieldNames) {
		next unless (exists $self->{requiredFields}->{$fieldName});
        my $thisCGIParamValue = '';
        # check if this is a compound data type (e.g. date) if so need to extract its parts
        if ($self->{fieldTypes}->{$fieldName} eq 'date') {
            $thisCGIParamValue = $self->_getCompoundCGIValue('date', $fieldName);
        } elsif ($self->{fieldTypes}->{$fieldName} eq 'time') {
            $thisCGIParamValue = $self->_getCompoundCGIValue('time', $fieldName);
        } else {
            $thisCGIParamValue = $self->{cgi}->param( $fieldName );
        }
        if ($thisCGIParamValue eq '') {
            return 0;
        }
    }
    return 1;
}



# --------------------------------------------------------------------------------------

sub _checkLang {
=pod

=head2 _checkLang()

Takes the $self->{acceptLang} paramater and checks if its in a list of expected languages,
if so the self->{lang} attrib is set and ultimately passed into the UI transform to indicate
which language set of UI messages should be used. $self->{acceptLang} itself is set in
_parseCGIParams() by taking the 'Accept-Language' HTTP header value the browser has
volunteered.

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    my $langAccept = $self->{acceptLang};
    my @availLangs = qw/ en de mi /;
    my @acceptedLangs = split(/,/, $langAccept);
    $self->{lang} = 'en';    # default
    # check that browser accept-language val is one of the langs we support
    foreach my $thisLang (@acceptedLangs) {
        foreach my $availLang (@availLangs) {
            $self->{lang} = $thisLang if ($thisLang eq $availLang);
        }
    }
}


sub _configFilePath {
# --------------------------------------------------------------------------------------
=pod

=head2 _configFilePath()

returns the file system path to the config.xml file for the application currently being processed. 
Path is required in multiplae places so makes sense to have a method for it. Is based on the 'rootDir'
and 'app' properties

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    my $path = $self->{rootDir} . $self->{appDir} . $self->{app} . '/' . $self->{configFile};
    return $path;
}


sub _dataPayloadReqd {
# --------------------------------------------------------------------------------------
=pod

=head2 _dataPayloadReqd()

returns true if we're in a mode that will require loading of a data file. turns
out this is most modes except publish.

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    if ( (($self->{type} eq 'keys') and (!($self->mode() =~ /select|publish|publishall|view|xml|search/)))
            or ($self->{fkey} eq '') ) {
        return 0;
    } else {
        return 1; 
    }
}


sub _fieldTypesOK {
# --------------------------------------------------------------------------------------
=pod

=head2 _fieldTypesOK()

check the value submitted for each field and make sure it matches the type
defined in config.xml. if any dont match then set the property $self->displayError
which the XSLT will display to the user, and return '0' to processActions()

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    my @fieldNames = @{$self->{fieldNames}};
    my $brokenField = '';
    foreach my $fieldName (@fieldNames) {
        my $thisCGIParamValue = '';
        # check if this is a compound data type (e.g. date) if so need to extract its parts
        if ($self->{fieldTypes}->{$fieldName} eq 'date') {
            $thisCGIParamValue = $self->_getCompoundCGIValue('date', $fieldName);
            if (($thisCGIParamValue ne '') && 
					!($thisCGIParamValue =~ /^[0-9]{4}-?[0-3]{1}[0-9]{1}-?[0-3]{1}[0-9]{1}$/)) {
				$self->{displayError} .= "$thisCGIParamValue is not a valid date value for $fieldName";
				$self->_warn('Invalid date', "date was: '" . $thisCGIParamValue . "'");
                return 0;
            }
        } elsif ($self->{fieldTypes}->{$fieldName} eq 'time') {
            $thisCGIParamValue = $self->_getCompoundCGIValue('time', $fieldName);
            if (($thisCGIParamValue ne '') && !($thisCGIParamValue =~ /^[0-2]{1}[0-9]{1}[0-5]{1}[0-9]{1}$/)) {
                $self->{displayError} .= "$thisCGIParamValue is not a valid time value for $fieldName";
				$self->_warn('invalid time', "time was: '" . $thisCGIParamValue . "'");
                return 0;
            }
        } elsif ($self->{fieldTypes}->{$fieldName} eq 'text') {
            # for text/string types just want to know hasnt exceeded max length
            $thisCGIParamValue = $self->{cgi}->param( $fieldName );
            my $thisFieldSize = (defined $self->{fieldSizes}->{$fieldName}) ? $self->{fieldSizes}->{$fieldName} : 0;
            if (
                ($thisFieldSize > 0) &&
                (length($thisCGIParamValue) > $thisFieldSize)
               ){
                $self->{displayError} .= "$fieldName is too long, maximum length is " .
                    $thisFieldSize;
                return 0;
            }
        } else {
            # some unknown type, or we dont care
        }
    }
    return 1;
}




sub _getIndexFieldValue {
# --------------------------------------------------------------------------------------
=pod

=head2 _getIndexFieldValue()

Returns the submitted CGI value of what should be the index field for the record being
processed. Checks to see if is a compound field type (e.g. date) and if so calls 
_getCompoundCGIValue() to extract and assemble it.

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    # check if its a compound field, like a date
    if ($self->{indexFieldType} eq 'date') {
        $self->{indexFieldValue} = $self->_getCompoundCGIValue('date', $self->{indexFieldCGIVar});
    } else {
    	my $fieldValue = $self->{cgi}->param($self->{indexFieldCGIVar}) || '';
		# [?] need to escape the apos/quotes as they may break xpath queries on the index value
		# avoids the 'invalid predicate' error, parser converts them to normal single byte
		# vals when it reads them from file
		#$fieldValue =~ s/'/&apos;/g;
		#$fieldValue =~ s/"/&quot;/g;
		$fieldValue =~ s/'//g;
		$fieldValue =~ s/"//g;
        $self->{indexFieldValue} = $fieldValue;
    }
}



sub _getCompoundCGIValue {
# --------------------------------------------------------------------------------------
=pod

=head2 _getCompoundCGIValue()

Some compound field types (e.g. date) are able to be submitted as seperate entities
in the HTML FORM. Each field part is prefixed with the final field name and has
a date specific type appended. e.g. for a field named 'birthday' the actual
submitted CGI fields would be 'birthday:year', 'birthday:month' and 'birthday:day'.
This method takes the fields type (e.g. date) and final paramater name (e.g. 'birthday')
and returns the values of each of the compound parts found in the CGI submission
as a single string. If the type isnt recognised the result will be ''

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    my $paramType = shift;    # e.g. 'date'
    my $paramName = shift;    # e.g. 'birthday'
    if ($paramType eq 'date') {
		if ($self->{dateEntryFormat} eq 'ISO8601') {	# in this case the dates really arent compound so is one field not three
			return $self->{cgi}->param($paramName);
		} else {
			return $self->{cgi}->param( $paramName . ':year') . '-' .
                $self->{cgi}->param( $paramName . ':month') . '-' .
                $self->{cgi}->param( $paramName . ':day');
		}
    } elsif ($paramType eq 'time') {
        return $self->{cgi}->param( $paramName . ':hour') .
                $self->{cgi}->param( $paramName . ':min');
    }
    return '';
}




sub _parseConfig {
# --------------------------------------------------------------------------------------
=pod

=head2 _parseConfig()

Grab all the meta data and config data for the app requested, extracts the configuration
paramaters using a series of LibXML findvalue() XPath calls. The config.xml file itself
is located and parsed after a call to _configFilePath(). Returns '0' unless the config.xml file exists.
Checks if the titles or messages elements of the config file refer to external message libraries
(using their 'src' attributes) in which case those files are also parsed and added to the
applicaction configRootNode. This is called by the new() constructor only.

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    eval { $self->{configData} = $self->{parser}->parse_file( $self->_configFilePath() ); };
	if ($@) { $self->dieNice('Failed to parse config file', $self->_configFilePath()); }
    $self->{configRootNode} = $self->{configData}->documentElement();
    $self->{description} = $self->{configRootNode}->findvalue( '/config/meta/description' );
    $self->{author} = $self->{configRootNode}->findvalue( '/config/meta/author' ) || '';
    $self->{title} = $self->{configRootNode}->findvalue( "/config/meta/title[lang('$self->{lang}')]" ) ||
    		$self->{configRootNode}->findvalue( "/config/meta/title" );
    $self->{method} = $self->{configRootNode}->findvalue( '/config/actions/method' ) || 'POST';
    $self->{addFKeyEnabled} = $self->{configRootNode}->findvalue( '/config/actions/addfkey/@enabled' ) || 0;
    $self->{addEnabled} = $self->{configRootNode}->findvalue( '/config/actions/add/@enabled' ) || 0;
    $self->{copyPasteEnabled} = $self->{configRootNode}->findvalue( '/config/actions/copypaste/@enabled' ) || 0;
	$self->{selectEnabled} = '1';	# default should be 1, this is an optional element
	if ($self->{configRootNode}->findnodes('/config/actions/select')) {
	    $self->{selectEnabled} = $self->{configRootNode}->findvalue( '/config/actions/select/@enabled' );
	}
	$self->{editEnabled} = $self->{configRootNode}->findvalue( '/config/actions/edit/@enabled' ) || 0;
	$self->{editPopupMode} = $self->{configRootNode}->findvalue( '/config/actions/edit/@popup' ) || 0;
	$self->{insertEnabled} = $self->{configRootNode}->findvalue( '/config/actions/insert/@enabled' ) || 0;
    $self->{deleteEnabled} = $self->{configRootNode}->findvalue( '/config/actions/delete/@enabled' ) || 0;
    $self->{deleteFKeyEnabled} = $self->{configRootNode}->findvalue( '/config/actions/deletefkey/@enabled' ) || 0;
    $self->{viewEnabled} = $self->{configRootNode}->findvalue( '/config/actions/view/@enabled' ) || 0;
    $self->{xmlEnabled} = $self->{configRootNode}->findvalue( '/config/actions/xml/@enabled' ) || 0;
    $self->{viewXSLTFile} = $self->{configRootNode}->findvalue( '/config/actions/view/@transform' ) || 'view.xslt';
    $self->{searchEnabled} = $self->{configRootNode}->findvalue( '/config/actions/search/@enabled' ) || 0;
    $self->{searchXSLTFile} = $self->{configRootNode}->findvalue( '/config/actions/search/@transform' ) || 'search.xslt';
    $self->{publishEnabled} = $self->{configRootNode}->findvalue( '/config/actions/publish/@enabled' ) || 0;
    $self->{publishAllEnabled} = $self->{configRootNode}->findvalue( '/config/actions/publish/@all-enabled' ) || 0;
    $self->{publishXSLTFile} = $self->{configRootNode}->findvalue( '/config/actions/publish/@transform' ) || '';
    $self->{publishExtension} = $self->{configRootNode}->findvalue( '/config/actions/publish/@extension' ) || 'xml';
    $self->{publishFolder} = $self->{configRootNode}->findvalue( '/config/actions/publish/@folder' ) || '';
    $self->{publishTransforms} = $self->{configRootNode}->findvalue( 'count(/config/actions/publish/transform)' ) || 0;
    $self->{fkeyPath} = $self->{rootDir} . $self->{appDir} . $self->{app} . '/';
    $self->{fkeyPath} .= $self->{configRootNode}->findvalue( '/config/schema/groupby/@src' );
    $self->{rootName} = $self->{configRootNode}->findvalue( '/config/schema/rootname' ) || 'UNKNOWN';
    $self->{recordName} = $self->{configRootNode}->findvalue( '/config/schema/recordname' ) || 'UNKNOWN';
    $self->{recordsXPath} = "/" . $self->{rootName} . "/" . $self->{recordName};
    $self->{indexFieldName} = $self->{configRootNode}->findvalue( '/config/schema/fields/field[@isindex = 1][1]/@name' );
	if ($self->{indexFieldName}) {
		$self->{indexFieldCGIVar} = $self->{indexFieldName};
		$self->{indexFieldType} = $self->{configRootNode}->findvalue( '/config/schema/fields/field[@isindex = 1][1]/@type' );
	} else {
		$self->{indexFieldCGIVar} = 'xapp:id';
		$self->{indexFieldName} = 'xapp:id';
		$self->{indexFieldType} = 'xapp:id';
	}
	$self->{minOccurs} = $self->{configRootNode}->findvalue( '/config/schema/minOccurs' ) || 0;
    $self->{maxOccurs} = $self->{configRootNode}->findvalue( '/config/schema/maxOccurs' );
    $self->{validation} = $self->{configRootNode}->findvalue( '/config/schema/clientsidevalidation' ) || '';

	# now get all the field types 
    my @fieldNodes = $self->{configRootNode}->findnodes( '/config/schema/fields/field' );
    my $fieldNames = [];    # keep a list/array of field names, will use later
    my $fieldTypes = {};    # keep a hash of field->types
    my $requiredFields = {};    # keep a hash indicating whether each field is compulsory
    my $searchFields = {};    # keep a hash indicating whether each field is searchable
    my $fieldSizes = {};    # keep a hash of field->size's
    foreach my $fieldNode (@fieldNodes) {
        if ($fieldNode->hasAttributes()) {
            my $thisFieldName = '';
            # TODO - need to do this pass-thru once, instead of 4-times
            # pass thru the 1st time to get the name attribute
            foreach my $attrib ($fieldNode->attributes) {
                if ($attrib->name eq 'name') {
                    push @{$fieldNames}, $attrib->value;
                    $thisFieldName = $attrib->value;
                }
            }
            # now pass thru to get the type attribute based on name
            foreach my $attrib ($fieldNode->attributes) {
                if ($attrib->name eq 'type') {
                    $fieldTypes->{$thisFieldName} = $attrib->value;
                }
            }
            # now pass thru to get the size attribute
            foreach my $attrib ($fieldNode->attributes) {
                if ($attrib->name eq 'maxlength') {
                    $fieldSizes->{$thisFieldName} = $attrib->value;
                }
            }
            # now pass thru to get the 'required' attribute which we'll use for validation
            foreach my $attrib ($fieldNode->attributes) {
                if (($attrib->name eq 'required') and ($attrib->value eq '1')) {
                    $requiredFields->{$thisFieldName}++;
                }
            }
            # now pass thru to get the 'searchable' attribute which we'll use for validation
            foreach my $attrib ($fieldNode->attributes) {
                if (($attrib->name eq 'searchable') and ($attrib->value eq '1')) {
                    $searchFields->{$thisFieldName}++;
                }
            }
        }
    }
    $self->{fieldNames} = $fieldNames;
    $self->{fieldTypes} = $fieldTypes;
    $self->{fieldSizes} = $fieldSizes;
    $self->{requiredFields} = $requiredFields;
    $self->{searchFields} = $searchFields;
    # get some of the messages we need here in the module, which we may use or modify before XSLTing
    $self->{promptMenu} = $self->{configRootNode}->findvalue( "/config/messages/menu" ) || '';
    $self->{promptMenuEmpty} = $self->{configRootNode}->findvalue( "/config/messages/menuempty" ) || '';
    return 1;
}


sub _loadUIresource {
# --------------------------------------------------------------------------------------
=pod

=head2 _loadUIresource()

support internationalisation by loading lanugage specific 'messages' and 'titles' 
element resource files if specified in a 'src' attribute

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
	my $resourceType = shift;	# 'titles' or 'messages'

	my $folderPath = '';
	# if resourcesDir is relative (ie doesnt start with '/') then to 
	# prepend filePath with rootDir + resourcesDir
	if ($self->{resourcesDir} =~ /^[^\/]/) {
	    $folderPath = $self->{rootDir} . $self->{resourcesDir};
	# else if it's absolute (ie starts with '/') then use its raw value 
	} elsif ($self->{resourcesDir} =~ /^\//) {
		$folderPath = $self->{resourcesDir};
	}

	# use the resourc file specified in the config file...
    my $filePath = $self->{configRootNode}->findvalue( '/config/' . $resourceType . '/@src' )
		# or default to the current default user/browser language
    	 || $resourceType . '-' . $self->{lang} . '.xml';
	my $resourcePath = $folderPath . $filePath;

	if (-e $resourcePath) {
		my $resourceDoc;
		eval { $resourceDoc = $self->{parser}->parse_file($resourcePath); };
		if ($@) { $self->dieNice('failed to parse resource file', $resourcePath); }
		my $resourceRootNode = $resourceDoc->documentElement->find('/' . $resourceType)->get_node(1);
		my $configNodePoint = $self->{configRootNode}->find('/config');
		my $attachNode = $configNodePoint->get_node(1);
		$attachNode->appendChild($resourceRootNode);
	} else {
		$self->dieNice("Cant find resource file specified in config", $resourcePath);
	}
 
    return 1;
}


sub _parseCGIParams {
# --------------------------------------------------------------------------------------
=pod

=head2 _parseCGIParams()

Called by the new() constructor to get all the expected controller script CGI params.
These are directives used by the controller.cgi itself and not the data releated CGI
fields that may have been submitted through an edit FORM. XApp field names are
differentiated from user data fields by using a prefix of 'x-'. 

Each field's purpose is:

 x-app         # id of the selected application
 x-mode        # edit, delete, update, publish, save, view, xml
 x-pmode       # paste mode, ie cut or copy
 x-confirm     # used to confirm deletions
 x-label       # the label for a new foreign key
 x-value       # the value for a new foreign key
 x-type        # whether we're manipulating 'keys' or 'records'
 x-oldkey      # in case we're in edit mode and key has changed
 x-insertkey   # in case we're in insert mode, need to know where
 x-rootinsert  # in case we're in insert at docroot mode
 x-search      # text string to search for
 x-sortorder   # used when listing records, vals 'ascending' or 'descending'
 x-sortby      # field to sort by when listing records
 x-fkey        # foreign key grouping records
 x-key         # key field value of the record being edited
 x-tkey        # temp key field value, used to ref to record in copy/paste modes

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    my $q = $self->{cgi};
    $self->{app} = $self->{app} || $q->param('x-app');
    $self->{app} = $self->_sanitisePath($self->{app});
    $self->{mode} = lc($q->param('x-mode')) || '';
    $self->{mode} =~ s/[^a-z]//;    # remove spaces (e.g. from 'Publish All')
    $self->{pmode} = lc($q->param('x-pmode')) || '';   
    $self->{confirm} = lc($q->param('x-confirm')) || '';
    $self->{label} = (defined $q->param('x-label')) ? $q->param('x-label') : '';    
    $self->{value} = lc($q->param('x-value')) || 'records';    
    $self->{type} = lc($q->param('x-type')) || '';    
    $self->{oldkey} = (defined $q->param('x-oldkey')) ? $q->param('x-oldkey') : '';   
    $self->{newkey} = '';	# used for the id of a new record when copy/pasted    
    $self->{insertkey} = (defined $q->param('x-insertkey')) ? $q->param('x-insertkey') : '';  
    $self->{rootInsert} = (defined $q->param('x-rootinsert')) ? $q->param('x-rootinsert') : '';  
    $self->{fkey} = (defined $q->param('x-fkey')) ? $q->param('x-fkey') : '';       
    $self->{key} = (defined $q->param('x-key')) ? $q->param('x-key') : '';        
	$self->{key} =~ s/'//;
    $self->{tkey} = (defined $q->param('x-tkey')) ? $q->param('x-tkey') : '';        
	$self->{acceptLang} = $q->http('Accept-language')  || '';    # what languages browser accepts
    $self->{suppressmenu} = (defined $q->param('x-suppressmenu')) ? $q->param('x-suppressmenu') : '';        
    $self->{sortOrder} = (defined $q->param('x-sortorder') && ($q->param('x-sortorder') eq 'descending')) ? 'descending' : 'ascending';        
    $self->{sortBy} = (defined $q->param('x-sortby')) ? $q->param('x-sortby') : '';        
    $self->{search} = (defined $q->param('x-search')) ? $q->param('x-search') : '';        
}

=pod

=head1 SEE ALSO

L<XML::LibXSLT>, L<XML::LibXML>, XApperator, XApperator::Model, XApperator::View, XApperator::Utils  
L<http://xapperator.sourceforge.net/>

=cut

1
