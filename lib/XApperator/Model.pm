package XApperator::Model;

=pod

=head1 NAME

XApperator::Model - Model related functions for the XApperator MVC framework

=head1 VERSION

B<1.0.1>

This Release: 24/Sep/2007
Original Release: 20/Feb/2007

=head1 AUTHOR

Dean Stringer (deeknow @ pobox . com)

=head1 DESCRIPTION

This module contains all data layer access methods for reading or modifying the 
contents of application's records. None of these is intended to be called
directly, and are instead used mosly by an instantiated XApperator object's
controller methods, e.g processActions() 

=cut

# --------------------------------------------------------------------------------- 


# --------------------------------------------------------------------------------- 
BEGIN {
use vars qw($VERSION @EXPORT @ISA);
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(_addForeignKey _addNullPayload _addRecord _createRecord _dataPath 
	_deleteFKey _deleteRecord _keyExists _loadXML _pasteRecord _recordCount
	_fkeyLabel _fkeyExists _fkeyValOK _fkeyValues _parseFKeyData _insertRecord _schemaHasFieldType _selectNextRecordID
	_searchRecords _updateXML _updateRecord _updateFKeyData);
$VERSION = '1.0.1';
}




# ======================================================================================

=pod

=head1 PRIVATE(ish) SUBS

=cut

# ======================================================================================



sub _addForeignKey {
# --------------------------------------------------------------------------------------
=pod

=head2 _addForeignKey()

Adds a new foreign key record. In XApperator records are grouped together in seperate files with
the filename acting as a kind of foreign key. Each file is in turn listed in a KEYS.xml file.
_addForeignKey() adds a new file to the filesystem with a skeleton XML parent element and updates
the KEYS.xml file with a label and filename. If the user hasnt supplied a value (filename) or
label then we should have trapped that in processActions() before this was called.

=cut
    my $self = shift;
    my $newKey = $self->{applicationDoc}->createElement ( 'key' );
    my $label = $self->{applicationDoc}->createElement ( 'label' );

    my $textNodeLabel = $self->{applicationDoc}->createTextNode( $self->{label} );
    $label->addChild( $textNodeLabel );
    my $value = $self->{applicationDoc}->createElement ( 'value' );
    my $textNodeValue = $self->{applicationDoc}->createTextNode( $self->{value} );
    $value->addChild( $textNodeValue );
    $newKey->addChild( $value );
    $newKey->addChild( $label );
    $self->{fkeyRootNode}->addChild( $newKey );
    #print $self->{fkeyRootNode}->toString; exit;
    $self->_updateFKeyData();
    $self->buildMenu(); # need to rebuild this so can display updated fkey list to user

    # now create empty data file for it
    my $doc = XML::LibXML->createDocument;
    my $rootNode = $doc->createElementNS( '', $self->{rootName}  );
    $doc->setDocumentElement( $rootNode );
	# add the 'xapp' namespace declaration/attribute
	my $XAppNSattr = $doc->createAttributeNS( "", "xmlns:xapp", "http://xapperator.sourceforge.net/" );
	$rootNode->setAttributeNodeNS($XAppNSattr);

    $self->{fkey} = $self->{value};
    my $outFile = $self->_dataPath();
    $doc->toFile($outFile, 1) || $self->dieNice("Cant update $outFile");
}


sub _addNullPayload {
# --------------------------------------------------------------------------------------
=pod

=head2 _addNullPayload()

Creates a null/empty root data payload that consists of just the rootName
element with no children. This is so the XSLT has something in the data
element to look at and test.

=cut
    my $self = shift;
    my $doc = XML::LibXML->createDocument;
    my $rootNode = $doc->createElement( $self->{rootName}  );
    $doc->setDocumentElement( $rootNode );
    $self->{dataRootNode} = $doc->documentElement();
}



sub _addRecord {
# --------------------------------------------------------------------------------------
=pod

=head2 _addRecord()

Add a new record. We call _createRecord() to actually assemble the new record as an XML element
then we call addChild to add it to the main datafile root element then we update the XML file itself

=cut
    my $self = shift;
    my $newRecord = $self->_createRecord();
    $self->{dataRootNode}->addChild( $newRecord );
    $self->_updateXML($self->{dataRootNode});
}



sub _createRecord {
# --------------------------------------------------------------------------------------
=pod

=head2 _createRecord()

Takes care of building a new blank data record by checking the schema element from config.xml to 
see which fields are required, and adding a child element using the name specified in the schema
and giving it a textnode value of a CGI param matching that name. Is called by _addRecord() and
_insertRecord().

=cut

    # build and return a new record using passed CGI params
    my $self = shift;
    my $newRecord = $self->{applicationDoc}->createElement ( $self->{recordName} );
	if ($self->{indexFieldName} eq 'xapp:id') {
		my $nextID = $self->_selectNextRecordID();
		my $textNode = $self->{applicationDoc}->createTextNode( $nextID );
		my $newNode = $self->{applicationDoc}->createElement( 'xapp:id' );
        $newNode->addChild( $textNode );
		$newRecord->addChild( $newNode );
	}
	# for each field we got from the config file...
    foreach my $nodeName (@{$self->{fieldNames}}) {
        # create an element of the same name using the matching cgi paramater
        my $newNode = $self->{applicationDoc}->createElement( $nodeName );
		my $textContent = '';
	    # check if this is a compound data type (e.g. date) if so need to extract its parts
        if ($self->{fieldTypes}->{$nodeName} eq 'date') {
            $textContent = $self->_getCompoundCGIValue('date', $nodeName);
        } elsif ($self->{fieldTypes}->{$nodeName} eq 'time') {
            $textContent = $self->_getCompoundCGIValue('time', $nodeName);
        } elsif ($self->{fieldTypes}->{$nodeName} eq 'checkbox') {
			$textContent = ((defined $self->{cgi}->param( $nodeName )) && ($self->{cgi}->param( $nodeName ) eq 'on')) ? '1' : '0';
        } else {
            $textContent = $self->{cgi}->param( $nodeName ) || '';
        }
        #warn "VAL=" . $textContent;
        my $textNode = $self->{applicationDoc}->createTextNode( $textContent );
        $newNode->addChild( $textNode );
        $newRecord->addChild( $newNode );
    }
    return $newRecord;
}


sub _dataPath {
# --------------------------------------------------------------------------------------
=pod

=head2 _dataPath()

returns the file system path to the data xml file for the application and recordset currently
being processed. Path is required in multiple places so makes sense to have a method for it. 
Is based on the 'rootDir', 'appDir', 'dataDir' and 'fkey' properties

=cut
    my $self = shift;
    return $self->{rootDir} . $self->{appDir} . $self->{app} . '/' . $self->{dataDir} .
        $self->{fkey} . '.xml'; 
}


sub _deleteFKey {
# --------------------------------------------------------------------------------------
=pod

=head2 _deleteFKey()

Called by processActions() when we want to delete a foreign key. Involves removing
the key record from the fkeyRootNode and writing that back out to the keys file
(using _updateFKeyData() and finally deleting the actual data file associated
with the fkey. Returns '0' if either of those actions fail

=cut
    my $self = shift;
    my $fileName = $self->_dataPath();
    if ($self->_fkeyValOK($self->{fkey})) {
        my $XPath = "//foreignkeys/key[value = '$self->{fkey}']";
        my $keyNodes = $self->{fkeyRootNode}->findnodes($XPath);
        my $keyNode = $keyNodes->get_node(1);
        $keyNode->unbindNode();
        return 0 unless $self->_updateFKeyData();
        $self->buildMenu(); # need to rebuild this so can display updated fkey list to user
        return 0 unless unlink($fileName);   # unlink returns count of files deleted
        return 1;
    } else {
        return 0;
    }
}


sub _deleteRecord {
# --------------------------------------------------------------------------------------
=pod

=head2 _deleteRecord()

Deletes a record given an index field value. Steps through all the records using the 
record xpath extracted from elements in config.xml to find the record and returns '1'
if the record was found and the file updated successfully. Otherwiser returns '0'.
Is called by processActions() which first checks that we have confirmed we really
want to delete this thing.

=cut
    my $self = shift;
    my @records = $self->{dataRootNode}->findnodes( $self->{recordsXPath}  );
    foreach my $record (@records) {
        my @fields = $record->findnodes( '*');
        foreach my $field (@fields) {
            # step thru each field of the record and if this is the index field
            # and its value matches the index value we've been asked to delete
            if ($field->nodeName eq $self->{indexFieldName} and
                ($field->textContent eq $self->{oldkey}) ) {
                    $record->unbindNode();
                    $self->_updateXML($self->{dataRootNode});
                    return 1;
            }
        }
    }
    return 0;
}


sub _fkeyExists {
# --------------------------------------------------------------------------------------
=pod

=head2 _fkeyExists()

Check that user hasn't tried to save a new record collection that has the same
foreign key as an existing one, do this by counting the no of existing keys,
error if more than '0'.

=cut
    my $self = shift;
    my $indexValue = shift;
    my $indexCheckXPath = "count(//foreignkeys/key[value = '$indexValue'])";
    #print $indexCheckXPath;
    my $indexCount = $self->{fkeyRootNode}->findvalue($indexCheckXPath);
    if ($indexCount > 0) { return 1; }
    else { return 0; }
}

sub _fkeyLabel {
# --------------------------------------------------------------------------------------
=pod

=head2 _fkeyLabel()

Return the text for the label node of a key in KEYS.xml which has a value node with the
text specified in the 1st argument to this method.

=cut
    my $self = shift;
    my $keyValue = shift;
    my $XPath = "//foreignkeys/key[value = '$keyValue']/label";
    my $keyLabel = $self->{fkeyRootNode}->findvalue($XPath);
    return $keyLabel;
}

sub _fkeyValOK {
# --------------------------------------------------------------------------------------
=pod

=head2 _fkeyValOK()

used to sanitise a fkey value. these vals are used to reference file-system
resources so will return '0' if the passed key value contains any chars other
than 0-9, A-Z, - or _

=cut
    my $self = shift;
    my $keyVal = shift; # might need to test vals other than $self->{key} so accept as passed val
    if ($keyVal =~ /[^0-9a-z\-_]/) {
        return 0;
    }
    return 1;
}

sub _keyExists {
# --------------------------------------------------------------------------------------
=pod

=head2 _keyExists()

Check that user hasn't tried to save a new record that has the same key
as an existing one, do this by counting the no of existing keys, error if
more than '0'.

=cut
    my $self = shift;
    my $indexValue = shift;
    my $indexCheckXPath = "count(//" . $self->{recordName} . "[" . $self->{indexFieldName} .
        " = '" . $indexValue . "'])";
    my $indexCount = $self->{dataRootNode}->findvalue($indexCheckXPath);
    if ($indexCount > 0) { return 1; }
    else { return 0; }
}




sub _insertRecord {
# --------------------------------------------------------------------------------------
=pod

=head2 _insertRecord()

Insert a new record into the dataset. Calls _createRecord() to actually build the new record
then checks to see *where* we've been asked to insert it. Could be that its a rootInsert
which means find the 1st record and insert before it. Otherwise its a findInsert which means
we look for a record with a certain indexFieldValue and add the new record before that.
Whatever the insert mode we eventually make a call to _updateXML() to save the changes

=cut
    my $self = shift;
    my $newRecord = $self->_createRecord();
    # if we've been asked to insert at root level
    if ( $self->{rootInsert} || 
        # or there's no insert location specified
        ($self->{insertkey} eq '') ||
        # or the specified key doesnt exist
        (! $self->_keyExists($self->{insertkey})) ) {
        # then insert a new record at the top of the document
        my $childnode = $self->{dataRootNode}->firstChild;
        $self->{dataRootNode}->insertBefore($newRecord, $childnode);
    } else {
        # find the record that we've been asked to insert before
        my $xPath = "//" . $self->{recordName} . "[" . $self->{indexFieldName} .
            " = '" . $self->{insertkey} . "']";
        # assume findnodes will succeed coz keyExists didnt find it above
        # or we were asked to insert before a specific key
        my $insertBefore = $self->{dataRootNode}->findnodes($xPath)->get_node(1);
        # and insert our new record before it
        $self->{dataRootNode}->insertBefore($newRecord, $insertBefore);
    }
    $self->_updateXML($self->{dataRootNode});
}





sub _loadXML {
# --------------------------------------------------------------------------------------
=pod

=head2 _loadXML()

Loads the XML file returned by a call to _dataPath() and parses its contents into a
DOM object which is then set as the 'dataRootNode' for the current application. Calls
_dieNice() if we cant find the file. This is called by processActions() only.

=cut
    my $self = shift;
    my $inFile = $self->_dataPath();
	unless (-e $inFile) { $self->dieNice('Cant find source xml file', $inFile); }
    my $data;
	eval {$data = $self->{parser}->parse_file($inFile); };
	if ($@) { $self->dieNice('failed to parse xml data file' . $@, $inFile); }
	$self->{dataRootNode} = $data->documentElement();
	#$self->_debug( $self->{dataRootNode}->toString );
	return 1;
}


sub _pasteRecord {
# --------------------------------------------------------------------------------------
=pod

=head2 _pasteRecord()

Insert a new record into the dataset by copying or cutting the record specified by the CGI
param 'tkey' and inserting it before the record whose ID is specified by CGI param 'key'.
Uses the DOM cloneNode method to copy the new record. The new records ID field is generated
by postfixign it with the string '(n)'. Makes a call to _updateXML() after
inserting it to save the changes.

=cut
    my $self = shift;
    # find the record that we've been asked to 
    my $xPath = "//" . $self->{recordName} . "[" . $self->{indexFieldName} .
        " = '" . $self->{tkey} . "']";
    # assume findnodes will succeed coz keyExists didnt find it above
    my $findNode = $self->{dataRootNode}->findnodes($xPath)->get_node(1);
    my $newRecord = $findNode->cloneNode( '1' );    # '1' = deep copy, ie all child elements

    # if we're in cut/paste mode, need to delete the one we're moving
    if ($self->{pmode} eq 'cut') {
        $findNode->unbindNode();
    }

    # if we're in copy/paste mode we'll need a new unique index field value
    if ($self->{pmode} eq 'copy') {
        # change the index field name of our new record to something else
        # so we dont collide with the original record
        my $indexFieldNode = $newRecord->findnodes("$self->{indexFieldName}")->get_node(1);
        # create a new replacement index field node
        my $replacementIndexNode = $self->{applicationDoc}->createElement($self->{indexFieldName});
		
		if ($self->{indexFieldName} eq 'xapp:id') {
			# is an auto-generated one so get the next id
			$self->{newkey} = $self->_selectNextRecordID();
		} else {
	        # is a textual one, so give it a new value postfixing copied node with the txt '(n)'
    	    # but make sure we dont already have a copy, in which case we bump up the count prefix
        	my $copyIndex = 1;
	        $self->{newkey} =  $self->{tkey} . '(' . $copyIndex . ')'; # name format change
    	    while ($self->_keyExists($self->{newkey})) {
        	    $copyIndex++;
            	$self->{newkey} = $self->{tkey} . '(' . $copyIndex . ')';
        	}
		}
        my $textNode = $self->{applicationDoc}->createTextNode($self->{newkey});
        $replacementIndexNode->addChild($textNode);
        # now replace the old index field with the new one
        $indexFieldNode->replaceNode($replacementIndexNode);
    }

    # find the record that we've been asked to paste after
    $xPath = "//" . $self->{recordName} . "[" . $self->{indexFieldName} .
        " = '" . $self->{key} . "']";
	my $findResult = $self->{dataRootNode}->findnodes($xPath);
	if ($findResult->size() == 1) {
	    $self->{dataRootNode}->insertAfter($newRecord, $findResult->get_node(1));
	} else {
		# if no node found to insert after we just pass undef to insertAfter and the
		# node is libxml adds it as the last child of the parent
		$self->_warn("no node found for xpath: " . $xPath);
	    $self->{dataRootNode}->insertAfter($newRecord, undef);
	}
    $self->_updateXML($self->{dataRootNode});
}

sub _fkeyValues {
# --------------------------------------------------------------------------------------
=pod

=head2 _fkeyValues()

Search the /foreignkeys XML for key elements and return all of the key
values in a list. This is used by _searchRecords to be able to determine
the key values for all XML data files which need to be loaded and searched 

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
	my @fKeyValues;
	my @findResult = $self->{fkeyRootNode}->findnodes('//foreignkeys/key');
	foreach my $keyNode (@findResult) {
		my $keyValue = $keyNode->findvalue('value');
		push @fKeyValues,$keyValue;
	}
	return @fKeyValues;
}
	
sub _parseFKeyData {
# --------------------------------------------------------------------------------------
=pod

=head2 _parseFKeyData()

Parse the foreign keys file (normally KEYS.xml) and point the application's
'fkeyRootNode' at the documentElement for the Keys file. At the same time
we fetch the foreign keys label value from the @label attribute of the
document element in KEYS.xml. This is called by the new() constructor only.

=cut
    my $self = shift;
    eval { $self->{fkeyData} = $self->{parser}->parse_file( $self->{fkeyPath} ); };
	if ($@) { $self->dieNice('Failed to parse foreign key file', $self->{fkeyPath}); }
    $self->{fkeyRootNode} = $self->{fkeyData}->documentElement();
    $self->{fkeyLabel} = $self->{fkeyRootNode}->findvalue( '/foreignkeys/@label' );
    return 1;
}

sub _recordCount {
# --------------------------------------------------------------------------------------
=pod

=head2 _recordCount()

Returns the current number of records in the dataRootNode using recordsXPath.
Is called when checking if adding or deleting records falls within the config.xml
settings for max or min numbers of records.

=cut
    my $self = shift;
    return $self->{dataRootNode}->findvalue( "count($self->{recordsXPath})" );
}




sub _schemaHasFieldType {
# --------------------------------------------------------------------------------------
=pod

=head2 _schemaHasFieldType()

returns '1' if any fields of type passed as 1st paramater exist in the schema, otherwise
returns 0

=cut

	my $self = shift;
	my $fieldTypeToFind = shift;
	foreach my $fieldName (@{$self->{fieldNames}}) {
		if ($self->{fieldTypes}->{$fieldName} eq $fieldTypeToFind) {
			return 1;
		}
	}
	return 0;
}


sub _searchRecords {
# --------------------------------------------------------------------------------------
=pod

=head2 _searchRecords()

Search the currently loaded XML paylod for a passed search string. Return a list
of nodes which contain portions of that string in fields which are configured
as 'searchable' 

=cut
    my $self = shift;
    my $searchText = shift;
    my @searchWords = split(' ', $searchText);
    my @matchingRecords;	# will return a nodelist of records matching the passed text
    my @records = $self->{dataRootNode}->findnodes( $self->{recordsXPath}  );
    RECORDS: foreach my $record (@records) {
        my @fields = $record->findnodes( '*');	# check each child element of current record
        foreach my $field (@fields) {
        	# if this field is a 'searchField'...
			if (exists $self->{searchFields}->{$field->nodeName}) {
		        SEARCHWORDS: foreach my $searchWord (@searchWords) {
					if ($field->textContent =~ /$searchWord/i) {
						push @matchingRecords, $record;
						next RECORDS;
					}
		        }
			}
		}
	}
	return @matchingRecords;
}

sub _selectNextRecordID {
# --------------------------------------------------------------------------------------
=pod

=head2 _selectNextRecordID()

generate an ID for a new record

=cut
	my $self = shift;
	my $nextID = $self->_recordCount() + 1;		# start with this
	# return that as long as it doesnt exist (which it might if records have been deleted)
	return $nextID unless $self->_keyExists($nextID); 
	my $idsXpath = $self->{recordsXPath} . '/' . $self->{indexFieldName};
	#warn "KEY PATH=" .  $idsXpath;
	foreach my $recordNode ($self->{dataRootNode}->findnodes($idsXpath)) {
		# step through each record
		if ($recordNode->textContent >= $nextID) {
			$nextID += 1;
		}
	}
	return $nextID;
}

sub _updateRecord {
# --------------------------------------------------------------------------------------
=pod

=head2 _updateRecord()

Finds a specified record and updates it. Searches through each record in the
root document looking for index fields that have the same value as the 
'indexFieldValue' CGI paramater, if found creates a new replacement record 
using the rest of the passed CGI param values, replaces the node and
finally calls updateXML() to commit the change.

=cut
    my $self = shift;
    my $inFile = $self->_dataPath(); 
    $self->{parser} = XML::LibXML->new();	# [?] should already be a parser ref, should be able to remove this
    my $changes = 0;
	my $recordNodeToUpdate;

	if ($self->{indexFieldName} eq 'xapp:id') {

		# this is an app which uses the default xapp:id as index field, so look for a record with matching value
		my $XPath = $self->{recordsXPath} . '[xapp:id = \'' . $self->{indexFieldValue} . '\']';
	    my $recordNodeList = $self->{dataRootNode}->findnodes($XPath);
		unless ($recordNodeList->size() > 0) {
			$self->dieNice('No such index', $XPath . ": No record found with requested ID '" . $self->{indexFieldValue} . "'");
		}
		$recordNodeToUpdate = $recordNodeList->get_node(1);
		warn "UPDATING W DEFAULT ID FIELD, VAL=" . $self->{indexFieldValue}; 
		$changes++;

	} else {

		# this is an app which uses one of the record fields as an index, so look for that
		my @recordNodes = $self->{dataRootNode}->findnodes( $self->{recordsXPath} );
		foreach my $recordNode (@recordNodes) {			# step through each record
	        my @fieldNodes = $recordNode->childNodes;
			foreach my $fieldNode (@fieldNodes) {	    # and each field of each record
				# check if this field is the index field
	            if ($fieldNode->nodeName() eq $self->{indexFieldName}) {
		            # now see if the index value matches this fields index
			        my $indexFieldValue = $fieldNode->textContent;
				    my $cgiIndexFieldVal = $self->{indexFieldValue};
	                if (    # check if this index is the one we're looking for
		                    ($cgiIndexFieldVal eq $indexFieldValue) or
			                # or may have changed the index field so check the previous vaule
				            ($self->{oldkey} eq $indexFieldValue) )    # [2] 
						{
		                # yep, this index matches the one we're looking for...
						$recordNodeToUpdate = $recordNode;
				        $changes++;
						last;	# [?]
		            }
			    }
	        }
	    }

	}

    if ($changes) {	#[?] moved this stuff out of the above loop
		# create a new record node and replace the one we found 
		my $newRecordNode = $self->{applicationDoc}->createElement ( $self->{recordName} );

		# add an xapp:id if no other id specified 		
		if ($self->{indexFieldName} eq 'xapp:id') {
			my $textNode = $self->{applicationDoc}->createTextNode( $self->{indexFieldValue} );
			my $newNode = $self->{applicationDoc}->createElement( 'xapp:id' );
        	$newNode->addChild( $textNode );
			$newRecordNode->addChild( $newNode );
		}
		
		# then get all its cgi fields
		my @fieldNames = @{$self->{fieldNames}};
        foreach my $fieldName (@fieldNames) {
            # create new child nodes for each record field and add them to the new temp record
            my $newNode = $self->{applicationDoc}->createElement ( $fieldName );
            my $thisCGIParamValue = '';
            # check if this is a compound data type (e.g. date) if so need to extract its parts
            if ($self->{fieldTypes}->{$fieldName} eq 'date') {
				$thisCGIParamValue = $self->_getCompoundCGIValue('date', $fieldName);
			# check if time, if so also treat as compound value
			} elsif ($self->{fieldTypes}->{$fieldName} eq 'time') {
				$thisCGIParamValue = $self->_getCompoundCGIValue('time', $fieldName);
			# check if this is a checkbox data type, if write to XML as boolean '1' or '0'
			} elsif ($self->{fieldTypes}->{$fieldName} eq 'checkbox') {
				$thisCGIParamValue = (defined $self->{cgi}->param( $fieldName )) ? $self->{cgi}->param( $fieldName ) : '0';
				$thisCGIParamValue = ($thisCGIParamValue eq 'on') ? '1' : '0';
			} elsif ($fieldName eq $self->{indexFieldName}) {
				$thisCGIParamValue = $self->{indexFieldValue};
			} else {
				$thisCGIParamValue = $self->{cgi}->param( $fieldName ) || '';
			}
			my $textNode = $self->{applicationDoc}->createTextNode( $thisCGIParamValue );
			$newNode->addChild( $textNode );
			$newRecordNode->addChild( $newNode );
		}
		# replace the existing record with the newly updated one
		$recordNodeToUpdate->replaceNode($newRecordNode);
		$self->_updateXML($self->{dataRootNode});
    }
    return $changes;
}


sub _updateXML {
# --------------------------------------------------------------------------------------
=pod

=head2 _updateXML()

write the app objects data DOM out to the path specified by a call to _dataPath.
makes a call to _dieNice on fail

=cut

    my $self = shift;
    my $doc = shift;
    my $fileName = $self->_dataPath(); 
    my $text = $doc->toString;
    if( open( F, ">$fileName" )) {
        print F $text;
        close F;
    } else {
        $self->dieNice( 'Could not update data file', $fileName);
    }
}


sub _updateFKeyData {
# --------------------------------------------------------------------------------------
=pod

=head2 _updateFKeyData()

Write an updated set of foreign key data out to the KEYS.xml file.
On file error die with a related message. Note: Cant use toFile method here coz doesnt seem to return error on fail

=cut
    my $self = shift;
    if( open( F, ">$self->{fkeyPath}" )) {
        print F $self->{fkeyRootNode}->toString;
        close F;
    } else {
        $self->dieNice( 'Could not write to keys file', $self->{fkeyPath} );
    }
    return 1;
}

=pod

=head1 SEE ALSO

L<XML::LibXSLT>, L<XML::LibXML>, XApperator, XApperator::Controller, XApperator::View, XApperator::Utils  
L<http://xapperator.sourceforge.net/>

=cut


1
