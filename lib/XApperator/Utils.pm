package XApperator::Utils;

=pod

=head1 NAME

XApperator::Utils - General utils component in XApperator MVC framework

=head1 VERSION

B<1.0.1>

This Release: 24/Sep/2007
Original Release: 20/Feb/2007

=head1 AUTHOR

Dean Stringer (deeknow @ pobox . com)

=head1 DESCRIPTION

All subs in Utils.pm are general purpose or read-only functions that are independant of
the current application data, state or representation.

=cut

# --------------------------------------------------------------------------------- 


# --------------------------------------------------------------------------------- 
BEGIN {
use vars qw($VERSION @EXPORT @ISA);
use Carp;
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(dieNice _debug _fkeyValOK _registerFunctions _sanitisePath _getDateTime _getDateDisplay _warn);
$VERSION = '1.0.1';
}
# --------------------------------------------------------------------------------------


sub dieNice {
# --------------------------------------------------------------------------------------
=pod

=head2 dieNice()

Is called whenever we've encountered what seems like a fatal error. Checks the value
of 'dieOnError' and if set to '1' will actually die, otherwise simply 'warn's the 
passed error message. Also checks 'detailsToUser' and if set to a non null vaule
will return the error to the controller.cgi. Whatever the result the attribute
'error' is loaded with the passed message and this can be inspected by the
controller.cgi script.

=cut
    my $self = shift;
    my $msg = shift || '';
    my $details = shift || '';
	$self->{error} .= '<h1>Application Error</h1><p>Error was: ' . $msg . '</p>';
	if ($self->{detailsToUser} && $details ne '') { $self->{error} .= '<p>Details: ' . $details . '</p>'; }
	$self->{error} .= '<p>Please contact the administrator</p>';
    if ($self->{dieOnError} == 1) {
        croak "[XApperator] [error] $msg $details";
    } elsif ($self->{warnLevel} > 0) {
    	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);        
        carp "[XApperator] [error] $msg $details";
		print $self->contentHeader() . $self->{error} . $self->contentFooter();
		exit;
	} else {
    	croak "XApperator died!!! " . $msg;
	}
}



# ======================================================================================
=pod

=head1 PRIVATE(ish) SUBS

The following should all only be used internally by the XApperator object itself. They are
called directly by each other or by the three public subs new(), buildMenu() or processActions()

=cut
# ======================================================================================



sub _debug {
# --------------------------------------------------------------------------------------
=pod

=head2 _debug()

called when we want to log some debug info to the log file

=cut
    my $self = shift;
    my $msg = shift || '';
    my $details = shift || '';
	if ($self->{debugMode}) {
        carp "[XApperator] [debug] $msg $details";
	}
}



sub _getDateTime {
# --------------------------------------------------------------------------------------
=pod

=head2 _getDateTime()

Used by XApperator::new() to get date/time values and set propertites
$self->{dateToday} and $self->{timeNow} to ISO8601 like values to be
used in the XSLT (e.g. '20051230' and '2359')

=cut
    my $self = shift;
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);        
    $self->{dateNow} = ($year + 1900) . '-' . sprintf("%02d-%02d", $mon+1, $mday);
    $self->{timeNow} = sprintf("%02d%02d", $hour, $min);
}


sub _getDateDisplay {
# --------------------------------------------------------------------------------------
=pod

=head2 _getDateDisplay()

renders a date string for display, e.g. "Mon Aug 8 12:38:11 2005". is mostly
used when writing log entries

=cut
    my $self = shift;
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);        
    return "$days[$wday-1] $months[$mon] $mday $hour:$min:$sec " . ($year + 1900);
}


sub _registerFunctions {
# --------------------------------------------------------------------------------------
=pod

=head2 _registerFunctions()

Registers custom perl functions that will then be accessable to the
XSLT templates. You should add any new subs here and to access them from your
template add the following namespace declaration in the root xsl:stylesheet
element:

  xmlns:xapp="http://xapperator.sourceforge.net/"

The two functions currently registered are:

displayPrep() - passed a string of HTML and if it contains any escaped left or right hand
brackets (&lt; and &gt;) will unescape them back into their original < and >. Used in the publishing
stage in the sample CMS app for displaying easily editable brackets in HTML form elements. Not required
for normal XApp editing use

intToMonthShort() - if passed an integer between 0 and 11 will return a short month
name from a fixed array of names (e.g. Jan, Feb, Mar etc). Also used in the CMS app and not 
required for general purpose XApp use.

NOTE: these should really be abstracted out to a seperate module so developers can
extend the function set independently of the XApp distribution

=cut
    my $self = shift;
    
    $self->{xslt}->register_function("http://xapperator.sourceforge.net/",
        "displayPrep",
        sub {
            my $inString = shift;
            $inString =~ s/&lt;/</gi;
            $inString =~ s/&gt;/>/gi;
            return "" . $inString; }
    );
    $self->{xslt}->register_function("http://xapperator.sourceforge.net/",
        "intToMonthShort",
        sub {
            my $intMonth = shift;
            my @months = ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
            if (($intMonth < 0) or ($intMonth > 12)) {
                return '';
            } else {
                return $months[$intMonth-1];
            }
            }
    );
}



sub _sanitisePath {
# --------------------------------------------------------------------------------------
=pod

=head2 _sanitisePath()

Removes any '../' strings found in a passed path returning the sanitised value. Currently
only called by _parseCGIParams()

=cut
    my $self = shift;
    my $file_path = shift;
    $file_path =~ s#\.\.([/\:]|$)##g;
    return $file_path;
}



sub _warn {
# --------------------------------------------------------------------------------------
=pod

=head2 _warn()

called when we experience a non-fatal error that we want to log, but still want
to return and continue with processing, probably spitting something useful back
to the user later.

=cut
    my $self = shift;
    my $msg = shift || '';
    my $details = shift || '';
    if ($self->{warnLevel} > 0) {
        carp "[XApperator] [warn] $msg $details";
	}
}


1
