package XApperator::View;

=pod

=head1 NAME

XApperator::View - View related functions for the XApperator MVC framework

=head1 VERSION

B<1.0.1>

This Release: 24/Sep/2007
Original Release: 20/Feb/2007

=head1 AUTHOR

Dean Stringer (deeknow @ pobox . com)

=head1 DESCRIPTION

The subs in this module are invoked mostly by the processActions() method of the controller 
to build and return various elements of the user-interface, as well render content out to
the file-system in publish modes.   

=cut

# --------------------------------------------------------------------------------- 
BEGIN {
use vars qw($VERSION @EXPORT @ISA);
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(buildMenu buildEditContent buildSearchContent buildViewContent content contentFooter contentHeader contentHeadSection
	contentSelectmenu contentDebugInfo error publish publishAll);
$VERSION = '1.0.1';
}


# --------------------------------------------------------------------------------------
=pod

=head1 PUBLIC SUBS

=cut
# --------------------------------------------------------------------------------------


sub buildMenu {
# --------------------------------------------------------------------------------------
=pod

=head2 buildMenu()

build the UI section that lets a user select a set of records grouped
by some foreign key, this could be a dropdown select list or maybe
a serious of radio buttons, its up to the developer to tweak the
menu.xslt to suit if the out-of-box layout doesn't suit

=cut
    my $self = shift;
	my $menuXSLTFile = $self->{XSLTDir} . $self->{menuXSLTFile};
    unless (-e $menuXSLTFile) {
    	$self->_warn('No such menu XSLT',$menuXSLTFile);
    	return 0;
    }
    eval { $self->{menuXSLT} = $self->{parser}->parse_file( $menuXSLTFile ); } || return 0;
    # now chuck the keys file we loaded earlier through the XSLT
	my $rootDoc = XML::LibXML::Document->new();
	my $rootNode = $rootDoc->createElement( 'application' );
	$rootDoc->setDocumentElement($rootNode);
	$rootNode->appendChild($self->{fkeyRootNode});
	$rootNode->appendChild($self->{configRootNode});
    #print "Content-type: text\/xml\n\n" . $rootNode->toString; exit;
    my $stylesheet = $self->{xslt}->parse_stylesheet($self->{menuXSLT});
	my $results = $stylesheet->transform( $rootDoc,
	    XML::LibXSLT::xpath_to_string(
	        fkey => $self->{fkey},
	        app => $self->{app},
	        method => $self->{method},
	        addEnabled => $self->{addFKeyEnabled},
	        selectEnabled => 1, # [?] $self->{selectEnabled},
	        deleteEnabled => $self->{deleteFKeyEnabled},
	        publishEnabled => $self->{publishEnabled},
	        publishAllEnabled => $self->{publishAllEnabled},
	        viewEnabled => $self->{viewEnabled},
	        xmlEnabled => $self->{xmlEnabled},
	        relativeURL => $self->{relativeURL},
	        menuPrompt => $self->{promptMenu},
	        menuPromptEmpty => $self->{promptMenuEmpty},
			lang => $self->{lang},
        )
	);
    $self->{contentSelectmenu} = $results->toString;
}


sub buildViewContent {
# --------------------------------------------------------------------------------------
=pod

=head2 buildViewContent()

in view mode so add the foreign keys and data and do a transform passing in all the
xslt paramaters we need in view mode

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
	my $xsltPath = $self->{rootDir} . $self->{appDir} . $self->{app} . '/' . $self->{viewXSLTFile};
	unless (-e $xsltPath) {	$self->dieNice('Cant find View mode xslt file', $xsltPath);  }
	my $style_doc;
	eval { $style_doc = $self->{parser}->parse_file( $xsltPath ) };
	if ($@) { $self->dieNice('Failed to parse View mode xslt file', $xsltPath); }
   	my $stylesheet = $self->{xslt}->parse_stylesheet($style_doc);
    my $dataContainerNode = $self->{applicationDoc}->createElement( 'data' );
	$dataContainerNode->appendChild($self->{dataRootNode});
	$self->{applicationRootNode}->appendChild($dataContainerNode);
	$self->{applicationRootNode}->appendChild($self->{fkeyRootNode});
	#warn $self->{applicationRootNode}->toString;
	my $results = $stylesheet->transform($self->{applicationRootNode},
		XML::LibXSLT::xpath_to_string(
		key => $self->{key},
		fkey => $self->{fkey}
		)
	);
	$self->{content} .= $results->toString();
}

sub buildEditContent {
# --------------------------------------------------------------------------------------
=pod

=head2 buildEditContent()

in view mode so add the foreign keys and data and do a transform passing in all the
xslt paramaters we need in edit modes

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
	my $xsltPath = shift || $self->{XSLTDir} . $self->{editXSLTFile};
	unless (-e $xsltPath) { $self->dieNice('Cant find Edit mode xslt file', $xsltPath); }
	my $style_doc;
	eval { $style_doc = $self->{parser}->parse_file( $xsltPath ); };
	if ($@) { $self->dieNice('Failed to parse Edit mode xslt file', $xsltPath); }
	my $stylesheet = $self->{xslt}->parse_stylesheet($style_doc);
	my $dataContainerNode = $self->{applicationDoc}->createElement( 'data' );
	# dont add the dataroot node unless a payload is actually required
	if ($self->_dataPayloadReqd()) {
		$dataContainerNode->appendChild($self->{dataRootNode});
	}
	$self->{applicationRootNode}->appendChild($dataContainerNode);
	$self->{applicationRootNode}->appendChild($self->{configRootNode});
	$self->{applicationRootNode}->appendChild($self->{fkeyRootNode});
	#print "Content-type: text\/xml\n\n" . $self->{applicationRootNode}->toString; exit;
	my $results = $stylesheet->transform($self->{applicationRootNode},
		XML::LibXSLT::xpath_to_string(
			key => $self->{key},
			fkey => $self->{fkey},
			tkey => $self->{tkey},
			type => $self->{type},
			oldkey => $self->{oldkey},
			newkey => $self->{newkey},
			fkeyLabel => $self->{fkeyLabel},
			app => $self->{app},
			mode => $self->mode(),
			pmode => $self->{pmode},
			display => $self->{display},
			displayAlert => $self->{displayAlert},
			displayError => $self->{displayError},
			relativeURL => $self->{relativeURL},
			resourcesPath => $self->{resourcesPath},
			lang => $self->{lang},
			dateNow => $self->{dateNow},
			timeNow => $self->{timeNow},
			dateEntryFormat => $self->{dateEntryFormat},
			sortOrder => $self->{sortOrder},
			sortBy => $self->{sortBy},
		)
	);
	$self->{content} .= $results->toString();
}

sub buildSearchContent {
# --------------------------------------------------------------------------------------
=pod

=head2 buildSearchContent()

in search mode so add the foreign keys and data and do a transform passing in all the
xslt paramaters we need in search mode

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
	my $xsltPath = $self->{rootDir} . $self->{appDir} . $self->{app} . '/' . $self->{searchXSLTFile};
	unless (-e $xsltPath) {	$self->dieNice('Cant find Search mode xslt file', $xsltPath);  }
	my $style_doc;
	eval { $style_doc = $self->{parser}->parse_file( $xsltPath ) };
	if ($@) { $self->dieNice('Failed to parse Search mode xslt file', $xsltPath); }
   	my $stylesheet = $self->{xslt}->parse_stylesheet($style_doc);
    my $dataContainerNode = $self->{applicationDoc}->createElement( 'data' );
	$dataContainerNode->appendChild($self->{dataRootNode});
	$self->{applicationRootNode}->appendChild($dataContainerNode);
	$self->{applicationRootNode}->appendChild($self->{fkeyRootNode});
	my $results = $stylesheet->transform($self->{applicationRootNode},
		XML::LibXSLT::xpath_to_string(
		key => $self->{key},
		fkey => $self->{fkey},
		search => $self->{search}
		)
	);
	$self->{content} .= $results->toString();
}

sub content {
# --------------------------------------------------------------------------------------
=pod

=head2 content()

returns the XSLT rendering of the edit UI performed previously during
a call to processActions() . Used as method so developer of controller.cgi
doesnt need to reference any internal property value. Can also be used
to set the content value to something other than the XSLT result

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    if (@_) { $self->{content} = shift }
	return $self->{content};
}


sub contentHeader {
# --------------------------------------------------------------------------------------
=pod

=head2 contentHeader()

header content output before the actual body of the application, this is generally
an xhtml doctype, a head section, opening body tag and div container for the actual
page body  

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
	my $header = $self->{cgi}->header('text/html; charset=UTF-8');
	my $dtd = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>";
	my $bodyOpen = $dtd . "\n<html>\n<head>\n";
	$bodyOpen .= $self->contentHeadSection();
	$bodyOpen .= "</head>\n<body>\n<div class='body'>\n";
	if ($self->mode() eq 'xml') {
		$header = $self->{cgi}->header('text/xml; charset=UTF-8');
		$bodyOpen = '';
	}
	return $header . $bodyOpen;
}


sub contentHeadSection {
# --------------------------------------------------------------------------------------
=pod

=head2 contentHeadSection()

content that will be inserted in the rendered HEAD section if required. normally called 
by contentHeader but may also be used where a full contentHeader is not required if the 
developer is using their own templating system, in which case all they need is the
HEAD section content 

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
	my $headSection = "<link rel='stylesheet' href='" . $self->{resourcesPath} . "XApp.css'>\n";
	if ($self->_schemaHasFieldType('date')) {
		my $compressedPath = ($self->{useCompressedJS}) ?  '-compressed.js' : '.js';
		$headSection .= "<script src='" . $self->{resourcesPath} . 'date-functions' . $compressedPath . "' type='text/javascript'></script>\n" .
			"<script src='" . $self->{resourcesPath} . 'datechooser' . $compressedPath . "' type='text/javascript'></script>\n" .
			"<link rel='stylesheet' type='text/css' href='" . $self->{resourcesPath} . "datechooser.css'></link>\n" .
			"<!--[if lte IE 6.5]>\n" .
			"<link rel='stylesheet' type='text/css' href='" . $self->{resourcesPath} . "select-free.css'></link>\n" .
			"<![endif]-->\n";
	}
	#<xsl:if test="config/schema/clientsidevalidation = 'Javascript'">
	$headSection .= "<script language='javascript' type='text/javascript' src='" . $self->{resourcesPath} . "xapp-tools.js'></script>";
}


sub contentSelectmenu {
# --------------------------------------------------------------------------------------
=pod

=head2 contentSelectmenu()

returns the HTML rendering of the foreign key select menu. used as method so developer
of controller.cgi doesnt need to reference any internal property value

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
	if (($self->mode() =~ /xml|view|search/) || $self->{suppressmenu} || (($self->mode() eq 'edit') && $self->{editPopupMode})) {	# the menu is not required/wanted in these modes
		return '';
	} else {
		return $self->{contentSelectmenu};
	}
}


sub contentFooter {
# --------------------------------------------------------------------------------------
=pod

=head2 contentFooter()

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    my $footer = '';
	if ($self->mode() ne 'xml') {
		if ($self->{debugMode}) {
			$footer .= $self->contentDebugInfo();
		}
		$footer .= "</body></html>\n";
	}
	return $footer;
}

sub contentDebugInfo {
# --------------------------------------------------------------------------------------
=pod

=head2 contentDebugInfo()

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
	return '<pre style="padding-top: 20px;">Debug: ' .
				' FKEY=' . $self->{fkey} .
				' KEY=' . $self->{key} .
				' NEWKEY=' . $self->{newkey} .
				' NDX FIELD=' . $self->{indexFieldName} .
				' MODE=' . $self->{mode} .
				' LANG=' . $self->{lang} .
				' SORTORDER=' . $self->{sortOrder} .
				' SORTBY=' . $self->{sortBy} .
				' TYPE=' . $self->{type} .
			 '</pre>';
}

sub error {
# --------------------------------------------------------------------------------------
=pod

=head2 error()

sets or returns the text of the any processing error. used as method so developer
of controller.cgi doesnt need to reference any internal property value

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    if (@_) { $self->{error} = shift }
    return $self->{error};
}



sub publish {
# --------------------------------------------------------------------------------------
=pod

=head2 publish()

Render the active data file out through the nominated XSLT template. The XSLT template
location is determined by the 'transform' attribute of the actions/publish element
in the config.xml file. The target output location is determined by the 'folder'
attribute of the same config element. The data is transformed through the XSLT
template to the target folder and a filename that is a combination of key value
and file 'extension' (also from actions/publish). If the transform fails for any
reason the app dies, if all is well return with '1'

=cut
    my $self = shift;
    my @transforms;
    my $tranformsMade = 0;

    # get the foreign key label for this key value, designer might want to use it in their
    # XSLT so we pass it here as an argument
    my $keyLabel = $self->_fkeyLabel($self->{fkey});

    # the config file can either contain a single publish transform with all
    # filepaths embedded in it, or may optionally contain a bunch of transforms
    # specified in individual <transform> child elements. $self->{publishTransforms}
    # will be 1 or more if the latter is true
    if ($self->{publishTransforms} == 0) {
        my $styleDocPath = $self->{rootDir} . $self->{appDir} . $self->{app} . '/' . $self->{publishXSLTFile};
        my $outFile = $self->{publishFolder} . $self->{fkey} . '.' . $self->{publishExtension};
		unless ($outFile =~ /^\//) {	# if it doesnt start with '/' assume is relative and add the apps operating path in front
			$outFile = $self->{rootDir} . $self->{appDir} . $self->{app} . '/' . $outFile;
		}
        my @baseTransform = [$styleDocPath, $outFile];
		#warn "path " . $styleDocPath . ' file ' . $outFile;
        push @transforms, @baseTransform;
    } else {
        my @transformConfigs = $self->{configRootNode}->findnodes( 'actions/publish/transform'  );
        foreach my $node (@transformConfigs) {
            my $folder = $node->findvalue( '@folder' ) || $self->{publishFolder};
			unless ($folder =~ /^\//) {	# if it doesnt start with '/' assume is relative and add the apps operating path in front
				$folder = $self->{rootDir} . $self->{appDir} . $self->{app} . '/' . $folder;
			}
            # if we're in multi-transform mode, save the last folder location
            if ($self->{publishFolder} eq '') { $self->{publishFolder} = $folder; }
            my $xslt = $node->findvalue( '@xslt' ) || $self->{publishXSLTFile};
            my $extension = $node->findvalue( '@extension' ) || $self->{publishExtension};
            # vals for @filename include 'key-label' or 'key-value' (default)
            my $filename = $node->findvalue( '@filename' ) || 'key-value';
            if ($filename eq 'key-label') {
                $filename = $keyLabel; # label will probably have spaces and other chars
                $filename =~ s/[^\w\d\-_()]//g;  # so strip them out
            } else {
                $filename = $self->{fkey};
            }

            my $styleDocPath = $self->{rootDir} . $self->{appDir} . $self->{app} . '/' . $xslt;
            my $outFile = $folder . $filename . '.' . $extension;
            my @thisTransform = [$styleDocPath, $outFile];
            push @transforms, @thisTransform;
        }
    }


    # parse the data file which we'll use for each transform
    $self->{parser} = XML::LibXML->new();		# [?] should already be a parser ref, should be able to remove this
    my $inFile = $self->_dataPath();
    my $source;
	eval { $source = $self->{parser}->parse_file($inFile); };
	if ($@) { $self->dieNice("Failed to parse source xml file during publishing", $inFile); }

	# process each transform
    foreach my $transform (@transforms) {
        my @transformDetails = @{$transform};
        $styleDocPath = $transformDetails[0];
		unless (-e $styleDocPath) { $self->dieNice('Cant find Publishing xslt file', $styleDocPath); }
        my $style_doc;
		eval { $style_doc = $self->{parser}->parse_file($styleDocPath); } ||
	    	$self->dieNice('failed to parse Publishing xslt file  ' . $styleDocPath);
        my $stylesheet = $self->{xslt}->parse_stylesheet($style_doc);
        my $results = $stylesheet->transform($source,
            XML::LibXSLT::xpath_to_string(
                fkey => $self->{fkey},
                fkeyLabel => $keyLabel,
				dateNow => $self->{dateNow},
				timeNow => $self->{timeNow},
                )
        );
        my $outFile = $transformDetails[1];
 
        if (-e $outFile) {  # if target file exists, check we can write to it
			unless (-w $outFile) {
	            $self->dieNice("Cant publish to output file", $outFile);
			}
        } else {
            # must be a new file, see if the folder is writeable...
            my $outFolder = $outFile;
            $outFolder =~ s/((.*)\/)(.*)$/$1/;
			unless (-w $outFolder) {
	            $self->dieNice("Cant publish to new file", $outFile);
			}
        }
        eval {
            $stylesheet->output_file($results, $outFile);
        };
        if ($@) { $self->dieNice("Writing to output file failed", $outFile); }
        $tranformsMade++;
    }
    return $tranformsMade;
}



sub publishAll {
# --------------------------------------------------------------------------------------
=pod

=head2 publishAll()

Called by processActions() if the user has clicked 'Publish All' and makes a call
to publish() for each key in the main KEYS.xml file. Saves the current fkey value
and restores it at the end of the call. Returns the total number of files published.

=cut
# --------------------------------------------------------------------------------------
    my $self = shift;
    my $currentFKey = $self->{fkey};    # save so can restore before we return
    my $keysPublished = 0;
    foreach my $keyNode ($self->{fkeyRootNode}->findnodes('//foreignkeys/key')) {
        $self->{fkey} = $keyNode->find('value') || '';
        $self->publish();
        $keysPublished++;
    }
    $self->{fkey} = $currentFKey;
    return $keysPublished;
}

=pod

=head1 SEE ALSO

L<XML::LibXSLT>, L<XML::LibXML>, XApperator, XApperator::Model, XApperator::Controller, XApperator::Utils  
L<http://xapperator.sourceforge.net/>

=cut


1
