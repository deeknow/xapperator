
function getElById(idVal) {
  if (document.getElementById != null)
  return document.getElementById(idVal)
  if (document.all != null)
  return document.all[idVal]
  alert("Problem getting element by id")
  return null
}

function doCollapse( id, subid, maxread, doscroll )
{
  var ele = getElById('items'+id);
  var atitle;

  if( ele.style.display=="none" ) {

    ele.style.display="block";
    atitle = "Collapse";
    

  } else {
    ele.style.display="none";
    atitle = "Expand";
    if( doscroll == 1 )
      window.location.hash='base'+id;
  }
  if( ele.style.visibility=="hidden" )
    ele.style.visibility="visible";
  else
    ele.style.visibility="hidden";

  ele = getElById( 'anchor'+id );
  ele.title = atitle;
  ele.innerHTML = atitle;
}

function expandItem( itemid )
{
  var ele = getElById('item.'+itemid);
  var aimg;
  var atitle;

  if( ele.style.display=="none" ) {
    ele.style.display="block";
    aimg = "http://webteam-test.waikato.ac.nz/utilities/XApp/images/smallminus.gif";
    atitle = "Collapse";

  } else {
    ele.style.display="none";
    aimg = "http://webteam-test.waikato.ac.nz/utilities/XApp/images/smallplus.gif";
    atitle = "Expand";
  }

  if( ele.style.visibility=="hidden" )
    ele.style.visibility="visible";
  else {
    ele.style.visibility="hidden";
	}

  ele = getElById( 'itemimg.'+itemid );
  ele.src = aimg;
  ele = getElById( 'itemanchor.'+itemid );
  ele.title = atitle;
}