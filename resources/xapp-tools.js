/*

Author: Dean Stringer (deeknow @ pobox . com)
This Release: 07/May/2007
Version: 0.31
Description/Purpose:
    some functions commonly used within the XApperator framework
    note: most of the JavaScript used in XApp is dynamically
        generated through the XSLT templates, or the CGI script
        itself.

LICENSE

XApperator: simple XML editing framework - menu XSLT file
Copyright (C) 2005  Dean Stringer (deeknow @ pobox . com)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

    // is only support by NS6/IE5+ though but fails gracefully
    function tickID(idx) {
        if (document.getElementById) {
            document.getElementById(idx).checked = true;
        }
    }

    // used to check one of the records displayed is actually selected when
    // user has clicked 'Delete', 'Edit', 'Cut', 'Copy' or 'Paste'
    // Saves a round trip to the server
    function checkSelected(thisAction) {
        for( i = 0; i < document.records.elements.length; i++ ) {
            if (document.records.elements[i].name == 'x-key')
                if (document.records.elements[i].checked)
                    return true;
        }        
        alert('You must select one of the records to ' + thisAction);
        return false;
    }


/**
 * Uses getElementByID to find and return a requested element and
 * falls back to document.all[] if the former not supported
 * @param {String} itemid ID of the element whos object reference we want to return
 * @returns an element reference
*/
function getByID (itemid) {
	//alert('thisID = ' + itemid);
	  if (document.getElementById != null)
		  return document.getElementById(itemid)
	  if (document.all != null)
		  return document.all[itemid]
	  return null
}

/**
 * Submit the record list form following a double-click by user
*/
function editRecord (recordID) {
	tickID(recordID);
	document.getElementById('x-mode-Edit').click()
}

