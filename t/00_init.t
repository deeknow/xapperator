# --------------------------------------------------------------------------------- 
# Description/Purpose:
#    Used to test initialisation and common XApperator.pm methods
# --------------------------------------------------------------------------------- 
use warnings;
use strict;
use CGI;
use vars qw( $q $XApp );

my $q = new CGI;

use Test::More;
BEGIN { plan tests => 16 }

use_ok('XApperator');
ok( $XApperator::VERSION eq '1.0.1', 'version check');

# -----------------------------------------------------------------
# Test: create and parse
# -----------------------------------------------------------------

# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');
$q->param(-name=>'x-fkey',-value=>'acc');
$q->param(-name=>'x-mode',-value=>'select');
$q->param(-name=>'user',-value=>'bob');
# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 1,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
		);
ok( defined $XApp,		'new()' );
ok( $XApp->isa('XApperator'),	'package name' );		# check the package name
ok( checkConfigActions(),	'default config settings' );
ok( checkConfigFields(),	'correct config field names');
ok( checkConfigMisc(),		'misc config details' );
is( $XApp->title(), 'Test App',			'title()' );
is( $XApp->{indexFieldName}, 'user',		'app index field name');
is( $XApp->{recordName}, 'member',		'app record name');
is( $XApp->_configFilePath(), 't/apps/test/config.xml',		'_configFilePath()');
is( $XApp->_dataPath(), 't/apps/test/data/acc.xml',		'_dataPath()');
is( $XApp->_sanitisePath('/this/../that/'), '/this/that/',	'_sanitisePath()');
is( $XApp->_getIndexFieldValue(), 'bob',	'_getIndexFieldValue()');
is( $XApp->_dataPayloadReqd(),'1',		'app data payload reqd');
$XApp->{fkey} = '';
is( $XApp->_dataPayloadReqd(),'0',		'app data payload NOT reqd');


#print "display = " . $XApp->{display} . " alert = " . $XApp->{displayAlert};


# ------------------------------------------------------------------------------
sub checkConfigActions {
    if (
        ($XApp->{addEnabled} eq '1') && ($XApp->{addFKeyEnabled} eq '1') && 
        ($XApp->{insertEnabled} eq '1') && ($XApp->{deleteEnabled} eq '1')  &&
        ($XApp->{publishEnabled} eq '1') && ($XApp->{xmlEnabled} eq '1') &&
        ($XApp->{viewEnabled} eq '1')
        ) {
        return 1;
    } else {
        return 0;
    }
}
# ------------------------------------------------------------------------------
sub checkConfigFields {
    my @expectedFieldNames = qw(contact email expertise familyname name
        password role showemail user);
    my $fieldCount = scalar @{$XApp->{fieldNames}};
    return 0 unless ($fieldCount == scalar @expectedFieldNames);
    return 1;
}
# ------------------------------------------------------------------------------
sub checkConfigMisc {
    return 0 unless ($XApp->{minOccurs} eq 10);
    return 0 unless ($XApp->{maxOccurs} eq 100);
    return 0 unless ($XApp->{recordsXPath} eq '/staff/member');
    return 0 unless ($XApp->{method} eq 'POST');
    return 1;
}
# ------------------------------------------------------------------------------

