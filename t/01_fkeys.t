# --------------------------------------------------------------------------------- 
# Description/Purpose:
#    Used to test foreign key operations of XApperator.pm
# --------------------------------------------------------------------------------- 
use warnings;
use strict;
use CGI;
use vars qw( $q $XApp );

my $q = new CGI;

use Test::More;
BEGIN { plan tests => 16 }

use XApperator;

# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');
$q->param(-name=>'x-fkey',-value=>'acc');
$q->param(-name=>'x-mode',-value=>'select');
$q->param(-name=>'user',-value=>'bob');
# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 0,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
		);

# -----------------------------------------------------------------
print "# Test: foreign keys\n";
#   test foreign keys to catch...
#		- adding without a key value
#		- saving without a file-system friendly key value
#		- deleting without specifiying a key
#		- specifiying a key that doesnt exist
#		- that we are asked to confirm a deletion
#		- a successful delete
# -----------------------------------------------------------------
ok($XApp->{fkeyLabel} eq 'Department');
ok($XApp->{fkeyPath} eq 't/apps/test/KEYS.xml');
ok($XApp->_fkeyLabel('acc') eq 'Accounting');
ok($XApp->_fkeyLabel('bla') eq ''); # bogus key should return ''

# should be two fkey's to start with 
ok ((scalar $XApp->_fkeyValues()) eq 2);

# try saving a new fkey, missing a label
$XApp->mode('save'); $XApp->{type} = 'keys';
$XApp->{label} = ''; $XApp->processActions();
ok($XApp->{displayAlert} eq 'nofkeylabel');

# try saving again, this time without a value
$XApp->{label} = 'something'; $XApp->{value} = ''; $XApp->processActions();
ok($XApp->{displayAlert} eq 'nofkeyvalue');

# try saving again but with a non-filesystem friendly value/filename
$XApp->{value} = ' some unfriendly - filename '; $XApp->processActions();
ok($XApp->{displayAlert} eq 'fkeyinvalid');

ok( $XApp->_fkeyExists('acc') eq '1');

# this time we should be able to save the new fkey
$XApp->{label} = 'test'; $XApp->{value} = 'test'; 
$XApp->mode('save'); $XApp->processActions();
ok($XApp->{display} eq 'addedfkey');

# check the new key exists
ok( $XApp->_fkeyExists('test') eq '1');

# should be two fkey's to start with 
ok ((scalar $XApp->_fkeyValues()) eq 3);

# now try to delete
$XApp->mode('delete'); $XApp->{type} = 'keys';

# test error when no fk0ey selected
$XApp->{fkey} = ''; $XApp->processActions();
ok($XApp->{displayAlert} eq 'nothingselected');

# now set to some bogus key and check get the nofkeyexists error
$XApp->{fkey} = 'blabal';

$XApp->processActions();
ok($XApp->{displayAlert} eq 'nofkeyexists');

# now check we get the deletion confirm when using a real existing fkey value
$XApp->{fkey} = 'test'; $XApp->{confirm} = ''; $XApp->processActions();
ok($XApp->{display} eq 'confirmdelete');


# now set the confirm flag and we should be able to delete it
$XApp->{confirm} = 'yes'; $XApp->processActions();
ok($XApp->{display} eq 'deletedfkey');


#print "display = " . $XApp->{display} . " alert = " . $XApp->{displayAlert};