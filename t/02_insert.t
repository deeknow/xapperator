# --------------------------------------------------------------------------------- 
# Description/Purpose:
#    Used to test insert operations of XApperator.pm
# --------------------------------------------------------------------------------- 
use warnings;
use strict;
use CGI;
use vars qw( $q $XApp );

my $q = new CGI;

use Test::More;
BEGIN { plan tests => 8 }

use XApperator;

# -----------------------------------------------------------------
print "# Test: Insert methods\n";
# -----------------------------------------------------------------

# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');
$q->param(-name=>'x-fkey',-value=>'acc');
$q->param(-name=>'x-mode',-value=>'select');
$q->param(-name=>'user',-value=>'bob');
# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 0,
        warnLevel => 1,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
		);

# -----------------------------------------------------------------------
#   we test adding a record..
#       - when adding is disabled
#       - without specifying an index value
#       - where there is an existing one with same key
#       - where we've missed a 'required' field
#       - where a save should succeed
#       - and check the new record exists
# -----------------------------------------------------------------------

# Check that the no-adding setting is honoured
$XApp->mode('save');
$XApp->{rootInsert} = '1';
$XApp->{fkey} = 'acc';
$XApp->{type} = '';
$XApp->{addEnabled} = '0';
$XApp->{insertEnabled} = '0';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'adddisabled');

# reset them
$XApp->{addEnabled} = '1';
$XApp->{insertEnabled} = '1';

# try adding with no index value, should error
$XApp->mode('save');
$XApp->{indexFieldValue} = '';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'noindex');

#print "ALERT='" . $XApp->{displayAlert} . "'"; #'del= ' . $XApp->{deleteEnabled}; exit;

# try adding again, should now get duplicate index
$XApp->{indexFieldValue} = 'mikes';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'duplicateindex');

# try adding a new record 'frank' with name missing (which is compulsory in this test app)
$XApp->{cgi}->param(-name=>'user', -value=>'frank');
$XApp->{cgi}->param(-name=>'name', -value=>'');
$XApp->{indexFieldValue} = 'frank';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'missingfield');

# set maxOccurs to '1', should not be able to add a record
$XApp->{maxOccurs} = 1;
$XApp->processActions();
ok($XApp->{displayAlert} eq 'addtoomany');

# change the name to not null, and maxOccurs back to 100, should now save OK
$XApp->{cgi}->param(-name=>'name', -value=>'Frankie');
$XApp->{maxOccurs} = 100;
$XApp->processActions();
ok($XApp->{display} eq 'saved');

# check the key exists
ok( $XApp->_keyExists('frank') eq '1');

# now clean up by deleting the record
$XApp->mode('delete'); 
$XApp->{minOccurs} = 1;
$XApp->{key} = 'frank';
$XApp->{confirm} = 'yes';
$XApp->processActions();
ok($XApp->{display} eq 'deletedrecord');

#print "display = " . $XApp->{display} . " alert = " . $XApp->{displayAlert};