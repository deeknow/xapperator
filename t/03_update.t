# --------------------------------------------------------------------------------- 
# Description/Purpose:
#    Used to test record update operations of XApperator.pm
# --------------------------------------------------------------------------------- 
use warnings;
use strict;
use CGI;
use vars qw( $q $XApp );

my $q = new CGI;

use Test::More;
BEGIN { plan tests => 5 }

use XApperator;

# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');

# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 0,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
		);


# -----------------------------------------------------------------
print "# Test: Update methods\n";
#   we test updating a record..
#       - without specifying an index
#       - without including a 'required' field
#       - where the index exists for another record
#       - where the update should succeed
# -----------------------------------------------------------------

# test a SAVE without specifying an index CGI val
$XApp->{insertkey} = '';
$XApp->{rootInsert} = '';
$XApp->{mode} = 'save';
$XApp->{fkey} = 'acc';
$XApp->{key} = 'freddy';
$XApp->{oldkey} = 'freddy';
$XApp->{cgi}->param(-name=>'user', -value=>'freddy');
$XApp->{indexFieldValue} = '';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'noindex');

# check a SAVE with a missing compulsory field
$XApp->{cgi}->param(-name=>'name', -value=>'');
$XApp->{indexFieldValue} = 'freddy';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'missingfield');

# check a SAVE with duplicate key
$XApp->{cgi}->param(-name=>'name', -value=>'Fred Dagg');
$XApp->{oldkey} = 'bob';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'duplicateindex');

# reset to unique key
$XApp->{oldkey} = 'freddy';      

# check field length too long
$XApp->{cgi}->param(-name=>'name', -value=>'Really Long Big Bad Freddy');
$XApp->processActions();
ok($XApp->{displayAlert} eq 'fieldvalerror');

# reset to short name
$XApp->{cgi}->param(-name=>'name', -value=>'Fred Dagg');    

# should SAVE ok now
$XApp->processActions();
ok($XApp->{display} eq 'saved');


#print "display = " . $XApp->{display} . " alert = " . $XApp->{displayAlert};