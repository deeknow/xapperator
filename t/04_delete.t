# --------------------------------------------------------------------------------- 
# Description/Purpose:
#    Used to test record delete operations of XApperator.pm
# --------------------------------------------------------------------------------- 
use warnings;
use strict;
use CGI;
use vars qw( $q $XApp );
#use Data::Dumper;

my $q = new CGI;

use Test::More;
BEGIN { plan tests => 7 }

use XApperator;

# -----------------------------------------------------------------
print "# Test: Delete methods\n";
# -----------------------------------------------------------------

# -----------------------------------------------------------------
# Test: create and parse
# -----------------------------------------------------------------

# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');
$q->param(-name=>'x-fkey',-value=>'acc');
$q->param(-name=>'user',-value=>'bob');
# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 0,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
		);


# -----------------------------------------------------------------
#print "# Test: record delete\n";
#   we test deleting a record catching...
#		- can't delete when disabled
#		- no key selected
#		- confirm delete asked for
#		- delete when too few records
#		- successful delete
# -----------------------------------------------------------------
$XApp->{mode} = 'delete'; 
$XApp->{oldkey} = '';
$XApp->{key} = '';
# test delete disabled
$XApp->{deleteEnabled} = '0';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'deletedisabled');

# test no record selected
$XApp->{mode} = 'delete'; 
$XApp->{deleteEnabled} = '1';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'nothingselected');

# test delete without confirm
$XApp->{key} = 'frank';
$XApp->{confirm} = ''; 
$XApp->processActions();
ok($XApp->{display} eq 'confirmdelete');

# test delete but too few records (ie minOccurs)
$XApp->{confirm} = 'yes';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'deletetoofew');

# add a dummy record we can delete
$XApp->mode('save');
$XApp->{key} = 'temp';
$XApp->{oldkey} = '';
$XApp->{indexFieldValue} = 'temp';
$XApp->{cgi}->param(-name=>'name', -value=>'temp');
$XApp->{cgi}->param(-name=>'user',-value=>'temp');
$XApp->processActions();
ok($XApp->{display} eq 'addedrecord');

# test delete is now ok
$XApp->{mode} = 'delete'; 
$XApp->{minOccurs} = 1;
$XApp->{key} = 'temp';
$XApp->{oldkey} = 'temp';
$XApp->processActions();
ok($XApp->{display} eq 'deletedrecord');


# test a repeat delete doesnt make any changes
#$XApp->{key} = 'frank';
$XApp->{mode} = 'delete'; 
$XApp->processActions();
ok($XApp->{displayAlert} eq 'nochanges');

#print "display = " . $XApp->{display} . " alert = " . $XApp->{displayAlert};
