# --------------------------------------------------------------------------------- 
# Description/Purpose:
#    Used to test record copy/paste operations of XApperator.pm
# --------------------------------------------------------------------------------- 
use warnings;
use strict;
use CGI;
use vars qw( $q $XApp );

my $q = new CGI;

use Test::More;
BEGIN { plan tests => 12 }

use XApperator;


# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');
$q->param(-name=>'x-fkey',-value=>'acc');
#$q->param(-name=>'user',-value=>'bob');
# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 0,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
		);


# -----------------------------------------------------------------
print "# Test: copy/cut/paste methods\n";
#   we test cut/copy/paste modes catching...
#		- no record selected
#		- no paste location specified
#		- specified key not found
#		- max records exceeded
#		- successful paste
# -----------------------------------------------------------------

# should coz copy/paste disabled
$XApp->{mode} = 'copy'; 
$XApp->{key} = ''; 
$XApp->{copyPasteEnabled} = 0; 
$XApp->processActions();
ok($XApp->{displayAlert} eq 'copypastedisabled',	'alert: copypastedisabled');

# should fail without a key value in copy mode
$XApp->{copyPasteEnabled} = 1; 
$XApp->{mode} = 'copy'; 
$XApp->processActions();
ok($XApp->{displayAlert} eq 'nothingselected',	'alert: nothing selected on copy');

# ditto in cut mode
$XApp->{mode} = 'cut'; 
$XApp->processActions();
ok($XApp->{displayAlert} eq 'nothingselected',	'alert: nothing select on cut');

# display should be set to '' if a key specified
$XApp->{mode} = 'copy'; 
$XApp->{key} = 'bob'; 
$XApp->processActions();
ok($XApp->{display} eq '',	'copy');

# when pasteing should get error if paste target key doesnt exist
$XApp->{mode} = 'paste'; 
$XApp->{tkey} = 'zzz'; 
$XApp->processActions();
ok($XApp->{displayAlert} eq 'indexnotfound',	'alert: index not found');

# should also get an error if trying to paste when maxOccurs exceeded
$XApp->{maxOccurs} = 1;
$XApp->processActions();
ok($XApp->{displayAlert} eq 'addtoomany',	'alert: too many records');
$XApp->{maxOccurs} = 100;   # reset it

# should be no copy of freddy before we start
ok( $XApp->_keyExists('freddy(1)') eq '0',	'copied key found');

# should be able to paste now before a valid index
$XApp->{mode} = 'paste'; 
$XApp->{pmode} = 'cut'; 
$XApp->{tkey} = 'freddy';
$XApp->processActions();
ok($XApp->{display} eq 'pasted',	'record cut');

# still shouldnt be a 'copy' coz we did a cut/paste
ok( $XApp->_keyExists('freddy(1)') eq '0',	'key exists');

# should be able to paste now before a valid index
$XApp->{mode} = 'paste'; 
$XApp->{pmode} = 'copy'; 
$XApp->{tkey} = 'freddy'; 
$XApp->processActions();
ok($XApp->{display} eq 'pasted',	'record copied');

# should be one there now
ok( $XApp->_keyExists('freddy(1)') eq '1',	'key exists');

# now delete our copy so test runs next time
$XApp->{mode} = 'delete'; 
$XApp->{key} = 'freddy(1)';
$XApp->{oldkey} = ''; 
$XApp->{confirm} = 'yes';
$XApp->{minOccurs} = 1;		# coz is 10 in the config file
$XApp->processActions();
ok($XApp->{display} eq 'deletedrecord',	'record deleted');

#print "display = " . $XApp->{display} . " alert = " . $XApp->{displayAlert};
