# --------------------------------------------------------------------------------- 
# Description/Purpose:
#   test publishing methods looking for ...
#		- publish any disabled
#		- publish all disabled
#		- publish location ok
#		- succesful publish
# -----------------------------------------------------------------
use warnings;
use strict;
use CGI;
use vars qw( $q $XApp );

my $q = new CGI;

use Test::More;
BEGIN { plan tests => 7 }

use XApperator;

# -----------------------------------------------------------------
print "# Test: Publishing methods\n";
# -----------------------------------------------------------------


# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');
$q->param(-name=>'x-fkey',-value=>'acc');
$q->param(-name=>'user',-value=>'bob');
# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 0,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
);

$XApp->{publishEnabled} = 0;
$XApp->{mode} = 'publish'; 

ok($XApp->{publishTransforms} == 1);    # should be '1' as we're in multi-transform mode

$XApp->{type} = 'keys';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'publishdisabled');

$XApp->{publishEnabled} = 1; 
$XApp->{mode} = 'publish'; 
$XApp->processActions();
ok($XApp->{display} eq 'published');

# we're in mult-transform publish mode (see config.xml) so publishFolder wont be
# set until we actually do a publish
ok($XApp->{publishFolder} eq 't/apps/test/publish/');

ok($XApp->{publishAllEnabled} == 0);    # should be '0'
$XApp->{mode} = 'publishall'; 
$XApp->processActions();    # should fail
ok($XApp->{displayAlert} eq 'publishalldisabled');

$XApp->{publishAllEnabled} = 1; 
$XApp->{mode} = 'publishall'; $XApp->{type} = 'keys';
$XApp->processActions();    # should now work
ok($XApp->{display} eq 'published');
