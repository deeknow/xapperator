# --------------------------------------------------------------------------------- 
# Description/Purpose:
#    Used to test operation of various miscellaneous XApperator.pm methods
# --------------------------------------------------------------------------------- 
use warnings;
use strict;
use CGI;
use vars qw( $q $XApp );

my $q = new CGI;

use Test::More;
BEGIN { plan tests => 9 }

use XApperator;

# -----------------------------------------------------------------
print "# Test: misc methods\n";
# -----------------------------------------------------------------

# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');
$q->param(-name=>'x-fkey',-value=>'acc');
$q->param(-name=>'x-mode',-value=>'select');
$q->param(-name=>'user',-value=>'bob');
# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 0,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
);

$XApp->content('some content'); 
ok($XApp->{content} eq 'some content');
ok($XApp->content() eq 'some content');

# check compound value extraction, add 3 date field parts to CGI params..
$XApp->{cgi}->param( -name=>'mydate', -value=>'2005-07-08');
#$XApp->{cgi}->param( -name=>'mydate:month', -value=>'07');
#$XApp->{cgi}->param( -name=>'mydate:day', -value=>'08');
ok($XApp->_getCompoundCGIValue('date', 'mydate') eq '2005-07-08');

$XApp->{cgi}->param( -name=>'mytime:hour', -value=>'15');
$XApp->{cgi}->param( -name=>'mytime:min', -value=>'45');
ok($XApp->_getCompoundCGIValue('time', 'mytime') eq '1545');

# check date and time fetch values
$XApp->_getDateTime();
ok($XApp->{dateNow} =~ /\d{4}-\d{2}-\d{2}/);
ok($XApp->{timeNow} =~ /\d{4}/);

# schema field type tests, return 0 or 1
ok($XApp->_schemaHasFieldType('text'));
ok($XApp->_schemaHasFieldType('select'));
ok(! $XApp->_schemaHasFieldType('fred'));

