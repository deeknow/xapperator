# --------------------------------------------------------------------------------- 
# Description/Purpose:
#    Used to test operation of XML display mode in XApperator.pm
# --------------------------------------------------------------------------------- 
use warnings;
use strict;
use CGI;
use vars qw( $q $XApp );

my $q = new CGI;

use Test::More;
BEGIN { plan tests => 3 }

use XApperator;

# -----------------------------------------------------------------
print "# Test: XML view methods\n";
# -----------------------------------------------------------------

# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');
$q->param(-name=>'x-fkey',-value=>'acc');

# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 0,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
);

# schema field type tests, return 0 or 1
ok($XApp->{xmlEnabled} eq '1');	# XML enabled in config

$XApp->{mode} = 'xml'; 
$XApp->processActions();

use XML::LibXML;
my $parser = XML::LibXML->new();
my $doc = $parser->parse_string( $XApp->content );
my @nodelist = $doc->getElementsByTagName( $XApp->{recordName} );	# 'member' in this test phonebook app case
ok( scalar @nodelist );		# should be more than '0' of them 

$XApp->{xmlEnabled} = 0;
$XApp->processActions();
ok($XApp->{displayAlert} eq 'noxmlview');

#print $XApp->content;
