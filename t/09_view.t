# --------------------------------------------------------------------------------- 
# Description/Purpose:
# -----------------------------------------------------------------
use warnings;
use strict;
use CGI;
use vars qw( $q $XApp );

my $q = new CGI;

use Test::More;
BEGIN { plan tests => 5 }

use XApperator;

# -----------------------------------------------------------------
print "# Test: View methods\n";
# -----------------------------------------------------------------

# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');
$q->param(-name=>'x-fkey',-value=>'acc');
# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 0,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
);

ok($XApp->{viewEnabled});

$XApp->{viewEnabled} = 0;
$XApp->{mode} = 'view'; 
$XApp->processActions();
ok($XApp->{displayAlert} eq 'viewdisabled');

$XApp->{viewEnabled} = 1;
$XApp->{fkey} = '';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'nofkeyvalue');

$XApp->{fkey} = 'acc';
$XApp->processActions();
ok($XApp->{display} eq 'viewed');

ok ($XApp->content =~ /<table/g);	# should have a <table> tag in the content (see t/apps/test/publish.xslt)


