# --------------------------------------------------------------------------------- 
# Description/Purpose:
# -----------------------------------------------------------------
use warnings;
use strict;
use CGI;
use vars qw( $q $XApp );

my $q = new CGI;

use Test::More;
BEGIN { plan tests => 5 }

use XApperator;

# -----------------------------------------------------------------
print "# Test: Search methods\n";
# -----------------------------------------------------------------

# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');
# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 0,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
);

ok($XApp->{searchEnabled});

$XApp->{mode} = 'search'; 

$XApp->{search} = '';
$XApp->processActions();
ok($XApp->{displayAlert} eq 'searchempty');

$XApp->{search} = 'Joe';
$XApp->processActions();
ok($XApp->{displayAlert} eq '');
ok($XApp->{display} eq 'searched');

$XApp->{searchEnabled} = 0;
$XApp->processActions();
ok($XApp->{displayAlert} eq 'searchdisabled');

