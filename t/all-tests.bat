@ECHO OFF
REM -----------------------------------------------------------
REM | This BAT file is for testing purposes on Win32 boxes if  |
REM | having probs w nmake.exe. You should run it from the     |
REM | folder directly above the t/ test folder				   |
REM -----------------------------------------------------------
perl t/0_init.t
perl t/1_fkeys.t
perl t/2_insert.t
perl t/3_update.t
perl t/4_delete.t
perl t/5_copypaste.t
perl t/6_publish.t
perl t/7_misc.t
perl t/8_misc.t