<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xhtml="http://www.w3.org/1999/xhtml" exclude-result-prefixes="xhtml">
<xsl:output method="html" media-type="html" encoding="utf-8" 
doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN" doctype-system="http://www.w3.org/TR/html4/loose.dtd" indent="yes"/>
	<xsl:param name="department">5</xsl:param>
<xsl:template match="/" >
<html>
<head lang="en">
	<title><xsl:value-of select="/department/name" /> Staff</title>
	<meta name="description" content="" />
	<meta name="author" content="" />
</head>
<body>
<xsl:apply-templates select="staff"></xsl:apply-templates>
</body></html>
</xsl:template>

<xsl:template match="staff">
<h1><xsl:value-of select="name"></xsl:value-of></h1>
<table border="1">
<xsl:apply-templates select="member">
<xsl:sort select="familyname" order="ascending"></xsl:sort>
</xsl:apply-templates>
</table>
</xsl:template>

<xsl:template match="member">
<tr>
<td valign="top" nowrap="nowrap">
<strong><xsl:value-of select="name"></xsl:value-of></strong></td>
<td>
<xsl:if test="email/@display = '1'">
<a><xsl:attribute name="href">mailto:<xsl:value-of select="email" /></xsl:attribute><xsl:value-of select="email" /></a><br />
</xsl:if>
<xsl:value-of select="contact"></xsl:value-of><br />
</td>
</tr>
<tr>
<td colspan="2"><xsl:value-of select="expertise"></xsl:value-of></td>
</tr>
</xsl:template>


</xsl:stylesheet>
