# --------------------------------------------------------------------------------- 
# Description/Purpose:
#   Used to benchmark changes to parts of the perl libraries for XApperator
#	We create an instance of XApp, load the XML for a test app, then can
#	check the operation of any arbitrary method after its been changed
# --------------------------------------------------------------------------------- 
use warnings;
use strict;
use CGI;
use XApperator;
use Benchmark qw(:all) ;
use vars qw( $q $XApp );

my $q = new CGI;

# -----------------------------------------------------------------
# Test: create and parse
# -----------------------------------------------------------------

if (1) {
# simulate a CGi request by pre-loading some params...
$q->param(-name=>'x-app',-value=>'test');
$q->param(-name=>'x-fkey',-value=>'acc');
$q->param(-name=>'x-mode',-value=>'select');
$q->param(-name=>'user',-value=>'bob');
# now create our XApp object
$XApp = XApperator->new(
        cgi => $q,
        dieOnError => 1,
        detailsToUser => 1,
        rootDir => "t/",
        resourcesDir => "resources/",
        warnLevel => 0,
        XSLTDir => "./xslt/"
		);
}

my $count = 1000;
my $result='';

$XApp->_loadXML();

my $t = timeit ($count, sub { $result = $XApp->_recordCount(); } );
print "$count loops took:",timestr($t),"\n";
print "result: " . $result;
