# --------------------------------------------------------------------------------- 
# Original Release Date: 19/Jan/2006
# This Release Date: 19/Jan/2006
# Version: 0.18
#
# Description/Purpose:
# --------------------------------------------------------------------------------- 

use strict;
use CGI;
use XApperator;

my $syntax = "
Syntax:\n\tperl $0 <application> <input file>" .
    "\ne.g.\n \tperl $0 myapp sample-import.tab";

unless (@ARGV > 0) {
    dieNice('');
}

my $appName = shift;
my $inputFile = shift;

if ($inputFile eq '') { dieNice('No input file specified'); }
unless (-e $inputFile) {
    dieNice("Config file '$inputFile' not found");
}

my $cgi = new CGI;

# note: you will need to tweak the following paths if you're xapp
#	instance is not in the folder above the location of this script
my $XApp = XApperator->new(
        app => "$appName",
        cgi => $cgi,
        errorsToUser => 1,
        dieOnError => 1,    # default is '0'
        XSLTDir => '../xslt/',
        rootDir => '../',
        resourcesDir => 'resources/',
        resourcesPath => "http://www.mysite.com/XApp/resources/",
);

open(INFILE, "< $inputFile") || dieNice('Cant open file');
print "Importing key/value pairs...\n\n";
my $keyCount = 0;
while (<INFILE>) {
	my ($key, $label) = split("\t",$_);
	if (($key ne '') and ($label ne '')) {
		if ($XApp->_fkeyValOK($key)) {
			$keyCount++;
			print "$key\n";
		} else {
			print "invalid key: '$key' - skipping\n";
		}
	}
}
close INFILE;


exit;




sub dieNice {
# ---------------------------------------------------------------------------
# print the error message and the syntax string
# ---------------------------------------------------------------------------
    my $msg = shift;
    print $msg . $syntax;
    exit;
}