# --------------------------------------------------------------------------------- 
# Original Release Date: 19/Jan/2006
# This Release Date: 19/Jan/2006
# Version: 0.18
#
# Description/Purpose:
#	This script creates stub XML representing a sample record or sample container
#	file as specified in the config file for a given XApperator application.
# --------------------------------------------------------------------------------- 

use strict;
use CGI;
use XApperator;

unless (@ARGV > 0) {
    print "$0
	This script creates stub XML representing a sample record or
	sample container file as specified in the config file for a
	given XApperator application.
Syntax:\n\tperl $0 <application> <output type>" .
    "\ne.g.\n \tperl $0 blog";
    "\ne.g.\n \tperl $0 xapp record (output a record only)";
    "\ne.g.\n \tperl $0 phonebook file (output container file only)";
	exit;
}

my $appName = shift;
my $outputType = shift;
my $cgi = new CGI;

# note: you will need to tweak the following paths if you're xapp
#	instance is not in the folder above the location of this script
my $XApp = XApperator->new(
        app => "$appName",
        cgi => $cgi,
        errorsToUser => 1,
        dieOnError => 1,    # default is '0'
        XSLTDir => '../xslt/',
        rootDir => '../',
        resourcesDir => 'resources/',
        resourcesPath => "http://www.mysite.com/XApp/resources/",
);

if (($outputType eq '') || ($outputType eq 'file')) {
	unless ($outputType eq 'file') { print "\nContainer file:\n"; }
	my $doc = XML::LibXML->createDocument;
	my $rootNode = $doc->createElement( $XApp->{rootName}  );
	$doc->setDocumentElement( $rootNode );
	print $doc->toString;
}

if (($outputType eq '') || ($outputType eq 'record')) {
	unless ($outputType eq 'record') { print "\nSample Record:\n"; }
	my $newRecord = $XApp->_createRecord();
	print $newRecord->toString;
}

exit;
