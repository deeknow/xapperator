# --------------------------------------------------------------------------------- 
# Original Release Date: 20/Jun/2005
# This Release Date: 20/June/2005
# Version: 0.15
#
# Description/Purpose:
#   This script is used to perform DTD based validation of a users XApperator
#   config.xml file, should they be using an XML editor which does not support
#   validation
# --------------------------------------------------------------------------------- 

use XML::LibXML;

my $syntax = "\n\nSyntax:\n\tperl $0 <pathToConfig> <pathToDTD>" .
    "\ne.g.\n \tperl $0 apps/thisapp/config.xml config.dtd";

unless (@ARGV == 2) {
    dieNice('');
}

my $configFile = shift;
my $DTDFile = shift;
unless (-e $configFile) {
    dieNice("Config file '$configFile' not found");
}
unless (-e $DTDFile) {
    dieNice("DTD file '$DTDFile' not found");
}

my $parser = XML::LibXML->new();
my $config = $parser->parse_file($configFile);

my $dtdContent = '';
local($/) = undef;
open (INFILE, "< $DTDFile");
$dtdContent = <INFILE>;
close (INFILE);
my $dtd = XML::LibXML::Dtd->parse_string( $dtdContent  );

eval {
    if ($config->validate( $dtd )) {
        print "\nYep, file validates";
    }
};

if ($@) {
    dieNice("Sorry, file does not validate...\n\n" . $@);
}



sub dieNice {
# ---------------------------------------------------------------------------
# print the error message and the syntax string
# ---------------------------------------------------------------------------
    my $msg = shift;
    print $msg . $syntax;
    exit;
}