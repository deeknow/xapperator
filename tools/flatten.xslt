<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

<xsl:template match="/">
	<staff>
		<xsl:apply-templates select="phonebook/dept[@name='ITS']/member"></xsl:apply-templates>
	</staff>
</xsl:template>

<xsl:template match="member">
	<member>
		<user><xsl:value-of select="@user"></xsl:value-of></user>
		<email><xsl:value-of select="email"></xsl:value-of></email>
		<xsl:choose>
		<xsl:when test="email[@display = '1']"><showemail>1</showemail></xsl:when>
		<xsl:otherwise><showemail>0</showemail></xsl:otherwise>
		</xsl:choose>
		<xsl:copy-of select="name"></xsl:copy-of>
		<xsl:copy-of select="familyname"></xsl:copy-of>
		<xsl:copy-of select="contact"></xsl:copy-of>
	</member>	
</xsl:template>

</xsl:stylesheet>
