<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xapp="http://xapperator.sourceforge.net/"
	exclude-result-prefixes="xapp">
<!--

LICENSE

XApperator: simple XML editing/publishing framework - edting XSLT file
Copyright (C) 2005-2007  Dean Stringer (deeknow @ pobox . com)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Version 1.0.1
Description:

	This XSLT file handles editing of new foreign key/value pairs
	
-->

<xsl:import href="utils.xslt"></xsl:import>

<xsl:param name="key"></xsl:param>
<xsl:param name="fkey"></xsl:param>
<xsl:param name="tkey"></xsl:param>
<xsl:param name="type"></xsl:param>
<xsl:param name="oldkey"></xsl:param>
<xsl:param name="newkey"></xsl:param>
<xsl:param name="fkeyLabel"></xsl:param>
<xsl:param name="app"></xsl:param>
<xsl:param name="mode"></xsl:param>
<xsl:param name="pmode"></xsl:param>
<xsl:param name="display"></xsl:param>
<xsl:param name="displayAlert"></xsl:param>
<xsl:param name="displayError"></xsl:param>
<xsl:param name="relativeURL" select="'controller.cgi'"></xsl:param>
<xsl:param name="resourcesPath"></xsl:param>
<xsl:param name="lang" select="'en'"></xsl:param>
<xsl:param name="dateNow"></xsl:param>
<xsl:param name="timeNow"></xsl:param>
<xsl:param name="dateEntryFormat"></xsl:param>
<xsl:param name="sortOrder"></xsl:param>
<xsl:param name="sortBy"></xsl:param>

<xsl:variable name="recordName" select="config/schema/recordname"></xsl:variable>
<xsl:variable name="rootName" select="config/schema/rootname"></xsl:variable>
<xsl:variable name="fkeyDisplayLabel" select="foreignkeys/key[value = $fkey]/label"></xsl:variable>

<xsl:output method="html" omit-xml-declaration="yes" />


<!-- ======================================================================= -->
<xsl:template match="/application">
<!-- ======================================================================= -->

<xsl:variable name="indexFieldName">
<xsl:choose>
<xsl:when test="config/schema/fields/field[@isindex='1']"><xsl:value-of select="config/schema/fields/field[@isindex='1'][1]/@name"></xsl:value-of></xsl:when>
<xsl:otherwise>xapp:id</xsl:otherwise>
</xsl:choose>
</xsl:variable>


<!-- ======================================================================= -->
<xsl:choose>
<!-- ======================================================================= -->

<xsl:when test="$mode = 'new'">
<!-- weve been asked to add a new fkey label/value pair to KEYS.xml so show a blank submit form -->
<div class="edit">
<h2><xsl:value-of select="config/titles/addingKey[lang($lang)]"></xsl:value-of></h2>
<p><xsl:value-of select="config/messages/addingKey[lang($lang)]"></xsl:value-of></p>
<xsl:if test="config/schema/clientsidevalidation = 'Javascript'">
	<xsl:call-template name="addValidateFKeyJS"></xsl:call-template>
</xsl:if>
<form action="{$relativeURL}" method="POST" name="editFKey">
<input type="hidden" name="x-app" value="{$app}"></input>
<input type="hidden" name="x-type" value="keys"></input>
<table class="record" cellspacing="5" border="0">
<tbody>
<tr>
<td>Label</td><td><input type="text" name="x-label"></input></td>
</tr>
<tr>
<td>Value</td><td><input type="text" name="x-value"></input></td>
</tr>
</tbody>
</table>
<button type="submit" value="Save" name="x-mode" onclick="return validateFKey(document.editFKey);"><xsl:value-of select="config/titles/save[lang($lang)]"></xsl:value-of></button>
<xsl:text> </xsl:text><button type="submit" value="Cancel" name="x-mode"><xsl:value-of select="config/titles/cancel[lang($lang)]"></xsl:value-of></button>
</form>
</div>
</xsl:when>

<!-- ======================================================================= -->

<xsl:otherwise>
<!-- We may have got here coz there arent any records to display, so show nothing -->
</xsl:otherwise>

<!-- ======================================================================= -->
</xsl:choose>
<!-- ======================================================================= -->

</xsl:template>

</xsl:stylesheet>