<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xapp="http://xapperator.sourceforge.net/"
	exclude-result-prefixes="xapp">
<!--

LICENSE

XApperator: simple XML editing/publishing framework - edting XSLT file
Copyright (C) 2005-2007  Dean Stringer (deeknow @ pobox . com)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Version 1.0.1
Description:

	This XSLT file handles extraction of application data records to be edited and
	presentation of them in editable FORM elements wrapped in a TABLE
	There are two templates:
		1) one that matches on /application and checks to see what mode
			we're in, edit, add, or default of list all records
		2) another thats called to display a field as a FORM element
			depending on its type which is specified in the applications
			config.xml file
	
-->

<xsl:import href="utils.xslt"></xsl:import>

<xsl:param name="key"></xsl:param>
<xsl:param name="fkey"></xsl:param>
<xsl:param name="tkey"></xsl:param>
<xsl:param name="type"></xsl:param>
<xsl:param name="oldkey"></xsl:param>
<xsl:param name="newkey"></xsl:param>
<xsl:param name="fkeyLabel"></xsl:param>
<xsl:param name="app"></xsl:param>
<xsl:param name="mode"></xsl:param>
<xsl:param name="pmode"></xsl:param>
<xsl:param name="display"></xsl:param>
<xsl:param name="displayAlert"></xsl:param>
<xsl:param name="displayError"></xsl:param>
<xsl:param name="relativeURL" select="'controller.cgi'"></xsl:param>
<xsl:param name="resourcesPath"></xsl:param>
<xsl:param name="lang" select="'en'"></xsl:param>
<xsl:param name="dateNow"></xsl:param>
<xsl:param name="timeNow"></xsl:param>
<xsl:param name="dateEntryFormat"></xsl:param>
<xsl:param name="sortOrder"></xsl:param>
<xsl:param name="sortBy"></xsl:param>

<xsl:variable name="recordName" select="config/schema/recordname"></xsl:variable>
<xsl:variable name="rootName" select="config/schema/rootname"></xsl:variable>
<xsl:variable name="fkeyDisplayLabel" select="foreignkeys/key[value = $fkey]/label"></xsl:variable>

<xsl:output method="html" omit-xml-declaration="yes" />


<!-- ======================================================================= -->
<xsl:template match="/application">
<!-- ======================================================================= -->

<xsl:variable name="indexFieldName">
<xsl:choose>
<xsl:when test="config/schema/fields/field[@isindex='1']"><xsl:value-of select="config/schema/fields/field[@isindex='1'][1]/@name"></xsl:value-of></xsl:when>
<xsl:otherwise>xapp:id</xsl:otherwise>
</xsl:choose>
</xsl:variable>


<!-- ======================================================================= -->
<xsl:choose>
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<xsl:when test="($mode = 'edit') and ($key != '')">
<!-- ======================================================================= -->
<!-- weve been asked for a specific record so show that -->
<div class="edit">
<xsl:variable name="indexFieldLabel" select="config/schema/fields/field[@isindexlabel='1'][1]/@name"></xsl:variable>
<!-- check each of the data records -->
<xsl:for-each select="data/*/*[name() = $recordName]">
<xsl:variable name="selectedRecord" select="."></xsl:variable>
<!-- and each of those record's fields -->
<xsl:for-each select="*">
<!-- check if this is the index field, and has the value of the record we're looking for -->
<xsl:if test="(name(.) = $indexFieldName) and (. = $key)">
<!-- include some client-side validation of edit form if specified in config -->
<xsl:if test="/application/config/schema/clientsidevalidation = 'Javascript'">
<xsl:call-template name="addValidateFormJS"></xsl:call-template>
</xsl:if>
<form action="{$relativeURL}" method="POST" name="edit">
<input type="hidden" name="x-app" value="{$app}"></input>
<!-- echo the old index value in hidden field in case user changes it in edit mode -->
<input type="hidden" name="x-oldkey" value="{$key}"></input>
<input type="hidden" name="x-fkey" value="{$fkey}"></input>
<!-- if using the default index field (xapp:id) then include it as a hidden field -->
<xsl:if test="$indexFieldName = 'xapp:id'">
<input type="hidden" name="xapp:id" value="{$key}"></input>
</xsl:if>
<div class="info">
<!-- step thru each of the sibling fields for this record to find the one thats marked
	as index field label (coz its not neccessarily the index field itself) -->
<xsl:for-each select="parent::*/*">
<xsl:if test="name(.) = $indexFieldLabel"><h2><xsl:value-of select="/application/config/titles/editing[lang($lang)]" />: <span class="field-heading"><xsl:value-of select="."></xsl:value-of></span></h2>
</xsl:if>
</xsl:for-each>
<p><xsl:value-of select="/application/config/messages/edit[lang($lang)]"></xsl:value-of></p>
</div>
<table class="record" cellspacing="5" border="0">
<tbody>
<xsl:for-each select="/application/config/schema/fields/field">
<xsl:variable name="thisFieldName" select="@name"></xsl:variable>
<!-- get the field value by checking each child element of the selected record -->
<xsl:variable name="thisFieldValue">
<xsl:for-each select="$selectedRecord/*">
<xsl:if test="name() = $thisFieldName">
<xsl:value-of select="text()"></xsl:value-of>
</xsl:if>
</xsl:for-each>
</xsl:variable>
<!-- got both name and value now so call showField to display them -->
<xsl:call-template name="showField">
<xsl:with-param name="nodeName" select="$thisFieldName"></xsl:with-param>
<xsl:with-param name="nodeValue" select="$thisFieldValue"></xsl:with-param>
</xsl:call-template>
</xsl:for-each>
</tbody>
</table>
<!-- add a javascript init() if any textarea fields are WYSIWYG editor type -->
<xsl:if test="/application/config/schema/fields/field[@type = 'wysiwyg']">
<xsl:call-template name="showWYSIWYGEditor">
	<xsl:with-param name="config" select="/application/config"></xsl:with-param>
</xsl:call-template>
</xsl:if>
<xsl:choose>
<xsl:when test="/application/config/schema/clientsidevalidation = 'Javascript'">
	<button type="submit" value="Save" name="" title="Save"  onclick="this.name='x-mode'; return validate(document.edit);"><xsl:value-of select="/application/config/titles/save[lang($lang)]"></xsl:value-of></button>
</xsl:when>
<xsl:otherwise>
	<button type="submit" value="Save" name="" title="Save" onclick="this.name='x-mode';"><xsl:value-of select="/application/config/titles/save[lang($lang)]"></xsl:value-of></button>
</xsl:otherwise>
</xsl:choose>
<xsl:text> </xsl:text><button type="submit" value="Cancel" name="" onclick="this.name='x-mode';"><xsl:value-of select="/application/config/titles/cancel[lang($lang)]"></xsl:value-of></button>
<xsl:if test="/application/config/actions/delete/@enabled = '1'">
<xsl:text> </xsl:text><button type="submit" value="Delete" name="" onClick="this.name='x-mode'; return checkdelete(this);" ><xsl:value-of select="/application/config/titles/delete[lang($lang)]"></xsl:value-of></button>
</xsl:if>
</form>
</xsl:if>
</xsl:for-each>
</xsl:for-each>
</div>
</xsl:when>

<!-- ======================================================================= -->
<xsl:when test="$display = 'confirmdelete'">
<!-- ======================================================================= -->

<!-- have been asked to delete but need to confirm -->
<div class="edit">
<div class="info">
<h2><xsl:value-of select="config/titles/confirmdelete[lang($lang)]"></xsl:value-of></h2>
<p><xsl:value-of select="config/messages/confirmdelete[lang($lang)]"></xsl:value-of></p>
</div>
<form action="{$relativeURL}" method="POST" name="edit">
<input type="hidden" name="x-app" value="{$app}"></input>
<input type="hidden" name="x-key" value="{$key}"></input>
<input type="hidden" name="x-fkey" value="{$fkey}"></input>
<xsl:if test="$type = 'keys'"><input type="hidden" name="x-type" value="keys"></input></xsl:if>
<input type="hidden" name="x-confirm" value="Yes"></input>
<button type="submit" value="Delete" name="" onclick="this.name='x-mode';"><xsl:value-of select="config/titles/delete[lang($lang)]"></xsl:value-of></button>
<xsl:text> </xsl:text><button type="submit" value="Cancel" name="" onclick="this.name='x-mode';"><xsl:value-of select="config/titles/cancel[lang($lang)]"></xsl:value-of></button>
</form>
</div>
</xsl:when>

<!-- ======================================================================= -->
<xsl:when test="$mode = 'message'">
<!-- ======================================================================= -->

<div class="edit">
<xsl:choose>
<xsl:when test="$displayAlert != ''">
<!-- check if the display is a type of alert (ie an error) and display that -->
<div class="alert">
<h2 class=""><xsl:value-of select="config/titles/*[name() = $displayAlert and lang($lang)]"></xsl:value-of></h2>
<p><xsl:value-of select="config/messages/*[name() = $displayAlert and lang($lang)]" disable-output-escaping="yes"></xsl:value-of>
<xsl:if test="$displayError != ''">
<br></br><xsl:value-of select="$displayError"></xsl:value-of>
</xsl:if>
</p>
</div>
</xsl:when>
<xsl:otherwise>
<div class="info">
<h2><xsl:value-of select="config/titles/*[name() = $display and lang($lang)]"></xsl:value-of></h2>
<p><xsl:value-of select="config/messages/*[name() = $display and lang($lang)]" disable-output-escaping="yes"></xsl:value-of></p>
</div>
</xsl:otherwise>
</xsl:choose>
</div>
</xsl:when>

<!-- ======================================================================= -->
<xsl:when test="($display = 'saved') and (config/actions/edit/@popup = '1')">
<!-- ======================================================================= -->
<div class="info">
<h2><xsl:value-of select="config/titles/*[name() = $display and lang($lang)]"></xsl:value-of></h2>
<p><xsl:value-of select="config/messages/*[name() = $display and lang($lang)]" disable-output-escaping="yes"></xsl:value-of></p>
</div>
</xsl:when>

<!-- ======================================================================= -->
<xsl:otherwise>
<!-- We may have got here coz there arent any records to display, so show nothing -->
</xsl:otherwise>
<!-- ======================================================================= -->

</xsl:choose>

<!-- ======================================================================= -->
</xsl:template>
<!-- ======================================================================= -->

</xsl:stylesheet>