<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--

LICENSE

XApperator: simple XML editing/publishing framework - menu XSLT file
Copyright (C) 2005-2007  Dean Stringer (deeknow @ pobox . com)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Version 1.0.1
Description:
        used to render a dropdown menu of foreign keys so user can
        select a datafile/set

-->

<xsl:param name="fkey"></xsl:param>
<xsl:param name="app"></xsl:param>
<xsl:param name="method" select="'POST'"></xsl:param>
<xsl:param name="relativeURL" select="'controller.cgi'"></xsl:param>
<xsl:param name="publishEnabled"></xsl:param>
<xsl:param name="publishAllEnabled"></xsl:param>
<xsl:param name="addEnabled"></xsl:param>
<xsl:param name="selectEnabled"></xsl:param>
<xsl:param name="deleteEnabled"></xsl:param>
<xsl:param name="viewEnabled"></xsl:param>
<xsl:param name="xmlEnabled"></xsl:param>
<xsl:param name="menuPrompt"></xsl:param>
<xsl:param name="menuPromptEmpty"></xsl:param>
<xsl:param name="mode"></xsl:param>
<xsl:param name="lang"></xsl:param>

<xsl:output method="html" omit-xml-declaration="yes" media-type="text/html"/>

<!-- ======================================================================= -->


<xsl:template match="/application">
<div class="menu">
<xsl:choose>
<xsl:when test="foreignkeys/key"><p><xsl:value-of select="$menuPrompt"></xsl:value-of></p></xsl:when>
<xsl:when test="$menuPromptEmpty != ''"><p><xsl:value-of select="$menuPromptEmpty"></xsl:value-of></p></xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
<form action="{$relativeURL}" method="GET">
<input type="hidden" name="x-app" value="{$app}"></input>
<input type="hidden" name="x-type" value="keys"></input>
<xsl:if test="foreignkeys/key">
<select name="x-fkey">
<xsl:apply-templates select="foreignkeys/key">
<xsl:sort select="label" />
</xsl:apply-templates>
</select>
<xsl:if test="$selectEnabled = '1'">
<xsl:text> </xsl:text><button type="submit" value="Select" name="x" onclick="this.name='x-mode';"><xsl:value-of select="config/titles/select[lang($lang)]"></xsl:value-of></button>
</xsl:if>
<xsl:if test="$publishEnabled = '1'">
<xsl:text> </xsl:text><button type="submit" value="Publish" name="" onclick="this.name='x-mode';"><xsl:value-of select="config/titles/publish[lang($lang)]"></xsl:value-of></button>
</xsl:if>
<xsl:if test="$publishAllEnabled = '1'">
<xsl:text> </xsl:text><button type="submit" value="Publish All" name="" onclick="this.name='x-mode';"><xsl:value-of select="config/titles/publishall[lang($lang)]"></xsl:value-of></button>
</xsl:if>
</xsl:if>
<xsl:if test="$addEnabled = '1'">
<xsl:text> </xsl:text><button type="submit" value="New" name="" onclick="this.name='x-mode';"><xsl:value-of select="config/titles/new[lang($lang)]"></xsl:value-of></button>
</xsl:if>
<xsl:if test="foreignkeys/key">
<xsl:if test="$deleteEnabled = '1'">
<xsl:text> </xsl:text><button type="submit" value="Delete" name="" onclick="this.name='x-mode';"><xsl:value-of select="config/titles/delete[lang($lang)]"></xsl:value-of></button>
</xsl:if>
<xsl:if test="$viewEnabled = '1'">
<xsl:text> </xsl:text><button type="submit" value="View" name="" onclick="this.name='x-mode';"><xsl:value-of select="config/titles/view[lang($lang)]"></xsl:value-of></button>
</xsl:if>
<xsl:if test="$xmlEnabled = '1'">
<xsl:text> </xsl:text><button type="submit" value="XML" name="" onclick="this.name='x-mode';">XML</button>
</xsl:if>
</xsl:if>
</form>
</div>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template match="key">
<option><xsl:attribute name="value"><xsl:value-of select="value" /></xsl:attribute><xsl:if test="value  = $fkey"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
<xsl:value-of select="label" /></option>
<xsl:text>
</xsl:text>
</xsl:template>

</xsl:stylesheet>
