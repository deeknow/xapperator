<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xapp="http://xapperator.sourceforge.net/"
	exclude-result-prefixes="xapp">
<!--

LICENSE

XApperator: simple XML editing/publishing framework - edting XSLT file
Copyright (C) 2005-2007  Dean Stringer (deeknow @ pobox . com)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Version 1.0.1
Description:

	This XSLT file handles listing of records availabl for selection
	
-->

<xsl:import href="utils.xslt"></xsl:import>

<xsl:param name="key"></xsl:param>
<xsl:param name="fkey"></xsl:param>
<xsl:param name="tkey"></xsl:param>
<xsl:param name="type"></xsl:param>
<xsl:param name="oldkey"></xsl:param>
<xsl:param name="newkey"></xsl:param>
<xsl:param name="fkeyLabel"></xsl:param>
<xsl:param name="app"></xsl:param>
<xsl:param name="mode"></xsl:param>
<xsl:param name="pmode"></xsl:param>
<xsl:param name="display"></xsl:param>
<xsl:param name="displayAlert"></xsl:param>
<xsl:param name="displayError"></xsl:param>
<xsl:param name="relativeURL" select="'controller.cgi'"></xsl:param>
<xsl:param name="resourcesPath"></xsl:param>
<xsl:param name="lang" select="'en'"></xsl:param>
<xsl:param name="dateNow"></xsl:param>
<xsl:param name="timeNow"></xsl:param>
<xsl:param name="dateEntryFormat"></xsl:param>
<xsl:param name="sortOrder"></xsl:param>
<xsl:param name="sortBy"></xsl:param>

<xsl:variable name="recordName" select="config/schema/recordname"></xsl:variable>
<xsl:variable name="rootName" select="config/schema/rootname"></xsl:variable>
<xsl:variable name="fkeyDisplayLabel" select="foreignkeys/key[value = $fkey]/label"></xsl:variable>

<xsl:output method="html" omit-xml-declaration="yes" />


<xsl:template match="/application">

<xsl:variable name="indexFieldName">
<xsl:choose>
<xsl:when test="config/schema/fields/field[@isindex='1']"><xsl:value-of select="config/schema/fields/field[@isindex='1'][1]/@name"></xsl:value-of></xsl:when>
<xsl:otherwise>xapp:id</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<!-- ======================================================================= -->
<xsl:choose>
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<xsl:when test="(count(foreignkeys/key) > 0)">
<!-- just show the list of records that are editable -->
<!-- ======================================================================= -->
<div class="edit">
<!-- check to see if we need to display some message to user first -->
<xsl:if test="($display != '') and ($display != 'select')">
<div class="info">
<h2 class=""><xsl:value-of select="config/titles/*[name() = $display and lang($lang)]"></xsl:value-of></h2>
<p><xsl:value-of select="config/messages/*[name() = $display and lang($lang)]" disable-output-escaping="yes"></xsl:value-of></p>
</div>
</xsl:if>
<!-- check if the display is a type of alert (ie an error) and display that -->
<xsl:if test="$displayAlert != ''">
<div class="alert">
<h2 class=""><xsl:value-of select="config/titles/*[name() = $displayAlert and lang($lang)]"></xsl:value-of></h2>
<p><xsl:value-of select="config/messages/*[name() = $displayAlert and lang($lang)]" disable-output-escaping="yes"></xsl:value-of>
<xsl:if test="$displayError != ''">
<br></br><xsl:value-of select="$displayError"></xsl:value-of>
</xsl:if>
</p>
</div>
</xsl:if>

<xsl:if test="not (($type = 'keys') and ($mode = 'cancel'))">

<!-- show a generalised message -->
<div class="info">
<h2 class=""><xsl:value-of select="config/titles/editing[lang($lang)]" /><xsl:text> </xsl:text> <xsl:value-of select="$fkeyLabel"></xsl:value-of><xsl:text>: </xsl:text><span class="field-heading"><xsl:value-of select="$fkeyDisplayLabel"></xsl:value-of></span></h2>
<!-- display a special message about the recordset being empty if there arent any records -->
<xsl:choose>
	<xsl:when test="count(data/*/*) > 0"><p><xsl:value-of select="config/messages/select[lang($lang)]" disable-output-escaping="yes"></xsl:value-of></p></xsl:when>
	<xsl:otherwise><p><xsl:value-of select="config/messages/empty[lang($lang)]"></xsl:value-of></p></xsl:otherwise>
</xsl:choose>
</div>

<!-- get the index field names and labels from config -->
<xsl:variable name="indexFieldLabel" select="config/schema/fields/field[@isindexlabel='1'][1]/@label"></xsl:variable>
<xsl:variable name="previewFieldName" select="config/schema/fields/field[@isindexlabel='1'][1]/@name"></xsl:variable>
<!-- allow up to two additional display fields to be used for ordering of rows -->
<xsl:variable name="sortableFieldName1" select="config/schema/fields/field[(@sortable='1') and (@name != $indexFieldName)][1]/@name"></xsl:variable>
<xsl:variable name="sortableFieldName2" select="config/schema/fields/field[(@sortable='1') and (@name != $indexFieldName)][2]/@name"></xsl:variable>
<form action="{$relativeURL}" method="GET" name="records">
<input type="hidden" name="x-app" value="{$app}"></input>
<input type="hidden" name="x-fkey" value="{$fkey}"></input>
<xsl:if test="($mode = 'copy') and ($key != '')">
<input type="hidden" name="x-pmode" value="copy"></input>
<input type="hidden" name="x-tkey" value="{$key}"></input>
</xsl:if>
<xsl:if test="($mode = 'cut') and ($key != '')">
<input type="hidden" name="x-pmode" value="cut"></input>
<input type="hidden" name="x-tkey" value="{$key}"></input>
</xsl:if>
<xsl:if test="(($pmode = 'copy') or ($pmode = 'cut')) and ($key = '')">
<input type="hidden" name="x-pmode" value="{$pmode}"></input>
</xsl:if>
<xsl:if test="($mode = 'paste') and ($key = '')"><input type="hidden" name="x-tkey" value="{$tkey}"></input></xsl:if>

<table width="80%" cellpadding="1" cellspacing="0">

<!-- add the table row header, including sortable display columns -->
<xsl:variable name="otherSortOrder">
<xsl:choose>
<xsl:when test="$sortOrder = 'ascending'">descending</xsl:when>
<xsl:otherwise>ascending</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<tr>

<!--  show the preview field, clickable to sort  -->
<xsl:variable name="arrowImgIndex">
<xsl:call-template name="chooseSortArrow">
<xsl:with-param name="fieldName" select="$previewFieldName"></xsl:with-param>
</xsl:call-template>
</xsl:variable>
<th style="padding-left: 1em;'" valign="top"><a href="{$relativeURL}?x-app={$app}&amp;x-fkey={$fkey}&amp;x-mode=Select&amp;x-sortorder={$otherSortOrder}&amp;x-sortby={$previewFieldName}"><xsl:value-of select="$indexFieldLabel"></xsl:value-of><xsl:value-of select="$arrowImgIndex" disable-output-escaping="yes"></xsl:value-of></a></th>

<!--  show the 1st sortable column if there is one -->
<xsl:if test="$sortableFieldName1 != ''">
<xsl:variable name="sortableFieldLabel" select="config/schema/fields/field[(@sortable='1') and (@name != $indexFieldName)][1]/@label"></xsl:variable>
<xsl:variable name="colLabel">
<xsl:choose>
<xsl:when test="$sortableFieldLabel"><xsl:value-of select="$sortableFieldLabel"></xsl:value-of></xsl:when>
<xsl:otherwise><xsl:value-of select="$sortableFieldName1"></xsl:value-of></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:variable name="arrowImg">
<xsl:call-template name="chooseSortArrow">
<xsl:with-param name="fieldName" select="$sortableFieldName1"></xsl:with-param>
</xsl:call-template>
</xsl:variable>
<th valign="top"><a href="{$relativeURL}?x-app={$app}&amp;x-fkey={$fkey}&amp;x-mode=Select&amp;x-sortorder={$otherSortOrder}&amp;x-sortby={$sortableFieldName1}"><xsl:value-of select="$colLabel"></xsl:value-of><xsl:value-of select="$arrowImg" disable-output-escaping="yes"></xsl:value-of></a></th>
</xsl:if>

<!--  show the 2nd sortable column if there is one -->
<xsl:if test="$sortableFieldName2 != ''">
<xsl:variable name="sortableFieldLabel" select="config/schema/fields/field[(@sortable='1') and (@name != $indexFieldName)][2]/@label"></xsl:variable>
<xsl:variable name="colLabel">
<xsl:choose>
<xsl:when test="$sortableFieldLabel"><xsl:value-of select="$sortableFieldLabel"></xsl:value-of></xsl:when>
<xsl:otherwise><xsl:value-of select="$sortableFieldName2"></xsl:value-of></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:variable name="arrowImg">
<xsl:call-template name="chooseSortArrow">
<xsl:with-param name="fieldName" select="$sortableFieldName2"></xsl:with-param>
</xsl:call-template>
</xsl:variable>
<th valign="top"><a href="{$relativeURL}?x-app={$app}&amp;x-fkey={$fkey}&amp;x-mode=Select&amp;x-sortorder={$otherSortOrder}&amp;x-sortby={$sortableFieldName2}"><xsl:value-of select="$colLabel"></xsl:value-of><xsl:value-of select="$arrowImg" disable-output-escaping="yes"></xsl:value-of></a></th>
</xsl:if>

</tr>

<!-- step thru each one of the data records and get its index value and display label so we can list them and make selectable -->
<xsl:for-each select="data/*/*">
<xsl:sort order="{$sortOrder}" select="*[name() = $sortBy]"></xsl:sort>
<tr>
<xsl:variable name="class">
<xsl:choose>
	<xsl:when test="(position() mod 2 = 1)">XAppRowEven</xsl:when>
	<xsl:otherwise>XAppRowOdd</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<td class="{$class}">
<!-- get the value for the index field for this record -->
<xsl:variable name="thisRecordIndexValue">
<xsl:choose>
<xsl:when test="($indexFieldName = 'xapp:id') and xapp:id != ''"><xsl:value-of select="xapp:id"></xsl:value-of></xsl:when>
<xsl:when test="($indexFieldName = 'xapp:id') and xapp:id = ''"><xsl:value-of select="$key"></xsl:value-of></xsl:when>
<xsl:otherwise><xsl:value-of select="*[name() = $indexFieldName]"></xsl:value-of></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<!-- we need to place the name in a single quoted Javascript call so need to escape single quotes (turn them into ") -->
<xsl:variable name="thisEscapedName"><xsl:call-template name="searchReplace"><xsl:with-param name="textString"><xsl:value-of select="$thisRecordIndexValue"></xsl:value-of></xsl:with-param><xsl:with-param name="textFind">'</xsl:with-param><xsl:with-param name="textReplace">"</xsl:with-param></xsl:call-template></xsl:variable>
<input id="id{$thisEscapedName}" type="radio" name="x-key" value="{$thisRecordIndexValue}"></input>
<!-- get the label for this record (if there is one) -->
<xsl:variable name="thisLabel" select="*[name() = $previewFieldName]"></xsl:variable>
<xsl:choose>
<xsl:when test="$thisLabel != ''">
<span  ondblclick="javascript:editRecord('id{$thisEscapedName}');" onclick="tickID('id{$thisEscapedName}');">
<xsl:value-of select="$thisLabel"></xsl:value-of></span>
</xsl:when>
<!-- no label so just use the index value -->
<xsl:otherwise>
<span onclick="tickID('id{$thisEscapedName}');"><xsl:value-of select="$thisRecordIndexValue"></xsl:value-of></span>
</xsl:otherwise>
</xsl:choose>
<xsl:if test="($mode = 'cut') and ($thisRecordIndexValue = $key)"><xsl:text> </xsl:text><em> - cut</em></xsl:if>
<xsl:if test="($mode = 'copy') and ($thisRecordIndexValue = $key)"><xsl:text> </xsl:text><em> - copied</em></xsl:if>
<xsl:if test="($mode = 'paste') and ($thisRecordIndexValue = $newkey)"><xsl:text> </xsl:text><em> - pasted</em></xsl:if>
</td>
<!-- add the sortable display columns -->
<xsl:for-each select="*">
<xsl:if test="(name() = $sortableFieldName1) or (name() = $sortableFieldName2)">
<td  class="{$class}">
<xsl:value-of select="."></xsl:value-of>
</td>
</xsl:if>
</xsl:for-each>
</tr>
</xsl:for-each>
</table>
<br />
<!-- make sure we've got some records, if not, no point in showing the Edit or Delete modes -->
<xsl:variable name="okToAdd" select="(config/schema/maxOccurs = '') or (count(data//*[name() = $recordName]) &lt; config/schema/maxOccurs)"></xsl:variable>
<xsl:if test="(config/actions/add/@enabled = '1') and ($okToAdd)">
	<button type="submit" value="Add" name="" onclick="this.name='x-mode';"><xsl:value-of select="config/titles/add[lang($lang)]"></xsl:value-of></button><xsl:text> </xsl:text>
</xsl:if>
<xsl:if test="count(data/*/*) > 0">
<xsl:if test="config/actions/edit/@enabled = '1'">
	<button type="submit" value="Edit" name="" id="x-mode-Edit" onclick="this.name='x-mode'; return checkSelected('Edit');"><xsl:value-of select="config/titles/edit[lang($lang)]"></xsl:value-of></button><xsl:text> </xsl:text>
</xsl:if>
<xsl:if test="config/actions/delete/@enabled = '1'">
	<button type="submit" value="Delete" name="" onclick="this.name='x-mode'; return checkSelected('Delete');"><xsl:value-of select="config/titles/delete[lang($lang)]"></xsl:value-of></button><xsl:text> </xsl:text>
</xsl:if>
</xsl:if>
<xsl:variable name="recordsExist" select="count(data//*[name() = $recordName]) > 0"></xsl:variable>
<xsl:if test="(config/actions/insert/@enabled = '1') and ($recordsExist) and ($okToAdd)">
	<button type="submit" value="Insert" name="" onclick="this.name='x-mode';"><xsl:value-of select="config/titles/insert[lang($lang)]"></xsl:value-of></button><xsl:text> </xsl:text>
</xsl:if>
<xsl:if test="(config/actions/copypaste/@enabled = '1') and ($recordsExist) and ($okToAdd)">
	<button type="submit" value="Copy" name="" onclick="this.name='x-mode'; return checkSelected('Copy');"><xsl:value-of select="config/titles/copy[lang($lang)]"></xsl:value-of></button><xsl:text> </xsl:text>
</xsl:if>
<xsl:if test="(config/actions/copypaste/@enabled = '1') and ($recordsExist) and ($okToAdd)">
	<button type="submit" value="Cut" name="" onclick="this.name='x-mode'; return checkSelected('Cut');"><xsl:value-of select="config/titles/cut[lang($lang)]"></xsl:value-of></button><xsl:text> </xsl:text>
</xsl:if>
<xsl:if test="(config/actions/copypaste/@enabled = '1') and ($okToAdd) and
	( (($mode = 'copy') and ($key != '')) or (($mode = 'cut') and ($key != '')) or (($mode = 'paste') and ($key = '')) )">
	<button type="submit" value="Paste" name="" onclick="this.name='x-mode';"><xsl:value-of select="config/titles/paste[lang($lang)]"></xsl:value-of></button>
<!--	<input type="submit" value="Paste" name="x-mode" onclick="return checkSelected('Paste before');"></input> -->
</xsl:if>
</form>
</xsl:if>
</div>



</xsl:when>

<!-- ======================================================================= -->

<xsl:otherwise>
<!-- We may have got here coz there arent any records to display, so show nothing -->
</xsl:otherwise>

<!-- ======================================================================= -->
</xsl:choose>
<!-- ======================================================================= -->

</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="chooseSortArrow">
<xsl:param name="fieldName"></xsl:param>
<xsl:choose>
<xsl:when test="($fieldName = $sortBy) and ($sortOrder = 'ascending')">&lt;img src='<xsl:value-of select="$resourcesPath"></xsl:value-of>/sort_arrow_up.gif' alt='sort' border='0'/&gt;</xsl:when>
<xsl:when test="($fieldName = $sortBy) and ($sortOrder = 'descending')">&lt;img src='<xsl:value-of select="$resourcesPath"></xsl:value-of>/sort_arrow_down.gif' alt='sort' border='0'/&gt;</xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:template>

</xsl:stylesheet>
