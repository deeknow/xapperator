<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xapp="http://xapperator.sourceforge.net/" exclude-result-prefixes="xapp">

<!--

LICENSE

XApperator: simple XML editing/publishing framework - utilities XSLT file
Copyright (C) 2005-2007  Dean Stringer (deeknow @ pobox . com)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

File: utils.xslt
Version 1.0.1
Description:
        used by edit.xslt to perform various common UI transforms

-->

<!-- ======================================================================= -->

<xsl:template name="dateToText">
<xsl:param name="dateValue"/>
<xsl:variable name="dateYear" select="substring($dateValue, 1, 4)"></xsl:variable>
<xsl:variable name="dateMon" select="substring($dateValue, 6, 2)"></xsl:variable>
<xsl:variable name="dateDay" select="substring($dateValue, 9, 2)"></xsl:variable>
<xsl:value-of select="$dateDay"></xsl:value-of> <xsl:value-of select="xapp:intToMonthShort($dateMon)"></xsl:value-of> <xsl:value-of select="$dateYear"></xsl:value-of>
</xsl:template>
	
<!-- ======================================================================= -->

	<xsl:template name="searchReplace">
	<!--	do a search / replace on a supplied string -->
	    <xsl:param name="textString"/>
	    <xsl:param name="textFind"/>
	    <xsl:param name="textReplace"/>
	    <xsl:choose>
	    <xsl:when test="contains($textString, $textFind)">
			<xsl:variable name="part1"><xsl:value-of select="substring-before($textString, $textFind)"></xsl:value-of></xsl:variable>
			<xsl:variable name="part2"><xsl:value-of select="substring-after($textString, $textFind)"></xsl:value-of></xsl:variable>
			<xsl:variable name="part3">
			<xsl:call-template name="searchReplace">
				<xsl:with-param name="textString" select="$part2"></xsl:with-param>
				<xsl:with-param name="textFind"><xsl:value-of select="$textFind"></xsl:value-of></xsl:with-param>
				<xsl:with-param name="textReplace"><xsl:value-of select="$textReplace"></xsl:value-of></xsl:with-param>
			</xsl:call-template>
			</xsl:variable>
			<xsl:value-of select="concat($part1,$textReplace,$part3)" />
		</xsl:when> 
		<xsl:otherwise><xsl:value-of select="$textString"></xsl:value-of></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

<!-- ======================================================================= -->

	
<xsl:template name="showWYSIWYGEditor">
<!-- only support TinyMCE at present, but should be failry easy to plugin another editor
	have to pass config here coz the node context is different in View and Edit modes -->
<xsl:param name="config"></xsl:param>
<xsl:variable name="buttons">
<xsl:choose>
<xsl:when test="$config/plugins/wysiwygEditor[@buttons != '']"><xsl:value-of select="$config/plugins/wysiwygEditor/@buttons"></xsl:value-of></xsl:when>
<xsl:otherwise>bold,italic,underline,separator,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,bullist,numlist,undo,redo,link,unlink,fontsizeselect,forecolor,charmap</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:text disable-output-escaping="yes">
&lt;script language="javascript" type="text/javascript" src="</xsl:text><xsl:value-of select="$config/plugins/wysiwygEditor/@href"></xsl:value-of><xsl:text disable-output-escaping="yes">tiny_mce.js"&gt;&lt;/script&gt;
&lt;script language="javascript" type="text/javascript"&gt;
tinyMCE.init({
	mode : "exact",
	elements : "</xsl:text><xsl:for-each select="$config/schema/fields/field[@type = 'wysiwyg']"><xsl:value-of select="@name"></xsl:value-of></xsl:for-each><xsl:text disable-output-escaping="yes">",
	theme : "custom",
	// editor_css : "/css/editorMCE.css",
	theme_custom_buttons1 : "</xsl:text><xsl:value-of select="$buttons"></xsl:value-of><xsl:text disable-output-escaping="yes">",
	theme_custom_buttons2 : "",
	theme_custom_buttons3 : "",
	font_size_style_values : "xx-small,x-small,small,medium,large,x-large,xx-large",
	theme_custom_toolbar_location : "top",
	theme_custom_toolbar_align : "left",
	theme_custom_path_location : "none",
	extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]"
});
&lt;/script&gt;
</xsl:text>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="showField">
<!-- take a passed field name and value, then find its matching paramaters in the config.xml file
	to determine how to render it. Is called whether we're in edit or add modes to show fields for
	records  -->
<xsl:param name="nodeName"></xsl:param>
<xsl:param name="nodeValue"></xsl:param>


<xsl:for-each select="/application/config/schema/fields/field">
<!-- check if this record in the for-each loop is the one we've been asked to display ... -->


<xsl:choose>

<!-- dont render any hidden fields as they're not editable -->
<xsl:when test="(@name = $nodeName) and (@type = 'hidden')">
<!-- if this is an auto-genereate-id field type and its value is currently blank then generate it a new unique value -->
<xsl:variable name="newNodeValue">
<xsl:choose>
<xsl:when test="($nodeValue = '') and (@generate-id = '1')"><xsl:value-of select="generate-id()"></xsl:value-of></xsl:when>
<xsl:otherwise><xsl:value-of select="$nodeValue"></xsl:value-of></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<input type="hidden" name="{$nodeName}" value="{$newNodeValue}"></input>
</xsl:when>

<xsl:when test="@name = $nodeName">
<!-- if this is an auto-genereate-id field type and its value is currently blank then generate it a new unique value -->
<xsl:variable name="newNodeValue">
<xsl:choose>
<xsl:when test="($nodeValue = '') and (@generate-id = '1')"><xsl:value-of select="generate-id()"></xsl:value-of></xsl:when>
<xsl:otherwise><xsl:value-of select="$nodeValue"></xsl:value-of></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:variable name="fieldLabelDisplay">
<xsl:choose>
<xsl:when test="@label != ''"><xsl:value-of select="@label"></xsl:value-of></xsl:when>
<xsl:otherwise><xsl:value-of select="@name"></xsl:value-of></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<!-- and add a row to the table with the label and the fields value -->
<tr>
<td valign="top"><xsl:value-of select="$fieldLabelDisplay"></xsl:value-of>
<xsl:if test="(@required = '1') or (@isindex != '')"><span class="required">*</span></xsl:if>
<xsl:if test="@tip != ''">
<xsl:text> </xsl:text><img onclick="alert('{@tip}')" src="{$resourcesPath}tip.png" alt="tip" title="{@tip}"></img>
</xsl:if>
</td>
<td valign="top">
<xsl:choose>
<!-- check if this field is uneditable, if so dont bother with input fields etc, just echo raw text -->
<xsl:when test="@editable = '0'"><p><xsl:value-of select="$newNodeValue"></xsl:value-of></p>
	<input type="hidden" name="{$nodeName}" value="{$newNodeValue}"></input>
</xsl:when>
<xsl:otherwise>
<xsl:choose>
<xsl:when test="@type = 'text'">
<!-- see if the field has specified a size in config.xml, otherwise default to '20' -->
<xsl:variable name="fieldSize">
<xsl:choose>
<xsl:when test="@size"><xsl:value-of select="@size"></xsl:value-of></xsl:when>
<xsl:otherwise>20</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<input type="text" id="x-field-{$nodeName}"  name="{$nodeName}" value="{$newNodeValue}" size="{$fieldSize}"></input>
</xsl:when>
<xsl:when test="@type = 'checkbox'">
<input type="checkbox" name="{$nodeName}">
<xsl:if test="($newNodeValue = '1') or ($newNodeValue = 'on')"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if>
</input>
</xsl:when>
<xsl:when test="(@type = 'textarea') or (@type = 'wysiwyg')">
<!-- see if the field has specified a row size in config.xml, otherwise default to '4' -->
<xsl:variable name="numRows">
<xsl:choose>
<xsl:when test="@rows"><xsl:value-of select="@rows"></xsl:value-of></xsl:when>
<xsl:otherwise>4</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<!-- see if the field has specified a column size in config.xml, otherwise default to '50' -->
<xsl:variable name="numCols">
<xsl:choose>
<xsl:when test="@cols"><xsl:value-of select="@cols"></xsl:value-of></xsl:when>
<xsl:otherwise>45</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<textarea rows="{$numRows}" cols="{$numCols}" name="{$nodeName}"><xsl:value-of select="$newNodeValue"></xsl:value-of>
<!-- insert a blank line if the textarea field is empty, otherwise browser may collapse it -->
<xsl:if test="$newNodeValue = ''"><xsl:text>
</xsl:text>
</xsl:if>
</textarea>
</xsl:when>
<xsl:when test="@type = 'select'">
<select name="{$nodeName}">
		<option><xsl:value-of select="$newNodeValue"></xsl:value-of></option>
		<xsl:call-template name="showSelectList">
			<xsl:with-param name="values" select="@values"></xsl:with-param>
			<xsl:with-param name="currentValue" select="$newNodeValue"></xsl:with-param>
		</xsl:call-template>
</select>
</xsl:when>
<xsl:when test="@type = 'radio'">
<xsl:call-template name="showRadioList">
	<xsl:with-param name="values" select="@values"></xsl:with-param>
	<xsl:with-param name="currentValue" select="$newNodeValue"></xsl:with-param>
	<xsl:with-param name="nodeName" select="$nodeName"></xsl:with-param>
</xsl:call-template>
</xsl:when>
<xsl:when test="@type = 'date'">
<xsl:call-template name="showDate">
	<xsl:with-param name="dateString" select="$newNodeValue"></xsl:with-param>
	<xsl:with-param name="dateName" select="$nodeName"></xsl:with-param>
</xsl:call-template>
</xsl:when>
<xsl:when test="@type = 'time'">
<xsl:call-template name="showTime">
	<xsl:with-param name="timeString" select="$newNodeValue"></xsl:with-param>
	<xsl:with-param name="timeName" select="$nodeName"></xsl:with-param>
</xsl:call-template>
</xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:otherwise>
</xsl:choose>
</td>
</tr>
</xsl:when>
<!-- must be some type we dont recognise, so dont do anything -->
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:for-each>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="showTime">
<!-- show a time string as two dropdowns, one each for hour and min -->
<xsl:param name="timeString"></xsl:param>
<xsl:param name="timeName"></xsl:param>
<!-- if no date passed for this value, use the the date paramater XApperator.pm passed in -->
<xsl:variable name="timeValue">
<xsl:choose>
<xsl:when test="$timeString = 'TIMENOW'"><xsl:value-of select="$timeNow"></xsl:value-of></xsl:when>
<xsl:otherwise><xsl:value-of select="$timeString"></xsl:value-of></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:variable name="timeHour" select="substring($timeValue, 1, 2)"></xsl:variable>
<xsl:variable name="timeMin" select="substring($timeValue, 3, 2)"></xsl:variable>
<xsl:call-template name="showSelectHour">
<xsl:with-param name="hour" select="$timeHour"></xsl:with-param>
<xsl:with-param name="timeName" select="$timeName"></xsl:with-param>
</xsl:call-template>
<xsl:call-template name="showSelectMin">
<xsl:with-param name="min" select="$timeMin"></xsl:with-param>
<xsl:with-param name="timeName" select="$timeName"></xsl:with-param>
</xsl:call-template>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="showSelectMin">
<xsl:param name="min"></xsl:param>
<xsl:param name="timeName"></xsl:param>
<select name="{$timeName}:min">
<xsl:choose>
<xsl:when test="$min = ''"><option value="" selected="selected">-----</option></xsl:when>
<xsl:otherwise><option selected="selected"><xsl:value-of select="$min"></xsl:value-of></option>
<option value="">-----</option>
</xsl:otherwise>
</xsl:choose>
<xsl:call-template name="optionRange">
<xsl:with-param name="thisVal" select="'0'"></xsl:with-param>
<xsl:with-param name="maxVal" select="'59'"></xsl:with-param>
<xsl:with-param name="increment" select="'5'"></xsl:with-param>
</xsl:call-template>
</select>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="showSelectHour">
<xsl:param name="hour"></xsl:param>
<xsl:param name="timeName"></xsl:param>
<xsl:variable name="displayHour">
<xsl:choose>
<xsl:when test="$hour &lt; 12"><xsl:value-of select="concat($hour, ' AM')"></xsl:value-of></xsl:when>
<xsl:otherwise><xsl:value-of select="concat($hour - 12, ' PM')"></xsl:value-of></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<select name="{$timeName}:hour">
<xsl:choose>
<xsl:when test="$hour = ''"><option value="" selected="selected">-----</option></xsl:when>
<xsl:otherwise><option selected="selected" value="{$hour}"><xsl:value-of select="$displayHour"></xsl:value-of></option>
<option value="">-----</option>
</xsl:otherwise>
</xsl:choose>
<option value="00">0 AM</option>
<option value="01">1</option>
<option value="02">2</option>
<option value="03">3</option>
<option value="04">4</option>
<option value="05">5</option>
<option value="06">6</option>
<option value="07">7</option>
<option value="08">8</option>
<option value="09">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12 PM</option>
<option value="13">1</option>
<option value="14">2</option>
<option value="15">3</option>
<option value="16">4</option>
<option value="17">5</option>
<option value="18">6</option>
<option value="19">7</option>
<option value="20">8</option>
<option value="21">9</option>
<option value="22">10</option>
<option value="23">11</option>
</select>
</xsl:template>


<!-- ======================================================================= -->

<xsl:template name="showDate">
<!-- show a date string as three dropdowns, one each for year, month and day -->
<xsl:param name="dateString"></xsl:param>
<xsl:param name="dateName"></xsl:param>
<!-- if no date passed for this value, use the the date paramater XApperator.pm passed in -->
<xsl:variable name="dateValue">
<xsl:choose>
<xsl:when test="$dateString = 'DATENOW'"><xsl:value-of select="$dateNow"></xsl:value-of></xsl:when>
<xsl:otherwise><xsl:value-of select="$dateString"></xsl:value-of></xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:choose>
<!-- add a javascript init() if any textarea fields are WYSIWYG editor type -->
<xsl:when test="$dateEntryFormat = 'ISO8601'">
<input name="{$dateName}" id="{$dateName}" type="text" maxlength="10" value="{$dateValue}" size="10"></input>
<img src="{$resourcesPath}calendar.gif" onclick="showChooser(this, '{$dateName}', 'chooserSpan', 2006, 2010, 'Y-m-d', false);" alt="Choose"/>
<div id="chooserSpan" class="dateChooser select-free" style="display: none; visibility: hidden; width: 160px;">
</div>
</xsl:when>
<xsl:otherwise>
<xsl:variable name="dateYear" select="substring($dateValue, 1, 4)"></xsl:variable>
<xsl:variable name="dateMon" select="substring($dateValue, 6, 2)"></xsl:variable>
<xsl:variable name="dateDay" select="substring($dateValue, 9, 2)"></xsl:variable>
<xsl:call-template name="showSelectYear">
<xsl:with-param name="year" select="$dateYear"></xsl:with-param>
<xsl:with-param name="dateName" select="$dateName"></xsl:with-param>
</xsl:call-template>
<xsl:call-template name="showSelectMonth">
<xsl:with-param name="month" select="$dateMon"></xsl:with-param>
<xsl:with-param name="dateName" select="$dateName"></xsl:with-param>
</xsl:call-template>
<xsl:call-template name="showSelectDay">
<xsl:with-param name="day" select="$dateDay"></xsl:with-param>
<xsl:with-param name="dateName" select="$dateName"></xsl:with-param>
</xsl:call-template>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="showSelectYear">
<xsl:param name="year"></xsl:param>
<xsl:param name="dateName"></xsl:param>
<select name="{$dateName}:year">
<xsl:choose>
<xsl:when test="$year = ''"><option value="" selected="selected">-----</option></xsl:when>
<xsl:otherwise><option selected="selected"><xsl:value-of select="$year"></xsl:value-of></option>
<option value="">-----</option>
</xsl:otherwise>
</xsl:choose>
<xsl:call-template name="optionRange">
<xsl:with-param name="thisVal" select="'2004'"></xsl:with-param>
<xsl:with-param name="maxVal" select="'2009'"></xsl:with-param>
</xsl:call-template>
</select>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="showSelectMonth">
<xsl:param name="month"></xsl:param>
<xsl:param name="dateName"></xsl:param>
<select name="{$dateName}:month">
<xsl:choose>
<xsl:when test="$month = ''"><option value="" selected="selected">-----</option></xsl:when>
<xsl:otherwise><option selected="selected" value="{$month}"><xsl:value-of select="xapp:intToMonthShort($month)"></xsl:value-of></option>
<option value="">-----</option>
</xsl:otherwise>
</xsl:choose>
<option value="01">Jan</option>
<option value="02">Feb</option>
<option value="03">Mar</option>
<option value="04">Apr</option>
<option value="05">May</option>
<option value="06">Jun</option>
<option value="07">Jul</option>
<option value="08">Aug</option>
<option value="09">Sep</option>
<option value="10">Oct</option>
<option value="11">Nov</option>
<option value="12">Dec</option>
</select>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="showSelectDay">
<xsl:param name="day"></xsl:param>
<xsl:param name="dateName"></xsl:param>
<select name="{$dateName}:day">
<xsl:choose>
<xsl:when test="$day = ''"><option value="" selected="selected">---</option></xsl:when>
<xsl:otherwise><option selected="selected"><xsl:value-of select="$day"></xsl:value-of></option>
<option value="">---</option>
</xsl:otherwise>
</xsl:choose>
<xsl:call-template name="optionRange">
<xsl:with-param name="thisDay" select="'1'"></xsl:with-param>
<xsl:with-param name="maxVal" select="'31'"></xsl:with-param>
</xsl:call-template>
</select>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="optionRange">
<xsl:param name="thisVal" select="'1'"></xsl:param>
<xsl:param name="maxVal" select="'1'"></xsl:param>
<xsl:param name="increment" select="'1'"></xsl:param>
<xsl:choose>
<xsl:when test="$thisVal &lt;= $maxVal">
<option><xsl:if test="$thisVal &lt; 10">0</xsl:if><xsl:value-of select="$thisVal"></xsl:value-of></option>
<xsl:call-template name="optionRange">
<xsl:with-param name="thisVal" select="$thisVal + $increment"></xsl:with-param>
<xsl:with-param name="maxVal" select="$maxVal"></xsl:with-param>
<xsl:with-param name="increment" select="$increment"></xsl:with-param>
</xsl:call-template>
</xsl:when>
<xsl:otherwise></xsl:otherwise>
</xsl:choose>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="showSelectList">
<!-- recursive template that runs through a list of values and shows them as select options 
	and  not showing the option if it matches that of the record being edited -->
<xsl:param name="values"></xsl:param>
<xsl:param name="currentValue"></xsl:param>
<xsl:variable name="nextItem" select="substring-before($values, ',')"></xsl:variable>
<xsl:variable name="theRest" select="substring-after($values, ',')"></xsl:variable>
<xsl:if test="$nextItem != $currentValue">
<option><xsl:value-of select="$nextItem"></xsl:value-of></option>
</xsl:if>
<xsl:choose>
<xsl:when test="contains($theRest, ',')">
	<xsl:call-template name="showSelectList">
		<xsl:with-param name="values" select="$theRest"></xsl:with-param>
		<xsl:with-param name="currentValue" select="$currentValue"></xsl:with-param>
	</xsl:call-template>
</xsl:when>
<xsl:otherwise>
	<xsl:if test="$theRest != $currentValue">
		<option><xsl:value-of select="$theRest"></xsl:value-of></option>
	</xsl:if>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="showRadioList">
<!-- recursive template that runs through a list of values and shows them as radio buttons,
	setting the checked property if the value is the same as the one for the record being edited -->
<xsl:param name="values"></xsl:param>
<xsl:param name="currentValue"></xsl:param>
<xsl:param name="nodeName"></xsl:param>
<xsl:variable name="nextItem" select="substring-before($values, ',')"></xsl:variable>
<xsl:variable name="theRest" select="substring-after($values, ',')"></xsl:variable>
<xsl:choose>
	<xsl:when test="$nextItem = $currentValue"><input type="radio" name="{$nodeName}" value="{$nextItem}" checked="checked"></input></xsl:when>
	<xsl:otherwise><input type="radio" name="{$nodeName}" value="{$nextItem}"></input></xsl:otherwise>
</xsl:choose>
  <xsl:value-of select="$nextItem"></xsl:value-of>
<xsl:choose>
<xsl:when test="contains($theRest, ',')">
	<xsl:call-template name="showRadioList">
		<xsl:with-param name="values" select="$theRest"></xsl:with-param>
		<xsl:with-param name="currentValue" select="$currentValue"></xsl:with-param>
		<xsl:with-param name="nodeName" select="$nodeName"></xsl:with-param>
	</xsl:call-template>
</xsl:when>
<xsl:otherwise>
<xsl:choose>
	<xsl:when test="$theRest = $currentValue"><input type="radio" name="{$nodeName}" value="{$theRest}" checked="checked"></input></xsl:when>
	<xsl:otherwise><input type="radio" name="{$nodeName}" value="{$theRest}"></input></xsl:otherwise>
</xsl:choose>
 <xsl:value-of select="$theRest"></xsl:value-of>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="addValidateFKeyJS">
<!-- dont show unless clientsidevalidation is specified -->
<xsl:if test="//config/schema/clientsidevalidation = 'Javascript'">
<script language='JavaScript' type='text/javascript'>
<xsl:comment>
function validateFKey(thisForm)
{
	var standardMsg = '<xsl:value-of select="//config/messages/pleasesupply[lang($lang)]"></xsl:value-of>';
	var invalidMsg = '<xsl:value-of select="//config/messages/fkeyinvalid[lang($lang)]"></xsl:value-of>';
	var thisLabel = '';
	var thisName = '';
	for ( i = 1; i &lt; thisForm.elements.length; i++) {
		if (thisForm.elements[i].name == 'x-label') {
			thisLabel = thisForm.elements[i].value;
		}
		if (thisForm.elements[i].name == 'x-value') {
			thisValue = thisForm.elements[i].value;
		}
	}
	if ( (thisLabel == "") || (thisValue == "") ) {
		alert( standardMsg + ' both the Label and Value');
		return false;
	} else if (thisValue.match("[^a-z0-9\-_]") ) {
		alert( invalidMsg );
		return false;
	} else {
		return true;
	}
}
</xsl:comment>
</script>
</xsl:if>
</xsl:template>

<!-- ======================================================================= -->

<xsl:template name="addValidateFormJS">
<!-- dont show unless clientsidevalidation is specified -->
<xsl:if test="//config/schema/clientsidevalidation = 'Javascript'">
<script language='JavaScript' type='text/javascript'>
<xsl:comment>
<xsl:text>
function validate(thisForm)
{
</xsl:text>
	var standardMsg = '<xsl:value-of select="//config/messages/pleasesupply[lang($lang)]"></xsl:value-of>';
	var value;
<xsl:for-each select="//config/schema/fields/field[((@isindex = '1') or (@required = '1'))]">
<xsl:choose>
<xsl:when test="(@format = 'textarea') and (@type = 'wysiwyg')">
// ====	need to extract the mce editor iframe content as the original TEXTAREA isnt updated till the tinyMCE.triggersave is called ====
// ====	solution from: http://tinymce.moxiecode.com/punbb/viewtopic.php?id=4288
	value = tinyMCE.getContent();
</xsl:when>
<xsl:otherwise>
	value = thisForm.<xsl:value-of select="@name"></xsl:value-of>.value;
</xsl:otherwise>
</xsl:choose>
	if ( value == "") {
		alert( standardMsg + '<xsl:value-of select="@label"></xsl:value-of>');
		thisForm.<xsl:value-of select="@name"></xsl:value-of>.focus();
		return false;
	}
</xsl:for-each>
	return true;
<xsl:text>
}
</xsl:text>
</xsl:comment>
</script>
</xsl:if>
</xsl:template>

<!-- ======================================================================= -->


</xsl:stylesheet>
